/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controllers;

import Entities.BusinessType;
import Entities.Customer;
import Entities.Invoice;
import Entities.Job;
import Entities.Payment;
import Entities.StockDelivery;
import Entities.StockItem;
import Entities.Vehicle;
import com.mysql.jdbc.exceptions.jdbc4.MySQLIntegrityConstraintViolationException;
import java.sql.*;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperPrintManager;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.view.JasperViewer;

/**
 * The receptionist controller class contains the complex logic used in the
 * ReceptionistGUI.
 * @author Abdalrahmane
 */
public class ReceptionistController extends ActorController {
    private Connection conn;
    private DatabaseConnection dbc;
    private ArrayList<Job> jobData;
    private ArrayList<Vehicle> vehData;
    private ArrayList<StockItem> stockData;
    private ArrayList<StockDelivery> deliveryData;
    private ArrayList<Customer> cusData;
    private ArrayList<Invoice> invoiceData;
    private ArrayList<String> busTypeData;
    private byte[] jobChecksum;
    private byte[] vehChecksum;
    private byte[] stockChecksum;
    private byte[] deliveryChecksum;
    private byte[] customerChecksum;
    private byte[] invoiceChecksum;

    public ReceptionistController(DatabaseConnection dbc) {
        this.dbc = dbc;
        conn = dbc.getConnection();
        jobData = new ArrayList<>();
        vehData = new ArrayList<>();
        stockData = new ArrayList<>();
        deliveryData = new ArrayList<>();
        cusData = new ArrayList<>();
        invoiceData = new ArrayList<>();
        busTypeData = new ArrayList<>();
        jobChecksum = setChecksum(conn, "Jobs");
        vehChecksum = setChecksum(conn, "Vehicles");
        stockChecksum = setChecksum(conn, "SparePartStock");
        deliveryChecksum = setChecksum(conn, "SparePartDelivery");
        invoiceChecksum = setChecksum(conn, "invoices");
        
        getJobInfo();
    }

    /**
     * Populates an arraylist with all jobs from the jobs table in the database
     */
    private void getJobInfo() {
        jobData.clear();
        try {
            Statement st = conn.createStatement();
            ResultSet rs = st.executeQuery("SELECT * FROM Jobs");
            while(rs.next()){
                jobData.add(new Job(rs.getInt("JobID"), rs.getString("RegNo"), 
                        rs.getString("Work_required"), rs.getString("JobStatus"), 
                        rs.getString("BusinessType"),
                        rs.getTime("Duration"), rs.getDate("Date_creation")));
            }
            rs.close();
            st.close();
        } catch (SQLException ex) {
            ex.printStackTrace();
        }    
    }
    /**
     * Returns the populated job arraylist, and sets the checksum to the latest one
     * @return 
     */
    public ArrayList getJobData(){
        jobChecksum = setChecksum(conn, "Jobs");
        getJobInfo();
        return jobData;
    }
    
    /**
     * Populates arraylist with all vehicle details that is located in the
     * vehicles table in the database
     */
    private void getVehInfo(){
        vehData.clear();
        try {
            Statement st = conn.createStatement();
            ResultSet rs = st.executeQuery("SELECT * FROM Vehicles");
            while(rs.next()){
                vehData.add(new Vehicle(rs.getString("RegNo"), rs.getInt("EngSerial"), 
                        rs.getInt("ChassisNo"),rs.getString("Colour"), 
                        rs.getString("Make"), rs.getString("Model"), rs.getString("Year"),
                        rs.getDate("LastMoTCheck")));
            }
            rs.close();
            st.close();
        } catch (SQLException ex) {
            ex.printStackTrace();
        }    
    }
    /**
     * Returns the vehicle arraylist that has been populated,
     * sets the checksum to the newest version
     * @return 
     */
    public ArrayList getVehData(){
        vehChecksum = setChecksum(conn, "Vehicles");
        getVehInfo();
        return vehData;
    }
    /**
     * Populates an arraylist with all stock information located in the sparepartstock
     * table
     */
    private void getStockInfo(){
        stockData.clear();
        try {
            Statement st = conn.createStatement();
            ResultSet rs = st.executeQuery("SELECT * FROM sparepartstock");
            while(rs.next()){
                stockData.add(new StockItem(rs.getString("Code"), rs.getString("part_name"), 
                        rs.getString("manufacturer"),rs.getString("Vehicle_Type"), 
                        rs.getString("Year"), rs.getDouble("Price"), rs.getDouble("Cost_Per_Item"),
                        rs.getInt("Stock_Level"),rs.getDouble("Stock_Cost"),rs.getInt("Threshold_Level"),
                        rs.getInt("Inital_Stock_Level"),rs.getDouble("Inital_Stock_Cost")));
            }
            rs.close();
            st.close();
        } catch (SQLException ex) {
            ex.printStackTrace();
        }   
    }
    
    /**
     * Returns the stock arraylist and sets the checksum to the latest version
     * @return 
     */
    public ArrayList getStockData(){
        stockChecksum = setChecksum(conn, "SparePartStock");
        getStockInfo();
        return stockData;
    }
    
    /**
     * Populates an arraylist with all stock items that are below the threshold level
     * The method is used to display low stock information during the threshold alert.
     */
    private void getLowStockInfo(){
        stockData.clear();
        try {
            Statement st = conn.createStatement();
            ResultSet rs = st.executeQuery("SELECT * FROM `sparepartstock` WHERE `Stock_Level` < `Threshold_Level`");
            while(rs.next()){
                stockData.add(new StockItem(rs.getString("Code"), rs.getString("part_name"), 
                        rs.getString("manufacturer"),rs.getString("Vehicle_Type"), 
                        rs.getString("Year"), rs.getDouble("Price"), rs.getDouble("Cost_Per_Item"),
                        rs.getInt("Stock_Level"),rs.getDouble("Stock_Cost"),rs.getInt("Threshold_Level"),
                        rs.getInt("Inital_Stock_Level"),rs.getDouble("Inital_Stock_Cost")));
            }
            rs.close();
            st.close();
        } catch (SQLException ex) {
            ex.printStackTrace();
        }   
    }
    
    /**
     * Returns the arraylist containing all the stock items that are below the thresold
     * @return 
     */
    public ArrayList getLowStockData(){
        getLowStockInfo();
        return stockData;
    }
    
    /**
     * Populates an arraylist with all delivery information located in the 
     * sparepartdelivery table in the database.
     * @param code the stock item code
     */
    private void getDeliveryInfo(String code){
        deliveryData.clear();
        try {
            PreparedStatement pstm = conn.prepareStatement("SELECT * FROM `SparePartDelivery` WHERE Code = ?");
            pstm.setString(1, code);
            ResultSet rs = pstm.executeQuery();
            while(rs.next()){
                deliveryData.add(new StockDelivery(rs.getInt("deliveryID"),rs.getString("code"), rs.getInt("Quantity"), 
                        rs.getBoolean("Recieved"), rs.getDouble("Stock_Cost"), rs.getDate("Date_Ordered")));
            }
            rs.close();
            pstm.close();
        } catch (SQLException ex) {
            ex.printStackTrace();
        }    
    }
    
    /**
     * Returns the arraylist that is popualted with all the spareparts delivery
     * sets the checksum to the latest one
     * @param code
     * @return 
     */
    public ArrayList getDeliveryData(String code){
        deliveryChecksum = setChecksum(conn, "SparePartDelivery");
        getDeliveryInfo(code);
        return deliveryData;
    }
    
    /**
     * Populates an arraylist with customer details retrieved from the customer table
     * in the database
     */
    private void getCusInfo(){
        cusData.clear();
        try {
            Statement st = conn.createStatement();
            ResultSet rs = st.executeQuery("SELECT * FROM customers");

            while(rs.next()){
                cusData.add(new Customer(rs.getInt("CustomerID"), rs.getString("Name"), 
                        rs.getString("telephone"), rs.getString("mobile"), 
                        rs.getString("email"), rs.getString("address"), rs.getString("Street"), rs.getString("locality"),
                        rs.getString("city"), rs.getString("postcode"),rs.getString("Additional_Notes"), rs.getString("customerType")));
            }
            rs.close();
            st.close();
        } catch (SQLException ex) {
            ex.printStackTrace();
        }    
    }
    
    /**
     * Return customer arraylist and sets checksum to latest one for customer table
     * @return 
     */
    public ArrayList getCusData(){
        customerChecksum = setChecksum(conn,"Customers"); //set the newest checksum
        getCusInfo(); //get most updated version of table
        return cusData;
    }
    
    /**
     * Populates an array list with all vehicle information linked to a single customer.
     * @param cusID the selected customer
     */
    public void getCusVehInfo(int cusID){
        vehData.clear();
        try {
            PreparedStatement pstm = conn.prepareStatement("SELECT * FROM `Vehicles` WHERE CustomerID = ?");
            pstm.setInt(1, cusID);
            ResultSet rs = pstm.executeQuery();
            while(rs.next()){
                vehData.add(new Vehicle(rs.getString("RegNo"), rs.getInt("EngSerial"), 
                        rs.getInt("ChassisNo"),rs.getString("Colour"), 
                        rs.getString("Make"), rs.getString("Model"), rs.getString("Year"),
                        rs.getDate("LastMoTCheck")));
            }
            rs.close();
            pstm.close();
        } catch (SQLException ex) {
            ex.printStackTrace();
        }    
    }
    
    /**
     * Returns arraylist of all vehicles linked to the customer
     * @param cusID selected customer id
     * @return 
     */
    public ArrayList getCusVehData(int cusID){
        getCusVehInfo(cusID);
        return vehData;
    }
    
    /**
     * Populate arraylist with invoice of a selected jobID
     * @param jobID 
     */
    public void getJobInvoiceInfo(int jobID){
        invoiceData.clear();
        try {
            Statement st = conn.createStatement();
            ResultSet rs = st.executeQuery("SELECT * FROM invoices WHERE jobID ="+jobID);
            while(rs.next()){
                invoiceData.add(new Invoice(rs.getInt("invoiceid"), rs.getInt("jobID"), rs.getInt("reminder"),
                        rs.getDouble("stockAmount"), rs.getDouble("labourcost"), 
                        rs.getDouble("vat"), rs.getDouble("vat"), rs.getDouble("grandtotal"), 
                        rs.getDate("initalDate"), rs.getDate("lastDateReminder"), rs.getBoolean("paid")));
            }
            rs.close();
            st.close();
        } catch (SQLException ex) {
            ex.printStackTrace();
        }    
    }
        
    /**
     * Returns the arraylist containing invoice of a selected job
     * @param jobID - selected job
     * @return 
     */
    public ArrayList getJobInvoiceData(int jobID){
        invoiceChecksum = setChecksum(conn, "invoices");
        getJobInvoiceInfo(jobID);
        return invoiceData;
    }
    
    /**
     * Populate arraylist with business types.
     * This information is used to create a job business type on this information 
     * eg repair/mot/annual service
     */
    public void getBusInfo(){
        busTypeData.clear();
        try {
            Statement st = conn.createStatement();
            ResultSet rs = st.executeQuery("SELECT * FROM businesstypes");
            while(rs.next()){
                busTypeData.add(rs.getString("Type"));
            }
            rs.close();
            st.close();
        } catch (SQLException ex) {
            ex.printStackTrace();
        }    
    }
        
    /**
     * Returns business type arraylist
     * @return 
     */
    public ArrayList getBusData(){
        getBusInfo();
        return busTypeData;
    }
    
    /**
     * Retrieve the pay information (eg. card details etc) related to an invoice
     * @param invoiceID selected invoice
     * @return 
     */
    public ArrayList getPayData(int invoiceID){
        ArrayList<Payment> payData = new ArrayList<>();
        try {
            Statement st = conn.createStatement();
            ResultSet rs = st.executeQuery("SELECT * FROM jobpayment WHERE invoiceID="+invoiceID);
            while(rs.next()){
                payData.add(new Payment(rs.getInt("PaymentID"), rs.getInt("invoiceID"), 
                        rs.getString("card_no"), rs.getString("expires"), rs.getDate("payment_made")));
            }
            rs.close();
            st.close();
        } catch (SQLException ex) {
            ex.printStackTrace();
        }     
        return payData;
    }
    
    
    /**
     * This method is called before create a job, the method checks whether the 
     * customer has outstanding invoice for both jobs or a spare parts
     * if the customer does have an outstanding invoice the method returns true
     * @param regNo selected reg no of job the receptionist is trying to create
     * @return 
     */
    public boolean outstandingInvoice(String regNo){
        try {
            int cusID = 0;
            PreparedStatement pstm = conn.prepareStatement("SELECT * FROM `vehicles` WHERE regNo = ?");
            pstm.setString(1, regNo);
            ResultSet rs = pstm.executeQuery();
            if (rs.next()){
                cusID = rs.getInt("customerID"); //get cusid
            }
            pstm = conn.prepareStatement("SELECT * FROM `invoices` "
                    + "INNER JOIN jobs ON invoices.JobID=jobs.JobID "
                    + "INNER JOIN vehicles ON jobs.RegNo = vehicles.RegNo "
                    + "INNER JOIN customers ON vehicles.CustomerID=customers.CustomerID "
                    + "WHERE customers.CustomerID = ? AND `initalDate` < date_sub(now(), interval 1 month) AND `paid` = 0");
            pstm.setInt(1, cusID);
            rs = pstm.executeQuery();
            if (rs.next()){
                return true; //has an outstanding invoice dont make job
            }
            
            //check spareparts invoices 
            pstm = conn.prepareStatement("SELECT * FROM `purchasedpartinvoices` "
                    + "INNER JOIN sparepartsold ON purchasedpartinvoices.SoldID=sparepartsold.SoldID "
                    + "AND `initalDate` < date_sub(now(), interval 1 month) WHERE sparepartsold.CustomerID=? AND `paid` = 0");
            pstm.setInt(1, cusID);
            rs = pstm.executeQuery();
            if (rs.next()){
                return true;
            }
            
            rs.close();
            pstm.close();   
        } catch (SQLException ex) {
            Logger.getLogger(ReceptionistController.class.getName()).log(Level.SEVERE, null, ex);
        }
        return false;
    }
    
    /**
     * The method first checks if the customer has an outstanding invoice
     * if so the message is displayed and the job is not created. if this check is passed
     * the method will check if the table data is latest if this is passed
     * the job is the ncreated in the jobs table with the information passed through the parameters
     * @param regNo - reg no of vehicle
     * @param workReq - work required for the specified job
     * @param bussType - the work business type eg. mot
     * @param h - hours job duration
     * @param m - min job duration
     * @param s - second job duration
     */
    public void createJob(String regNo, String workReq, String bussType, int h, int m, int s){
        if(outstandingInvoice(regNo)){
            JOptionPane.showMessageDialog(null,
                    "The account linked to this vehicle is suspended til late payment is fulfilled - Job has not been created",
                        "Job not created!",
                    JOptionPane.ERROR_MESSAGE);
            return;
        }
        
        if(!getChecksum(conn,"Jobs",jobChecksum)){
            JOptionPane.showMessageDialog(null,
                    "The previous action has failed as the table contains old records."
                            + "\nThe data will now be updated!",
                    "Table has been altered!",
            JOptionPane.ERROR_MESSAGE);
        }else{
            try {
                Time t = new Time(h,m,s);
                PreparedStatement pstm = conn.prepareStatement("INSERT INTO `Jobs` "
                        + "(`JobID`, `RegNo`, `Work_required`, `JobStatus`, `BusinessType`, `Duration`, `Date_Creation`) "
                        + "VALUES (NULL, ?, ?, \"IDLE\", ?, ?, CURRENT_DATE());");
                pstm.setString(1, regNo);
                pstm.setString(2, workReq);
                pstm.setString(3, bussType);
                pstm.setTime(4, t);
                pstm.executeUpdate();
                pstm.close();
                JOptionPane.showMessageDialog(null,
                    "Job for "+regNo+" has been added!",
                    "Job created",
                    JOptionPane.INFORMATION_MESSAGE);
            } catch (SQLException ex) {
                ex.printStackTrace();
            } catch (NullPointerException e){
                    JOptionPane.showMessageDialog(null,
                        "Job has not been added!",
                        "Operation cancelled out",
                        JOptionPane.WARNING_MESSAGE);
            }
        }
    }
    
    /**
     * Allows the receptionist to alter a existing job.
     * method checks if the table has been altered beforehand.
     * If the job status is changed to complete the method sums the duration of all tasks
     * and inserts it inside.
     * @param jobID - selected job id
     * @param workReq - new work required
     * @param status - new job status
     * @param bussType - new business type
     * @param h - new hour duration
     * @param m - new min duration
     * @param s - new second duration
     */
    public void alterJob(int jobID,  String workReq, String status, String bussType, int h, int m, int s){
            if(!getChecksum(conn,"Jobs",jobChecksum)){
            JOptionPane.showMessageDialog(null,
                    "The previous action has failed as the table contains old records."
                            + "\nThe data will now be updated!",
                    "Table has been altered!",
            JOptionPane.ERROR_MESSAGE);
        }else{
            try {

                Time t = new Time(h,m,s);
                PreparedStatement pstm = conn.prepareStatement("UPDATE `jobs` "
                        + "SET `Work_Required` = ?, `JobStatus` = ?, `BusinessType` = ?, `Duration` = ? "
                        + "WHERE `jobs`.`JobID` = ?");
                pstm.setString(1, workReq);
                pstm.setString(2, status);
                pstm.setString(3, bussType);
                pstm.setTime(4, t);
                pstm.setInt(5, jobID);
                pstm.executeUpdate();
                pstm.close();
                if(status.equals("COMPLETED")){
                    calculateTaskTime(jobID);
                }
                JOptionPane.showMessageDialog(null,
                    "Job has been altered!",
                    "Job altered",
                    JOptionPane.INFORMATION_MESSAGE);
            } catch (SQLException ex) {
                ex.printStackTrace();
            } catch (NullPointerException e){
                    JOptionPane.showMessageDialog(null,
                        "Job has not been altered!",
                        "Operation cancelled out",
                        JOptionPane.WARNING_MESSAGE);
            }
        }
    }
    
    
    /**
     * Calculates the sum duration of all tasks and sets it as the job duration
     * this called when the recpetionist sets job status as complete.
     * @param jobID 
     */
    private void calculateTaskTime(int jobID){
        try {
            Statement st = conn.createStatement();
            ResultSet rs = st.executeQuery("SELECT SEC_TO_TIME( SUM( TIME_TO_SEC( `Duration` ) ) ) AS timeSum FROM tasks WHERE JobID ="+jobID);
            java.sql.Time time = null;
            while(rs.next()){
                time = rs.getTime("timeSum");
            }
            
            if(time==null) time = new Time(0);
            
            rs.close();
            st.close();
            
            PreparedStatement pstm = conn.prepareStatement("UPDATE `jobs` SET `Duration` = ?"
                        + " WHERE `jobs`.`JobID` = ?");
            pstm.setTime(1, time);
            pstm.setInt(2, jobID);
            pstm.executeUpdate();
            pstm.close();
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
    }
    
    /**
     * Allows the receptionist to create a stock item
     * @param code stock code
     * @param name stock name
     * @param manu stock manufacturer
     * @param type stock type
     * @param year stock year
     * @param price stock price
     * @param stock stock level
     * @param threshold stock threshold
     * @param cost stock cost
     */
    public void createStockItem(String code, String name, String manu, String type, String year, double price, int stock, int threshold, double cost){
        if(!getChecksum(conn,"SparePartStock",stockChecksum)){
            JOptionPane.showMessageDialog(null,
                    "The previous action has failed as the table contains old records."
                            + "\nThe data will now be updated!",
                    "Table has been altered!",
            JOptionPane.ERROR_MESSAGE);
        }else{
            try {
                PreparedStatement pstm = conn.prepareStatement("INSERT INTO `sparepartstock` "
                        + "(`Code`, `Part_Name`, `Manufacturer`, `Vehicle_Type`, `Year`, `Price`, `Cost_Per_Item`, `Stock_Level`, `Stock_Cost`, `Threshold_Level`, `Inital_Stock_Level`, `Inital_Stock_Cost`) "
                        + "VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)");
                pstm.setString(1, code);
                pstm.setString(2, name);
                pstm.setString(3, manu);
                pstm.setString(4, type);
                pstm.setString(5, year);
                pstm.setDouble(6, price);
                pstm.setDouble(7, cost);
                pstm.setInt(8, stock);
                pstm.setDouble(9, cost*stock);
                pstm.setInt(10, threshold);
                pstm.setInt(11, stock);
                pstm.setDouble(12, cost*stock);
                pstm.executeUpdate();
                pstm.close();
                JOptionPane.showMessageDialog(null,
                    "Stock Item for "+name+" has been added!",
                    "Stock created",
                    JOptionPane.INFORMATION_MESSAGE);
            } catch(MySQLIntegrityConstraintViolationException e){
                    JOptionPane.showMessageDialog(null,
                        "Stock code already exists!",
                        "Operation cancelled out",
                        JOptionPane.WARNING_MESSAGE);
            }catch (SQLException ex) {
                ex.printStackTrace();
            } catch (NullPointerException e){
                    JOptionPane.showMessageDialog(null,
                        "Stock Item has not been added!",
                        "Operation cancelled out",
                        JOptionPane.WARNING_MESSAGE);
            }
        }
    }
    
    /**
     * Allows the receptionist to alter the details of a selected stock item
     * @param code stock code
     * @param name stock name
     * @param manu stock manufacturer
     * @param type stock type
     * @param year stock year
     * @param price stock price
     * @param stock stock level
     * @param threshold stock threshold
     * @param cost stock cost
     * @param istock inital stock level
     * @param stockID the selected stock item
     */
    public void alterStockItem(String code, String name, String manu, String type, String year, double price, int stock, int threshold, double cost, int istock, String stockID){
        if(!getChecksum(conn,"SparePartStock",stockChecksum)){
            JOptionPane.showMessageDialog(null,
                    "The previous action has failed as the table contains old records."
                            + "\nThe data will now be updated!",
                    "Table has been altered!",
            JOptionPane.ERROR_MESSAGE);
        }else{
            try {
                PreparedStatement pstm = conn.prepareStatement("UPDATE `sparepartstock` "
                        + "SET `Code` = ?, `Part_Name` = ?, `Manufacturer` = ?, `Vehicle_Type` = ?,"
                        + "`Year` = ?, `Price` = ?, `Cost_Per_Item` = ?, `Stock_Level` = ?, `Stock_Cost` = ?, "
                        + "`Threshold_Level` = ?, `Inital_Stock_Level` = ?, `Inital_Stock_Cost` = ? "
                        + "WHERE `sparepartstock`.`Code` = ? ");
                pstm.setString(1, code);
                pstm.setString(2, name);
                pstm.setString(3, manu);
                pstm.setString(4, type);
                pstm.setString(5, year);
                pstm.setDouble(6, price);
                pstm.setDouble(7, cost);
                pstm.setInt(8, stock);
                pstm.setDouble(9, cost*stock);
                pstm.setInt(10, threshold);
                pstm.setInt(11, istock);
                pstm.setDouble(12, istock*cost);
                pstm.setString(13, stockID);
                pstm.executeUpdate();
                pstm.close();
                updateDeliveryCost(code, cost);
                JOptionPane.showMessageDialog(null,
                    "Stock Item for "+name+" has been altered!",
                    "Stock Altered",
                    JOptionPane.INFORMATION_MESSAGE);
            } catch(MySQLIntegrityConstraintViolationException e){
                    JOptionPane.showMessageDialog(null,
                        "Stock code already exists!",
                        "Operation cancelled out",
                        JOptionPane.WARNING_MESSAGE);
            }catch (SQLException ex) {
                ex.printStackTrace();
            } catch (NullPointerException e){
                    JOptionPane.showMessageDialog(null,
                        "Stock Item has not been added!",
                        "Operation cancelled out",
                        JOptionPane.WARNING_MESSAGE);
            }
        }
    }
    
    /**
     * Allows the recepionist to order a new item
     * @param code - stock code
     * @param quantity - amount ordered
     * @param costItem - cost of the stock
     */
    public void orderItem(String code, int quantity, double costItem){
        if(!getChecksum(conn,"SparePartDelivery",deliveryChecksum)){
            JOptionPane.showMessageDialog(null,
                    "The previous action has failed as the table contains old records."
                            + "\nThe data will now be updated!",
                    "Table has been altered!",
            JOptionPane.ERROR_MESSAGE);
        }else{
            try {
                PreparedStatement pstm = conn.prepareStatement("INSERT INTO `sparepartdelivery` "
                        + "(`DeliveryID`, `Code`, `Quantity`, `Recieved`, `Stock_Cost`, `Date_Ordered`) "
                        + "VALUES (NULL, ?, ?, 0, ?, CURRENT_DATE())");
                pstm.setString(1, code);
                pstm.setInt(2, quantity);
                pstm.setDouble(3, quantity*costItem);
                pstm.executeUpdate();
                pstm.close();
                JOptionPane.showMessageDialog(null,
                    "An order for "+code+" has been made!",
                    "Stock Ordered",
                    JOptionPane.INFORMATION_MESSAGE);
            }catch (SQLException ex) {
                ex.printStackTrace();
            } catch (NullPointerException e){
                    JOptionPane.showMessageDialog(null,
                        "Stock Item has not been ordered!",
                        "Operation cancelled out",
                        JOptionPane.WARNING_MESSAGE);
            }
        }
    }

    /**
     * Allows the receptionist to recieve a delivery and in turn
     * updating the stock level of the item
     * @param delID - delivery id
     * @param delStock - delivery amount
     * @param code - stock part code
     */
    public void recieveDelivery(int delID, int delStock, String code){
        if(!getChecksum(conn,"SparePartDelivery",deliveryChecksum)){
            JOptionPane.showMessageDialog(null,
                    "The previous action has failed as the table contains old records."
                            + "\nThe data will now be updated!",
                    "Table has been altered!",
            JOptionPane.ERROR_MESSAGE);
        }else{
            try {
                PreparedStatement pstm = conn.prepareStatement("UPDATE `sparepartdelivery` "
                        + "SET `Recieved` = '1' WHERE `sparepartdelivery`.`DeliveryID` = ?");
                pstm.setInt(1, delID);
                pstm.executeUpdate();
                pstm = conn.prepareStatement("UPDATE `sparepartstock` "
                        + "SET `Stock_Level` = `Stock_Level` + ?, `Stock_Cost` = `Cost_Per_Item` * `Stock_Level` WHERE `sparepartstock`.`Code` = ? ");
                pstm.setInt(1,delStock);
                pstm.setString(2, code);
                pstm.executeUpdate();
                pstm.close();
                JOptionPane.showMessageDialog(null,
                    "The order has been recieved and stock has been updated!",
                    "Stock Ordered",
                    JOptionPane.INFORMATION_MESSAGE);
            }catch (SQLException ex) {
                ex.printStackTrace();
            } catch (NullPointerException e){
                    JOptionPane.showMessageDialog(null,
                        "Stock Item has not been ordered!",
                        "Operation cancelled out",
                        JOptionPane.WARNING_MESSAGE);
            }
        }
    }
    
    /**
     * Allows receptionist to delete a delivery
     * @param delID - selected delivery id
     */
    public void deleteDelivery(int delID){
        if(!getChecksum(conn,"SparePartDelivery",deliveryChecksum)){
            JOptionPane.showMessageDialog(null,
                    "The previous action has failed as the table contains old records."
                            + "\nThe data will now be updated!",
                    "Table has been altered!",
            JOptionPane.ERROR_MESSAGE);
        }else{
            try {
                PreparedStatement pstm = conn.prepareStatement("DELETE FROM SparePartDelivery WHERE DeliveryID=?;");
                pstm.setInt(1, delID);
                pstm.executeUpdate();
                pstm.close();
                JOptionPane.showMessageDialog(null,
                    "Delivery has been deleted!",
                    "Delivery Deleted",
                    JOptionPane.INFORMATION_MESSAGE);
            } catch (SQLException ex) {
                    JOptionPane.showMessageDialog(null,
                        "Vehicle has not been deleted!",
                        "Operation cancelled out",
                        JOptionPane.WARNING_MESSAGE);
            }
        }
    }
    
    
    /**
     * Update delivery cost when altered for items not received.
     * @param code
     * @param cost 
     */
    private void updateDeliveryCost(String code, double cost){
        try {
            PreparedStatement pstm = conn.prepareStatement("UPDATE `sparepartdelivery` "
                    + "SET `Stock_Cost` = `Quantity` * ? WHERE `sparepartdelivery`.`Code` = ? AND `Recieved` = 0");
            pstm.setDouble(1, cost);
            pstm.setString(2, code);
            pstm.executeUpdate();
            pstm.close();
        }catch (SQLException ex) {
            ex.printStackTrace();
        }
    }
    
    /**
     * Finds total amount of low stock levels. This amount is used to alert the
     * receptionist.
     * @return 
     */
    public int lowStockAmount(){
        int amount = 0;
        try {
            Statement stm = conn.createStatement();
            //ResultSet rs = stm.executeQuery("SELECT * FROM `vehicles` WHERE `LastMoTCheck` < date_sub(now(), interval 1 month)");
            ResultSet rs = stm.executeQuery("SELECT COUNT(*) FROM `sparepartstock` WHERE `Stock_Level` < `Threshold_Level`");
            while(rs.next()){
                amount = rs.getInt("COUNT(*)");
            }
        } catch (SQLException ex) {
            Logger.getLogger(ReceptionistController.class.getName()).log(Level.SEVERE, null, ex);
        }
           return amount;
    }
    
    /**
     * Allows the receptionist to search a job by customers name
     * @param name customers name
     * @return 
     */
    public ArrayList searchJobByName(String name){
    //SELECT * FROM `jobs` INNER JOIN vehicles ON jobs.RegNo=vehicles.RegNo INNER JOIN customers ON vehicles.CustomerID=customers.CustomerID WHERE 1
        jobData.clear();
        try {
            Statement st = conn.createStatement();
            ResultSet rs = st.executeQuery("SELECT * FROM `jobs` INNER JOIN vehicles ON jobs.RegNo=vehicles.RegNo INNER JOIN customers ON vehicles.CustomerID=customers.CustomerID WHERE name LIKE \"%"+name+"%\" ");
            while(rs.next()){
                jobData.add(new Job(rs.getInt("JobID"), rs.getString("RegNo"), 
                        rs.getString("Work_required"), rs.getString("JobStatus"), 
                        rs.getString("BusinessType"),
                        rs.getTime("Duration"), rs.getDate("Date_creation")));
            }
            rs.close();
            st.close();
        } catch (SQLException ex) {
            ex.printStackTrace();
        } 
        return jobData;
    }
    
    /**
     * Generates an invoice for a selected job, and creates the invoice PDF in invoices folder
     * A check is made to see if invoice already exists, if this check is passed all cost information is
     * calculated and placed in the invoices table.
     * @param jobID
     * @param regNo
     * @param bustype 
     */
    public void generateInvoice(int jobID, String regNo, String bustype){
        double stockCost = 0;
        double labourCost = 0;
        double total;
        double VAT = 0;
        double grandTotal;
        try {
            Statement st = conn.createStatement();
            ResultSet rs = st.executeQuery("SELECT COUNT(*) FROM `invoices` WHERE `JobID` ="+jobID);

            while(rs.next()){
                int exists = rs.getInt("COUNT(*)");
                if(exists > 0){
                    JOptionPane.showMessageDialog(null,
                        "Invoice for this job already exists!",
                        "Operation cancelled out",
                        JOptionPane.WARNING_MESSAGE);
                    return;
                }
            }

            st = conn.createStatement();
            rs = st.executeQuery("SELECT Sum(sparepartstock.price*sparepartused.used) AS `stockCost` "
                    + "FROM `sparepartused` INNER JOIN sparepartstock "
                    + "ON sparepartused.Code=sparepartstock.Code "
                    + "INNER JOIN jobs "
                    + "ON sparepartused.JobID=jobs.JobID "
                    + "WHERE jobs.JobID ="+jobID);

            while(rs.next()){
                stockCost = rs.getDouble("stockCost");
            }
            
            rs = st.executeQuery("SELECT (labour_cost)*(Sum(TIME_TO_SEC(Duration)/3600)) AS `staffCost` "
                    + "FROM `tasks` "
                    + "INNER JOIN pendingjobs "
                    + "ON pendingjobs.JobID = tasks.JobID "
                    + "INNER JOIN staffaccounts "
                    + "ON pendingjobs.StaffID = staffaccounts.ID "
                    + "WHERE tasks.JobID ="+jobID);
            
            while(rs.next()){
                labourCost = rs.getDouble("staffCost");
            }
            total = labourCost+stockCost;
            
            rs = st.executeQuery("SELECT * FROM `config` WHERE `ConfigName` = \"VAT\"");
            
            while(rs.next()){
                VAT = rs.getDouble("Value");
            }
            
            PreparedStatement pst = conn.prepareStatement("SELECT * FROM businesstypes WHERE type = ?");
            pst.setString(1, bustype);
            rs = pst.executeQuery();
            double flatrate = 0;
            while(rs.next()){
                flatrate = rs.getInt("flatrate");
            }
            VAT = VAT * total;
            grandTotal = VAT + total + flatrate;
            rs.close();
            st.close();
            
            PreparedStatement pstm = conn.prepareStatement("INSERT INTO `invoices` "
                    + "(`InvoiceID`, `JobID`, `StockAmount`, `LabourCost`, `Total`,`VAT`, `GrandTotal`, `Reminder`, `initalDate`, `lastDateReminder`, `paid`) "
                    + "VALUES (NULL, ?, ?, ?, ?, ?, ?, ?, ?, ?,0)");
            pstm.setInt(1, jobID);
            pstm.setDouble(2, stockCost);
            pstm.setDouble(3, labourCost);
            pstm.setDouble(4, total);
            pstm.setDouble(5, VAT);
            pstm.setDouble(6, grandTotal);
            pstm.setInt(7, 0);
            Date date = new Date(new java.util.Date().getTime());
            pstm.setDate(8, date);
            pstm.setDate(9, date);
            pstm.executeUpdate();
            pstm.close();
            
            JasperReport jasperMasterReport = JasperCompileManager.compileReport(System.getProperty("user.dir")+"\\reporttemplates\\Invoice.jrxml");
            Map paraMap = new HashMap();
            paraMap.put("jobID",jobID);
            paraMap.put("SUBREPORT_DIR",System.getProperty("user.dir")+"\\reporttemplates\\");
            String fileDate = LocalDateTime.now().format(DateTimeFormatter.ofPattern("dd-MM-YYYY"));
            JasperPrint jp = JasperFillManager.fillReport(jasperMasterReport, paraMap, conn);
            JasperExportManager.exportReportToPdfFile(jp,
                  System.getProperty("user.dir")+"\\invoices\\"+regNo+fileDate+".pdf");
            JOptionPane.showMessageDialog(null,
                    "Invoice generated file: "+regNo+fileDate+".pdf has been created",
                    "Invoice created",
                    JOptionPane.INFORMATION_MESSAGE);

            JasperPrintManager.printReport(jp, true); //print report
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
    
    /**
     * Allows the receptionist to add a vehicle to a customer
     * @param regNo - reg number
     * @param cusID - selected customer id
     * @param engNo - engine number
     * @param colour - vehicle colour
     * @param make - vehicle make
     * @param model - vehicle model
     * @param chassNo - vehicle chassis number
     * @param year - vehicle year
     * @param motDate - last mot date 
     */
    public void addVehicle(String regNo, int cusID, int engNo, String colour, String make, String model, int chassNo, String year, Date motDate){
        if(!getChecksum(conn,"Vehicles",vehChecksum)){
            JOptionPane.showMessageDialog(null,
                    "The previous action has failed as the table contains old records."
                            + "\nThe data will now be updated!",
                    "Table has been altered!",
            JOptionPane.ERROR_MESSAGE);
        }else{
            try {
                PreparedStatement pstm = conn.prepareStatement("INSERT INTO `vehicles` "
                        + "(`RegNo`, `CustomerID`, `EngSerial`, `Colour`, `Make`, `Model`, `ChassisNo`, `Year`, `LastMoTCheck`) "
                        + "VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?);");
                pstm.setString(1, regNo);
                pstm.setInt(2, cusID);
                pstm.setInt(3, engNo);
                pstm.setString(4, colour);
                pstm.setString(5, make);
                pstm.setString(6, model);
                pstm.setInt(7, chassNo);
                pstm.setString(8, year);
                pstm.setDate(9, motDate);
                pstm.executeUpdate();
                pstm.close();
                JOptionPane.showMessageDialog(null,
                    "Vehicle "+regNo+" added!",
                    "Vehicle created",
                    JOptionPane.INFORMATION_MESSAGE);
            } catch(MySQLIntegrityConstraintViolationException e){
                    JOptionPane.showMessageDialog(null,
                        "Registration Number already exists",
                        "Not added",
                        JOptionPane.WARNING_MESSAGE);
            } catch (SQLException ex) {
                ex.printStackTrace();
            } catch (NullPointerException e){
                    JOptionPane.showMessageDialog(null,
                        "Vehicle has not been added!",
                        "Operation cancelled out",
                        JOptionPane.WARNING_MESSAGE);
            }
        }
    }
    
    /**
     * Allows the recep to process a invoice and allow a customer to pay
     * @param invoiceID - selected invoice
     * @param card - card number
     * @param expire - expire date
     */
    public void invoicePaid(int invoiceID, String card, String expire){
        if(!getChecksum(conn,"invoices",invoiceChecksum)){
            JOptionPane.showMessageDialog(null,
                    "The previous action has failed as the table contains old records."
                            + "\nThe data will now be updated!",
                    "Table has been altered!",
            JOptionPane.ERROR_MESSAGE);
        }else{
            try {
                PreparedStatement pstm = conn.prepareStatement("UPDATE `invoices` SET `Paid` = ? WHERE `invoices`.`InvoiceID` = ?");
                pstm.setBoolean(1, true);
                pstm.setInt(2, invoiceID);
                pstm.executeUpdate();
                pstm = conn.prepareStatement("INSERT INTO `jobpayment` (`PaymentID`, `InvoiceID`, `Card_no`, `expires`, `payment_made`) VALUES (NULL, ?, ?, ?, CURRENT_DATE())");
                pstm.setInt(1, invoiceID);
                pstm.setString(2, card);
                pstm.setString(3, expire);
                pstm.executeUpdate();
                pstm.close();
                JOptionPane.showMessageDialog(null,
                    "Invoice is now paid!",
                    "Invoice paid",
                    JOptionPane.INFORMATION_MESSAGE);
            }catch (SQLException ex) {
                ex.printStackTrace();
            } catch (NullPointerException e){
                    JOptionPane.showMessageDialog(null,
                        "Invoice not paid!",
                        "Operation cancelled out",
                        JOptionPane.WARNING_MESSAGE);
            }
        }
    }
    
    /**
     * Allows the recep to sell a spare part to a customer.
     * The method will insert the information about the stock sold in table,
     * then method will then drop the stock levels to the correct level
     * then finally generate a invoice of the item that has been sold.
     * @param code - stock code
     * @param amount - stock amount
     * @param price - price
     * @param cusID - select customer purchasing
     * @param purchased - amount purchased
     */
    public void purchaseStockPart(String code, int amount, double price, int cusID, int purchased){
        if(!getChecksum(conn,"SparePartStock",stockChecksum)){
            JOptionPane.showMessageDialog(null,
                    "The previous action has failed as the table contains old records."
                            + "\nThe data will now be updated!",
                    "Table has been altered!",
            JOptionPane.ERROR_MESSAGE);
        }else{
            try {
                PreparedStatement pstm = conn.prepareStatement("UPDATE `sparepartstock` "
                        + "SET `Stock_Level` = ?, `Stock_Cost` = ? "
                        + "WHERE `sparepartstock`.`Code` = ?  ");
                pstm.setInt(1, amount);
                pstm.setDouble(2, amount*price);
                pstm.setString(3, code);
                pstm.executeUpdate();
                pstm = conn.prepareStatement("INSERT INTO `sparepartsold` (`SoldID`, `Code`, `CustomerID`, `Amount`, `DatePurchased`) "
                        + "VALUES (NULL, ?, ?, ?, CURRENT_DATE())");
                pstm.setString(1, code);
                pstm.setInt(2, cusID);
                pstm.setInt(3, purchased);
                pstm.executeUpdate();
                
                //need to get the autogenerated soldid to put in the invoicetable
                PreparedStatement pst = conn.prepareStatement("SELECT `SoldID` FROM `sparepartsold` WHERE `Code` = ? AND `CustomerID` = ? AND `Amount` = ? AND `DatePurchased` = CURRENT_DATE() LIMIT 1"); 
                pst.setString(1, code);
                pst.setInt(2, cusID);
                pst.setInt(3, purchased);
                ResultSet rs = pst.executeQuery();
                int soldID = 0;
                while(rs.next()){
                    soldID = rs.getInt("SoldID");
                } 
                
                //get VAT
                double VAT = 0;
                double total = price*purchased;
                double grandTotal;
                Statement st = conn.createStatement();
                rs = st.executeQuery("SELECT * FROM `config` WHERE `ConfigName` = \"VAT\"");
            
                while(rs.next()){
                    VAT = rs.getDouble("Value");
                }

                VAT = VAT * total;
                grandTotal = VAT + total;
                rs.close();
                st.close();
                
                //create the invoice in table
                pstm = conn.prepareStatement("INSERT INTO `purchasedpartinvoices` "
                            + "(`pInvoiceID`, `SoldID`, `StockAmount`, `VAT`, `GrandTotal`, `Reminder`, `initalDate`, `lastDateReminder`, `Paid`)"
                            + " VALUES (NULL, ?, ?, ?, ?, 0, CURRENT_DATE(), CURRENT_DATE(), 0)");
                pstm.setInt(1, soldID);
                pstm.setDouble(2, total);
                pstm.setDouble(3, VAT);
                pstm.setDouble(4, grandTotal);
                pstm.executeUpdate();
                pstm.close();
                
                
                JasperReport jasperMasterReport = JasperCompileManager.compileReport(System.getProperty("user.dir")+"\\reporttemplates\\purchasedPartInvoice.jrxml");
                Map paraMap = new HashMap();
                paraMap.put("soldID",soldID);
                paraMap.put("SUBREPORT_DIR",System.getProperty("user.dir")+"\\reporttemplates\\");
                String fileDate = LocalDateTime.now().format(DateTimeFormatter.ofPattern("dd-MM-YYYY"));
                JasperPrint jp = JasperFillManager.fillReport(jasperMasterReport, paraMap, conn);
                JasperExportManager.exportReportToPdfFile(jp,
                      System.getProperty("user.dir")+"\\spinvoices\\"+soldID+fileDate+".pdf");
                JOptionPane.showMessageDialog(null,
                        "Invoice generated file: "+soldID+fileDate+".pdf has been created",
                        "Invoice created",
                        JOptionPane.INFORMATION_MESSAGE);

                JasperPrintManager.printReport(jp, true); //print report
            } catch (SQLException ex) {
                ex.printStackTrace();
            } catch (NullPointerException e){
                    JOptionPane.showMessageDialog(null,
                        "Job has not been assigned!",
                        "Operation cancelled out",
                        JOptionPane.WARNING_MESSAGE);
            } catch (JRException ex) {
                Logger.getLogger(ReceptionistController.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }   
}
