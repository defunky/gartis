/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controllers;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Arrays;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * This class is inherited by all controllers, the class is used to generate a checksum
 * of a selected table and allows you to compare two checksums values. This was done to
 * allow concurrency in the system as if the checksums differ than the data is out of date
 * 
 * @author Abdalrahmane
 */
public abstract class ActorController {
    //private byte[] checkSum; //used to check if data is most recent
    //protected String checkSumTableName;

    /**
     * This function is used as apart of concurrency system. It will generate a checksum
     * value with the information in the database and store it. This can be used to do a check
     * against the most recent checksum to see if the table has been altered.
     * @param conn - sql connection
     * @param checkSumTableName - the name of the table you wish to get the checksum for
     * @return 
     */
    public byte[] setChecksum(Connection conn, String checkSumTableName){
        byte[] checkSum = null;
        try {
            Statement st = conn.createStatement();
            ResultSet rs = st.executeQuery("CHECKSUM TABLE "+checkSumTableName+";");
            while(rs.next()){
                checkSum = rs.getBytes("Checksum");
            }
            rs.close();
            st.close();
        } catch (SQLException ex) {
            Logger.getLogger(ActorController.class.getName()).log(Level.SEVERE, null, ex);
        }
        return checkSum;
    }
    

    /**
     * This method returns a boolean value, which depends on the stored checksum value
     * and the newest checksum value of the table. A comparision is made and depending on the result
     * true or false is sent back
     * @param conn - sql connection
     * @param checkSumTableName - the tablename that you wish to compare it too
     * @param checkSum - the last clientside stored checksum value
     * @return 
     */
    public boolean getChecksum(Connection conn, String checkSumTableName, byte[] checkSum){
        byte[] newCheckSum = checkSum;
        try {
            Statement st = conn.createStatement();
            ResultSet rs = st.executeQuery("CHECKSUM TABLE "+checkSumTableName+";");
            while(rs.next()){
                newCheckSum = rs.getBytes("Checksum");
            }
            rs.close();
            st.close();
        } catch (SQLException ex) {
            Logger.getLogger(ActorController.class.getName()).log(Level.SEVERE, null, ex);
        }
        //return newCheckSum;
        return Arrays.equals(checkSum,newCheckSum);
    }
}
