/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controllers;

import Entities.Customer;
import Entities.Invoice;
import Entities.Staff;
import Entities.Vehicle;
import GUI.AdminGUI;
import com.mysql.jdbc.exceptions.jdbc4.MySQLIntegrityConstraintViolationException;
import java.io.File;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.*;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.view.JasperViewer;
import java.util.regex.Pattern;
import javax.swing.JOptionPane;
import net.sf.jasperreports.engine.*;

/**
 * The Franchisee controller. This class contains all the complex logic
 * that the Franchisee account can do. This class is used by FranchiseeGUI.
 * @author Abdalrahmane
 */
public class FranchiseeController extends ActorController{
    private Connection conn;
    private DatabaseConnection dbc;
    private ArrayList<Customer> cusData;
    private ArrayList<Vehicle> vehData;
    private ArrayList<Staff> staffData; //used in reports to get mechanics
    private ArrayList<String> busTypeData; //business types used for report selection
    private ArrayList<Invoice> invoiceData;
    private byte[] customerCheckSum;
    private byte[] vehicleCheckSum;
    public FranchiseeController(DatabaseConnection dbc) {
        this.dbc = dbc;
        conn = dbc.getConnection();
        cusData = new ArrayList<>();
        vehData = new ArrayList<>();
        staffData = new ArrayList<>();
        busTypeData = new ArrayList<>();
        invoiceData = new ArrayList<>();
        customerCheckSum = setChecksum(conn,"Customers");
        vehicleCheckSum = setChecksum(conn, "Vehicles");
        getCusInfo();
    }

    /**
     * Populate an arraylist with all customer information from the customer
     * table in the database
     */
    private void getCusInfo(){
        cusData.clear();
        try {
            Statement st = conn.createStatement();
            ResultSet rs = st.executeQuery("SELECT * FROM customers");

            while(rs.next()){
                cusData.add(new Customer(rs.getInt("CustomerID"), rs.getString("Name"), 
                        rs.getString("telephone"), rs.getString("mobile"), 
                        rs.getString("email"), rs.getString("address"), rs.getString("Street"), rs.getString("locality"),
                        rs.getString("city"), rs.getString("postcode"),rs.getString("Additional_Notes"), rs.getString("customerType")));
            }
            rs.close();
            st.close();
        } catch (SQLException ex) {
            ex.printStackTrace();
        }    
    }
    
    /**
     * Populates the arraylist with the vehicles associated to a customer
     * @param cusID the selected customer
     */
    public void getVehInfo(int cusID){
        vehData.clear();
        try {
            PreparedStatement pstm = conn.prepareStatement("SELECT * FROM `Vehicles` WHERE CustomerID = ?");
            pstm.setInt(1, cusID);
            ResultSet rs = pstm.executeQuery();
            while(rs.next()){
                vehData.add(new Vehicle(rs.getString("RegNo"), rs.getInt("EngSerial"), 
                        rs.getInt("ChassisNo"),rs.getString("Colour"), 
                        rs.getString("Make"), rs.getString("Model"), rs.getString("Year"),
                        rs.getDate("LastMoTCheck")));
            }
            rs.close();
            pstm.close();
        } catch (SQLException ex) {
            ex.printStackTrace();
        }    
    }
    
    /**
     * Populate an arraylist filled with staff accounts that has type mechanic or foreperson
     * this is used to display the list of workers to generate the report needed
     */
    private void getStaffInfo(){
        staffData.clear();
        try {
            Statement st = conn.createStatement();
            ResultSet rs = st.executeQuery("SELECT * FROM staffAccounts WHERE `Type` = \"Mechanic\" OR Type = \"FOREPERSON\"");
            while(rs.next()){
                staffData.add(new Staff(rs.getInt("ID"), rs.getString("username"), 
                        rs.getBytes("password"), rs.getString("type"), 
                        rs.getString("surname"), rs.getString("name"),
                        rs.getDate("date_creation"), rs.getDouble("labour_cost")));
            }
            rs.close();
            st.close();
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
    }

    /**
     * Return the staff arraylist
     * @return 
     */
    public ArrayList getStaffData(){
        getStaffInfo(); //get most updated version of table
        return staffData;
    }
    /**
     * Return the customer arraylist and update the checksum to the latest one
     * @return 
     */
    public ArrayList getCusData(){
        customerCheckSum = setChecksum(conn,"Customers"); //set the newest checksum
        getCusInfo(); //get most updated version of table
        return cusData;
    }
    /**
     * Return the vehicle arraylist that is linked to the selected customer
     * @param cusID - selected customer
     * @return 
     */
    public ArrayList getVehData(int cusID){
        vehicleCheckSum = setChecksum(conn,"Vehicles");
        getVehInfo(cusID);
        return vehData;
    }
    
    /**
     * Populates an arraylist with business types that are located in the 
     * business type table.
     * This is data is used to generate specific reports that needs a selected
     * business type
     */
    public void getBusInfo(){
        busTypeData.clear();
        try {
            Statement st = conn.createStatement();
            ResultSet rs = st.executeQuery("SELECT * FROM businesstypes");
            while(rs.next()){
                busTypeData.add(rs.getString("Type"));
            }
            rs.close();
            st.close();
        } catch (SQLException ex) {
            ex.printStackTrace();
        }    
    }
        
    /**
     * Returns the arraylist populated with business types
     * @return 
     */
    public ArrayList getBusData(){
        getBusInfo();
        return busTypeData;
    }
    
    /**
     * Populates arraylist with all invoice details stored in the invoices
     * table in the database
     */
    public void getInvoiceInfo(){
        invoiceData.clear();
        try {
            Statement st = conn.createStatement();
            ResultSet rs = st.executeQuery("SELECT * FROM invoices");
            while(rs.next()){
                invoiceData.add(new Invoice(rs.getInt("invoiceid"), rs.getInt("jobID"), rs.getInt("reminder"),
                        rs.getDouble("stockAmount"), rs.getDouble("labourcost"), 
                        rs.getDouble("vat"), rs.getDouble("vat"), rs.getDouble("grandtotal"), 
                        rs.getDate("initalDate"), rs.getDate("lastDateReminder"), rs.getBoolean("paid")));
            }
            rs.close();
            st.close();
        } catch (SQLException ex) {
            ex.printStackTrace();
        }    
    }
    /**
     * Returns arraylist filled with the invoice details
     * @return 
     */
    public ArrayList getInvoiceData(){
        getInvoiceInfo();
        return invoiceData;
    }
    
    
    /**
     * The method allows the franchisee to create a customer in the customer table.
     * The method will first check if the table is outdated before creating the user
     * if the check is passed the customer details is then inserted in the table
     * using the information passed in the parameters. Then finally alerts the
     * user that the customer has been added
     * @param fname - customers name
     * @param tele - customer tele
     * @param mobile - customer mobile
     * @param email - customer email
     * @param address - customer address
     * @param street - customer street
     * @param locality - customer locality
     * @param city - customer city
     * @param pcode - customer post code
     * @param note - additional notes related to the customer
     * @param type - the customer account type
     */
    public void createCustomer(String fname, String tele, String mobile, String email, String address, String street, String locality, String city, String pcode, String note, String type ){
        if(!getChecksum(conn,"Customers",customerCheckSum)){
            JOptionPane.showMessageDialog(null,
                    "The previous action has failed as the table contains old records."
                            + "\nThe data will now be updated!",
                    "Table has been altered!",
            JOptionPane.ERROR_MESSAGE);
        }else{
            try {
                PreparedStatement pstm = conn.prepareStatement("INSERT INTO `customers` "
                        + "(`CustomerID`, `Name`, `Telephone`, `Mobile`, `Email`, `Address`, `Street`, `Locality`, `"
                        + "City`, `PostCode`, `Additional_Notes`, `CustomerType`, `Discount_ID`) "
                        + "VALUES (NULL, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, NULL)");
                pstm.setString(1, fname);
                pstm.setString(2, tele);
                pstm.setString(3, mobile);
                pstm.setString(4, email);
                pstm.setString(5, address);
                pstm.setString(6, street);
                pstm.setString(7, locality);
                pstm.setString(8, city);
                pstm.setString(9, pcode);
                pstm.setString(10, note);
                pstm.setString(11, type);
                pstm.executeUpdate();
                pstm.close();
                JOptionPane.showMessageDialog(null,
                    "Customer "+fname+" added!",
                    "Customer created",
                    JOptionPane.INFORMATION_MESSAGE);
            } catch (SQLException ex) {
                ex.printStackTrace();
            } catch (NullPointerException e){
                    JOptionPane.showMessageDialog(null,
                        "Customer has not been added!",
                        "Operation cancelled out",
                        JOptionPane.WARNING_MESSAGE);
            }
        }
    }
    /**
     * The method allows the franchisee to alter an existing customer 
     * The method first checks if the the table checksum has altered
     * once passed, the method alters the selected customer 
     * with the information provided
     * @param fname - customers name
     * @param tele - customer tele
     * @param mobile - customer mobile
     * @param email - customer email
     * @param address - customer address
     * @param street - customer street
     * @param locality - customer locality
     * @param city - customer city
     * @param pcode - customer post code
     * @param note - additional notes related to the customer
     * @param type - the customer account type
     * @param cusID - selected customer id
     */
    public void alterCustomer(String fname, String tele, String mobile, String email, String address, String street, String locality, String city, String pcode, String note, String type, int cusID){
        if(!getChecksum(conn,"Customers",customerCheckSum)){
            JOptionPane.showMessageDialog(null,
                    "The previous action has failed as the table contains old records."
                            + "\nThe data will now be updated!",
                    "Table has been altered!",
            JOptionPane.ERROR_MESSAGE);
        }else{
            try {
                PreparedStatement pstm = conn.prepareStatement("UPDATE `customers` SET "
                        + "`Name` = ?, `Telephone` = ?, `Mobile` = ?, `Email` = ?, `Address` = ?, `Street` = ?, "
                        + "`Locality` = ?, `City` = ?, `PostCode` = ?, `Additional_Notes` = ?, `CustomerType` = ? "
                        + "WHERE `customers`.`CustomerID` = ?");
                pstm.setString(1, fname);
                pstm.setString(2, tele);
                pstm.setString(3, mobile);
                pstm.setString(4, email);
                pstm.setString(5, address);
                pstm.setString(6, street);
                pstm.setString(7, locality);
                pstm.setString(8, city);
                pstm.setString(9, pcode);
                pstm.setString(10, note);
                pstm.setString(11, type);
                pstm.setInt(12, cusID);
                pstm.executeUpdate();
                pstm.close();
                JOptionPane.showMessageDialog(null,
                    "Customer "+fname+" Altered!",
                    "Customer Altered",
                    JOptionPane.INFORMATION_MESSAGE);
            } catch (SQLException ex) {
                ex.printStackTrace();
            } catch (NullPointerException e){
                    JOptionPane.showMessageDialog(null,
                        "Customer has not been added!",
                        "Operation cancelled out",
                        JOptionPane.WARNING_MESSAGE);
            }
        }
    }
    
    /**
     * Allows the franchisee to delete a selected customer account
     * @param cusID - selected customer ID
     */
    public void deleteCustomer(int cusID){
        if(!getChecksum(conn,"Customers",customerCheckSum)){
            JOptionPane.showMessageDialog(null,
                    "The previous action has failed as the table contains old records."
                            + "\nThe data will now be updated!",
                    "Table has been altered!",
            JOptionPane.ERROR_MESSAGE);
        }else{
            try {
                PreparedStatement pstm = conn.prepareStatement("DELETE FROM Customers WHERE CustomerID=?;");
                pstm.setInt(1, cusID);
                pstm.executeUpdate();
                pstm.close();
                JOptionPane.showMessageDialog(null,
                    "Customer has been deleted!",
                    "Customer Deleted",
                    JOptionPane.INFORMATION_MESSAGE);
            } catch (SQLException ex) {
                    JOptionPane.showMessageDialog(null,
                        "Customer has not been deleted!",
                        "Operation cancelled out",
                        JOptionPane.WARNING_MESSAGE);
            }
        }
    }

    /**
     * Allows the franchisee to create a vehicle that is linked to a customer
     * The method will check if the vehicles checksum has changed if the pass is checked
     * the vehicle is added with the information provided in the parameters.
     * @param regNo - vehicle reg no
     * @param cusID - the selected customer id
     * @param engNo - vehicle engine number
     * @param colour - vehicle colour
     * @param make - vehicle make
     * @param model - vehicle model
     * @param chassNo - vehicle chassis number
     * @param year - vehicle year
     * @param motDate - vehicle last mot date
     */
    public void addVehicle(String regNo, int cusID, int engNo, String colour, String make, String model, int chassNo, String year, Date motDate){
        if(!getChecksum(conn,"Vehicles",vehicleCheckSum)){
            JOptionPane.showMessageDialog(null,
                    "The previous action has failed as the table contains old records."
                            + "\nThe data will now be updated!",
                    "Table has been altered!",
            JOptionPane.ERROR_MESSAGE);
        }else{
            try {
                PreparedStatement pstm = conn.prepareStatement("INSERT INTO `vehicles` "
                        + "(`RegNo`, `CustomerID`, `EngSerial`, `Colour`, `Make`, `Model`, `ChassisNo`, `Year`, `LastMoTCheck`) "
                        + "VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?);");
                pstm.setString(1, regNo);
                pstm.setInt(2, cusID);
                pstm.setInt(3, engNo);
                pstm.setString(4, colour);
                pstm.setString(5, make);
                pstm.setString(6, model);
                pstm.setInt(7, chassNo);
                pstm.setString(8, year);
                pstm.setDate(9, motDate);
                pstm.executeUpdate();
                pstm.close();
                JOptionPane.showMessageDialog(null,
                    "Vehicle "+regNo+" added!",
                    "Vehicle created",
                    JOptionPane.INFORMATION_MESSAGE);
            } catch(MySQLIntegrityConstraintViolationException e){
                    JOptionPane.showMessageDialog(null,
                        "Registration Number already exists",
                        "Not added",
                        JOptionPane.WARNING_MESSAGE);
            } catch (SQLException ex) {
                ex.printStackTrace();
            } catch (NullPointerException e){
                    JOptionPane.showMessageDialog(null,
                        "Vehicle has not been added!",
                        "Operation cancelled out",
                        JOptionPane.WARNING_MESSAGE);
            }
        }
    }
    
    /**
     * Allows the franchisee to alter a vehicle details
     * The method will check if the vehicles checksum has changed if the pass is checked
     * the vehicle is altered with the information provided in the parameters.
     * @param regNo - vehicle reg no
     * @param vehID - the selected vehicle id
     * @param engNo - vehicle engine number
     * @param colour - vehicle colour
     * @param make - vehicle make
     * @param model - vehicle model
     * @param chassNo - vehicle chassis number
     * @param year - vehicle year
     * @param motDate - vehicle last mot date
     */
    public void alterVehicle(String regNo, String vehID, int engNo, String colour, String make, String model, int chassNo, String year, Date motDate){
        if(!getChecksum(conn,"Vehicles",vehicleCheckSum)){
            JOptionPane.showMessageDialog(null,
                    "The previous action has failed as the table contains old records."
                            + "\nThe data will now be updated!",
                    "Table has been altered!",
            JOptionPane.ERROR_MESSAGE);
        }else{
            try {
                PreparedStatement pstm = conn.prepareStatement("UPDATE `vehicles` "
                        + "SET `RegNo` = ?, `EngSerial` = ?, `Colour` = ?, `Make` = ?, `Model` = ?, `ChassisNo` = ?, "
                        + "`Year` = ?, `LastMoTCheck` = ? "
                        + "WHERE `vehicles`.`RegNo` = ?");
                pstm.setString(1, regNo);
                pstm.setInt(2, engNo);
                pstm.setString(3, colour);
                pstm.setString(4, make);
                pstm.setString(5, model);
                pstm.setInt(6, chassNo);
                pstm.setString(7, year);
                pstm.setDate(8, motDate);
                pstm.setString(9, vehID);
                pstm.executeUpdate();
                pstm.close();
                JOptionPane.showMessageDialog(null,
                    "Vehicle "+regNo+" has been altered!",
                    "Vehicle Altered",
                    JOptionPane.INFORMATION_MESSAGE);
            } catch(MySQLIntegrityConstraintViolationException e){
                    JOptionPane.showMessageDialog(null,
                        "Registration Number already exists",
                        "Not added",
                        JOptionPane.WARNING_MESSAGE);
            } catch (SQLException ex) {
                ex.printStackTrace();
            } catch (NullPointerException e){
                    JOptionPane.showMessageDialog(null,
                        "Vehicle has not been altered!",
                        "Operation cancelled out",
                        JOptionPane.WARNING_MESSAGE);
            }
        }
    }
    
    /**
     * Allows the franchisee to delete a vehicle from the database,
     * checks if the table has been altered before deleting.
     * @param vehID - selected vehicle id
     */
    public void deleteVehicle(String vehID){
        if(!getChecksum(conn,"Vehicles",vehicleCheckSum)){
            JOptionPane.showMessageDialog(null,
                    "The previous action has failed as the table contains old records."
                            + "\nThe data will now be updated!",
                    "Table has been altered!",
            JOptionPane.ERROR_MESSAGE);
        }else{
            try {
                PreparedStatement pstm = conn.prepareStatement("DELETE FROM Vehicles WHERE RegNo=?;");
                pstm.setString(1, vehID);
                pstm.executeUpdate();
                pstm.close();
                JOptionPane.showMessageDialog(null,
                    "Vehicle has been deleted!",
                    "Vehicle Deleted",
                    JOptionPane.INFORMATION_MESSAGE);
            } catch (SQLException ex) {
                    JOptionPane.showMessageDialog(null,
                        "Vehicle has not been deleted!",
                        "Operation cancelled out",
                        JOptionPane.WARNING_MESSAGE);
                    ex.printStackTrace();
            }
        }
    }
    
//    public int checkDueMot(){
//        int amount = 0;
//        try {
//            Statement stm = conn.createStatement();
//            //ResultSet rs = stm.executeQuery("SELECT * FROM `vehicles` WHERE `LastMoTCheck` < date_sub(now(), interval 1 month)");
//            ResultSet rs = stm.executeQuery("SELECT COUNT(*) FROM `vehicles` WHERE `LastMoTCheck` < date_sub(now(), interval 358 day)");
//            while(rs.next()){
//                amount = rs.getInt("COUNT(*)");
//            }
//        } catch (SQLException ex) {
//            Logger.getLogger(FranchiseeController.class.getName()).log(Level.SEVERE, null, ex);
//        }
//           return amount;
//    }
    
    /**
     * The method creates an arraylist and calculates all late payments that are
     * over a month old from the inital invoice date.
     * This is then displayed once the franchisee accepts the late payment alert
     * when logged in
     * @return 
     */
    public ArrayList checklatePayments(){
        ArrayList<Invoice> latePayData = new ArrayList<>();
        try {
            Statement stm = conn.createStatement();
            //ResultSet rs = stm.executeQuery("SELECT * FROM `vehicles` WHERE `LastMoTCheck` < date_sub(now(), interval 1 month)");
            ResultSet rs = stm.executeQuery("SELECT * FROM `invoices` WHERE `initalDate` < date_sub(now(), interval 1 month) AND `paid` = 0");
            
            while(rs.next()){
                latePayData.add(new Invoice(rs.getInt("invoiceid"), rs.getInt("jobID"), rs.getInt("reminder"),
                        rs.getDouble("stockAmount"), rs.getDouble("labourcost"), 
                        rs.getDouble("vat"), rs.getDouble("vat"), rs.getDouble("grandtotal"), 
                        rs.getDate("initalDate"), rs.getDate("lastDateReminder"), rs.getBoolean("paid")));
            }
        } catch (SQLException ex) {
            Logger.getLogger(FranchiseeController.class.getName()).log(Level.SEVERE, null, ex);
        }
            return latePayData;
    }
    
    /**
     * The method is called during the login of the Franchisee
     * The method checks the invoice table any unpaid invoices where the last date reminder
     * has passed a month. This will then retrieve the amount of reminders sent before, using this information
     * jasper report then generates a reminder notice (1st/2nd/final) and places the pdf in invoicereminder
     * folder.
     */
    public void generateInvoiceReminders(){
        int count = 0;
        try {
            Statement stm = conn.createStatement();
            //ResultSet rs = stm.executeQuery("SELECT * FROM `vehicles` WHERE `LastMoTCheck` < date_sub(now(), interval 1 month)");
            ResultSet rs = stm.executeQuery("SELECT * FROM `invoices` WHERE `lastDateReminder` < date_sub(now(), interval 1 month) AND `paid` = 0");
            while(rs.next()){
                count++;
                PreparedStatement pstm = conn.prepareStatement("UPDATE `invoices` "
                        + "SET `Reminder` = ?, `lastDateReminder` = CURRENT_DATE() "
                        + "WHERE invoiceid = ?  ");
                int reminderAmount = rs.getInt("Reminder");
                int invoiceID = rs.getInt("invoiceid");
                reminderAmount++;
                pstm.setInt(1, reminderAmount);
                pstm.setInt(2, invoiceID);
                
                pstm.executeUpdate();
                pstm.close();
                try{
                    String fileDate = LocalDateTime.now().format(DateTimeFormatter.ofPattern("dd-MM-YYYY"));
                    HashMap paraMap = new HashMap<>();
                    paraMap.put("invoiceID",invoiceID);
                    if(reminderAmount == 1){
                        JasperReport jasperMasterReport = JasperCompileManager.compileReport(System.getProperty("user.dir")+"\\reporttemplates\\InvoiceReminder.jrxml");
                        JasperPrint jp = JasperFillManager.fillReport(jasperMasterReport, paraMap, conn);
                        JasperExportManager.exportReportToPdfFile(jp,
                              System.getProperty("user.dir")+"\\invoicereminders\\firstreminderID"+invoiceID+"-"+fileDate+".pdf");
                        JasperPrintManager.printReport(jp, true); //print report
                    }else if(reminderAmount == 2){
                        JasperReport jasperMasterReport = JasperCompileManager.compileReport(System.getProperty("user.dir")+"\\reporttemplates\\InvoiceSecondReminder.jrxml");
                        JasperPrint jp = JasperFillManager.fillReport(jasperMasterReport, paraMap, conn);
                        JasperExportManager.exportReportToPdfFile(jp,
                              System.getProperty("user.dir")+"\\invoicereminders\\secondreminderID"+invoiceID+"-"+fileDate+".pdf");
                        JasperPrintManager.printReport(jp, true); //print report
                    }else if(reminderAmount == 3){
                        JasperReport jasperMasterReport = JasperCompileManager.compileReport(System.getProperty("user.dir")+"\\reporttemplates\\InvoiceFinalReminder.jrxml");
                        JasperPrint jp = JasperFillManager.fillReport(jasperMasterReport, paraMap, conn);
                        JasperExportManager.exportReportToPdfFile(jp,
                              System.getProperty("user.dir")+"\\invoicereminders\\finalreminderID"+invoiceID+"-"+fileDate+".pdf");
                        JasperPrintManager.printReport(jp, true); //print report
                    }
                }catch(Exception e){
                    e.printStackTrace();
                }
            }
        } catch (SQLException ex) {
            Logger.getLogger(FranchiseeController.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        if(count > 0){
            JOptionPane.showMessageDialog(null,
                    ""+count+" Invoice reminders have been created",
                    "Invoice reminders generated",
                    JOptionPane.WARNING_MESSAGE);
        }
    
    }
    
    /**
     * The method will first check if MoT check up date is within 7 days, if theres a checkup due
     * it will then check if theres been a job created for the vehicle in the last 7
     * if not it will create the job and generate the MoT reminder
     * @return 
     */
    public int checkDueMot(){
        int count = 0;
        String regNo;
        int cusID;
        try {
            Statement stm = conn.createStatement();
            ResultSet rs = stm.executeQuery("SELECT * FROM `vehicles` "
                    + "INNER JOIN Customers ON vehicles.customerID = customers.customerID "
                    + "WHERE DATEDIFF(CURRENT_DATE,`LastMoTCheck`) BETWEEN 365 AND 373");
            while(rs.next()){
                regNo = rs.getString("regNo");
                cusID = rs.getInt("CustomerID");
                //SELECT * FROM `jobs` WHERE DATEDIFF(CURRENT_DATE,`Date_creation`) BETWEEN 0 AND 7 // check if MoT job has been created for this vehicle in the past 7 days
                Statement stm2 = conn.createStatement();
                PreparedStatement pst = conn.prepareStatement("SELECT * FROM `jobs` WHERE DATEDIFF(CURRENT_DATE,`Date_creation`) BETWEEN 0 AND 7 "
                        + "AND BusinessType = \"MoT\" AND `RegNo` = ?"); 
                pst.setString(1, regNo);
                ResultSet rs2 = pst.executeQuery();
                if (!rs2.isBeforeFirst() ) {    //check if mot job exists
                    PreparedStatement pstm = conn.prepareStatement("INSERT INTO `jobs` "
                            + "(`JobID`, `RegNo`, `Work_Required`, `JobStatus`, `BusinessType`, `Duration`, `Date_creation`) "
                            + "VALUES (NULL, ?, ?, ?, ?, ?, CURRENT_DATE())");
                    pstm.setString(1, regNo);
                    pstm.setString(2, "MoT Annual Checkup");
                    pstm.setString(3, "IDLE");
                    pstm.setString(4, "MoT");
                    pstm.setTime(5, new Time(0,45,0));
                    pstm.executeUpdate();
                    pstm.close(); 
                    generateMoTReminder(cusID, regNo);
                    count++;
                } 
                rs2.close();
                stm2.close();
            }
            rs.close();
            stm.close();
        } catch (SQLException ex) {
            Logger.getLogger(FranchiseeController.class.getName()).log(Level.SEVERE, null, ex);
        }
           return count;
    }
    
    /**
     * Generates the PDF of a MoT reminder containing the vehicle/customer information
     * @param cusID - customer id
     * @param regNo - vehicle reg no
     */
    public void generateMoTReminder(int cusID, String regNo){
        try{
            JasperReport jasperMasterReport = JasperCompileManager.compileReport(System.getProperty("user.dir")+"\\reporttemplates\\MOTReminder.jrxml");
            String fileDate = LocalDateTime.now().format(DateTimeFormatter.ofPattern("dd-MM-YYYY"));
            HashMap paraMap = new HashMap<>();
            paraMap.put("cusID",cusID);
            paraMap.put("regNo",regNo);
            paraMap.put("SUBREPORT_DIR",System.getProperty("user.dir")+"\\reporttemplates\\");
            JasperPrint jp = JasperFillManager.fillReport(jasperMasterReport, paraMap, conn);
            JasperExportManager.exportReportToPdfFile(jp,
                  System.getProperty("user.dir")+"\\motreminders\\motreminder"+regNo+"-"+fileDate+".pdf");
            JasperPrintManager.printReport(jp, true); //print report
        }catch(Exception e){
            e.printStackTrace();
        }
    }
    
    /**
     * Generates the spare part report
     * Called in the menubar in franchiseeGUI
     */
    public void generateSpartPartReport(){
        try{
            JasperReport jasperMasterReport = JasperCompileManager.compileReport(System.getProperty("user.dir")+"\\reporttemplates\\sparepartsreport.jrxml");
            String fileDate = LocalDateTime.now().format(DateTimeFormatter.ofPattern("dd-MM-YYYY"));
            JasperPrint jp = JasperFillManager.fillReport(jasperMasterReport, null, conn);
            JasperExportManager.exportReportToPdfFile(jp,
                  System.getProperty("user.dir")+"\\reports\\sparepartsreport-"+fileDate+".pdf");
            JOptionPane.showMessageDialog(null,
                    "Report generated file: sparepartsreport-"+fileDate+".pdf has been created",
                    "Report created",
                    JOptionPane.INFORMATION_MESSAGE);
            JasperPrintManager.printReport(jp, true); //print report
        }catch(Exception e){
            e.printStackTrace();
        }
    }
    
    /**
     * Generates average price report per job, overall and per given mechanic
     * @param staffID - mech/foreperson id
     * @param surname - staff surname
     * @param name  - staff name
     */
    public void generateAvgPriceReport(int staffID, String surname, String name){
        try{
            JasperReport jasperMasterReport = JasperCompileManager.compileReport(System.getProperty("user.dir")+"\\reporttemplates\\avgJobPriceReport.jrxml");
            String fileDate = LocalDateTime.now().format(DateTimeFormatter.ofPattern("dd-MM-YYYY"));
            HashMap paraMap = new HashMap<>();
            paraMap.put("staffID",staffID);
            paraMap.put("SUBREPORT_DIR",System.getProperty("user.dir")+"\\reporttemplates\\");
            JasperPrint jp = JasperFillManager.fillReport(jasperMasterReport, paraMap, conn);
            JasperExportManager.exportReportToPdfFile(jp,
                  System.getProperty("user.dir")+"\\reports\\avgJobPriceReport-"+surname+name+"-"+fileDate+".pdf");
            JOptionPane.showMessageDialog(null,
                    "Report generated file: avgJobPriceReport-"+surname+name+"-"+fileDate+".pdf has been created",
                    "Report created",
                    JOptionPane.INFORMATION_MESSAGE);
            JasperPrintManager.printReport(jp, true); //print report
        }catch(Exception e){
            e.printStackTrace();
        }
    }
    
    /**
     * Generate number of vehicles booked in on a monthly basis, over ll and per service requested and type of customer.
     * @param busType - selected bus type
     * @param accType - select account type
     */
    public void generateVehBookReport(String busType, String accType){
        try{
            JasperReport jasperMasterReport = JasperCompileManager.compileReport(System.getProperty("user.dir")+"\\reporttemplates\\avgVehiclesBooked.jrxml");
            String fileDate = LocalDateTime.now().format(DateTimeFormatter.ofPattern("dd-MM-YYYY"));
            HashMap paraMap = new HashMap<>();
            paraMap.put("busType",busType);
            paraMap.put("accType",accType);
            paraMap.put("SUBREPORT_DIR",System.getProperty("user.dir")+"\\reporttemplates\\");
            JasperPrint jp = JasperFillManager.fillReport(jasperMasterReport, paraMap, conn);
            JasperExportManager.exportReportToPdfFile(jp,
                  System.getProperty("user.dir")+"\\reports\\vehBookedReport-"+busType+"-"+fileDate+".pdf");
            JOptionPane.showMessageDialog(null,
                    "Report generated file: vehBookedReport-"+busType+"-"+fileDate+".pdf",
                    "Report created",
                    JOptionPane.INFORMATION_MESSAGE);
            JasperPrintManager.printReport(jp, true); //print report
        }catch(Exception e){
            e.printStackTrace();
        }
    }
    
    /**
     * Generates average time to service vehicle overall and per service/repair request and given mechanic
     * @param staffID
     * @param busType 
     */
    public void generateAvgTimeReport(int staffID, String busType){
        try{
            JasperReport jasperMasterReport = JasperCompileManager.compileReport(System.getProperty("user.dir")+"\\reporttemplates\\avgServiceTime.jrxml");
            String fileDate = LocalDateTime.now().format(DateTimeFormatter.ofPattern("dd-MM-YYYY"));
            HashMap paraMap = new HashMap<>();
            paraMap.put("staffID",staffID);
            paraMap.put("busType",busType);
            paraMap.put("SUBREPORT_DIR",System.getProperty("user.dir")+"\\reporttemplates\\");
            JasperPrint jp = JasperFillManager.fillReport(jasperMasterReport, paraMap, conn);
            JasperExportManager.exportReportToPdfFile(jp,
                  System.getProperty("user.dir")+"\\reports\\avgServiceTime-"+busType+"-"+fileDate+".pdf");
            JOptionPane.showMessageDialog(null,
                    "Report generated file: \\reports\\avgServiceTime-"+busType+"-"+fileDate+".pdf has been created",
                    "Report created",
                    JOptionPane.INFORMATION_MESSAGE);
            JasperPrintManager.printReport(jp, true); //print report
        }catch(Exception e){
            e.printStackTrace();
        }
    }
 
}
