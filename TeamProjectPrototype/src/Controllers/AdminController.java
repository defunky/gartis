/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controllers;

import Entities.BusinessType;
import GUI.AdminGUI;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;
import Entities.Staff;
import Entities.Task;
import Helpers.PasswordHashing;
import java.io.File;
import java.lang.ProcessBuilder.Redirect;
import java.sql.PreparedStatement;
import java.sql.Time;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.filechooser.FileNameExtensionFilter;

/**
 * Admincontroller class is used by the Admin GUI. All complex logic that admin
 * account can do is done here.
 * @author Abdalrahmane
 */
public class AdminController extends ActorController{
    private Connection conn;
    private DatabaseConnection dbc;
    private ArrayList<Staff> staffData; //handles the Staff data
    private ArrayList<Task> taskListData;
    private ArrayList<BusinessType> busTypeData;
    private byte[] staffCheckSum; //used to check if data is most recent
    private byte[] taskListChecksum;
    private byte[] busTypeChecksum;
    
    /**
     * Retrieves the connection from the Database connection class, and init all data
     * that is relevant to the Admin account, such as staff account data.
     * It also applies the current checksums to the data that is retrieved
     * @param conn Connection to the database
     */
    public AdminController(DatabaseConnection dbc){
        this.dbc = dbc;
        conn = dbc.getConnection();
        staffData = new ArrayList<>();
        taskListData = new ArrayList<>();
        busTypeData = new ArrayList<>();
        staffCheckSum = setChecksum(conn,"staffAccounts");
        taskListChecksum = setChecksum(conn,"Tasklist");
        busTypeChecksum = setChecksum(conn,"businesstypes");
        getStaffInfo();
    }
    
    /**
     * Populates the ArrayList with staff data that is located in the staffAccounts table. 
     */
    private void getStaffInfo(){
        staffData.clear();
        try {
            Statement st = conn.createStatement();
            ResultSet rs = st.executeQuery("SELECT * FROM staffAccounts");
            while(rs.next()){
                staffData.add(new Staff(rs.getInt("ID"), rs.getString("username"), 
                        rs.getBytes("password"), rs.getString("type"), 
                        rs.getString("surname"), rs.getString("name"),
                        rs.getDate("date_creation"), rs.getDouble("labour_cost")));
            }
            rs.close();
            st.close();
        } catch (SQLException ex) {
            Logger.getLogger(AdminGUI.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Simple get method used to retrieve the staff information.
     * The method will call setChecksum and getInfo to make sure that the data returned
     * is the most recent.
     * Used in the AdminGUI, as it's needed by the StaffTableModel.
     * @return 
     */
    public ArrayList getStaffData(){
        staffCheckSum = setChecksum(conn,"staffAccounts"); //set the newest checksum
        getStaffInfo(); //get most updated version of table
        return staffData;
    }

    /**
     * Populates the Task List arraylist that is located in the tasklist table.
     */
    private void getTaskListInfo(){
        taskListData.clear();
        try {
            Statement st = conn.createStatement();
            ResultSet rs = st.executeQuery("SELECT * FROM tasklist");
            while(rs.next()){
                taskListData.add(new Task(rs.getInt("ListID"), rs.getString("Description"), rs.getTime("Duration")));
            }
            rs.close();
            st.close();
        } catch (SQLException ex) {
            Logger.getLogger(AdminGUI.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Simple get method used to retrieve the tasklist data
     * The method will call setChecksum and getInfo to make sure that the data returned
     * is the most recent.     
     * Task list dynamically modified list of tasks that the mechanic chooses from
     * @return 
     */
    public ArrayList getTaskListData(){
        taskListChecksum = setChecksum(conn,"tasklist"); //set the newest checksum
        getTaskListInfo(); //get most updated version of table
        return taskListData;
    }
    
    /**
     * Populates the Business Type arraylist from the BusinessType table
     */
    private void getBusinessTypeInfo(){
        busTypeData.clear();
        try {
            Statement st = conn.createStatement();
            ResultSet rs = st.executeQuery("SELECT * FROM businesstypes");
            while(rs.next()){
                busTypeData.add(new BusinessType(rs.getInt("BusID"), rs.getString("type")));
            }
            rs.close();
            st.close();
        } catch (SQLException ex) {
            Logger.getLogger(AdminGUI.class.getName()).log(Level.SEVERE, null, ex);
        }
    }


    /**
     * Simple get method used to retrieve the businesstype data
     * The method will call setChecksum and getInfo to make sure that the data returned
     * is the most recent. 
     * @return 
     */
    public ArrayList getBusinessTypeData(){
        busTypeChecksum = setChecksum(conn,"businesstypes"); //set the newest checksum
        getBusinessTypeInfo(); //get most updated version of table
        return busTypeData;
    }
    
    /**
     * This method is called when the Admin account tries to rename a username.
     * @param username - account username that is being changed
     */
    public void changeUsername(String username){
        if(!getChecksum(conn,"staffAccounts",staffCheckSum)){ //check if information is old or not through checksums
            JOptionPane.showMessageDialog(null,
                    "The previous action has failed as the table contains old records."
                            + "\nThe data will now be updated!",
                    "Table has been altered!",
            JOptionPane.ERROR_MESSAGE);
        }else{
            String newname;
            try {
                //Statement stmt = conn.createStatement();
                PreparedStatement pstm = conn.prepareStatement("UPDATE staffAccounts SET username=? WHERE username=?;");

                do{
                    newname = JOptionPane.showInputDialog(null, "Enter a new username: (must be at least 3 characters long)", "Please enter username", 
                            JOptionPane.QUESTION_MESSAGE);
                }while(newname.length() < 3); //name needs to be more than 3 chars long

                pstm.setString(1, newname);
                pstm.setString(2, username);
                pstm.executeUpdate();
                pstm.close();
            } catch (SQLException ex) {
                Logger.getLogger(AdminGUI.class.getName()).log(Level.SEVERE, null, ex);
            } catch (NullPointerException e){
                    JOptionPane.showMessageDialog(null,
                        "Username not changed!",
                        "Operation cancelled out",
                JOptionPane.WARNING_MESSAGE);
            }
        }
    }
    
    /**
     * This method is called to change the labour cost of a certain staff member
     * @param userID - user account id
     */
    public void changeLabourCost(int userID){
        if(!getChecksum(conn,"staffAccounts",staffCheckSum)){
            JOptionPane.showMessageDialog(null,
                    "The previous action has failed as the table contains old records."
                            + "\nThe data will now be updated!",
                    "Table has been altered!",
            JOptionPane.ERROR_MESSAGE);
        }else{
            String newCost;
            try {
                //Statement stmt = conn.createStatement();
                PreparedStatement pstm = conn.prepareStatement("UPDATE `staffaccounts` SET `Labour_cost` = ? WHERE `staffaccounts`.`ID` = ? ");

                do{
                    newCost = JOptionPane.showInputDialog(null, "Enter a new cost:", "Please enter new cost", 
                            JOptionPane.QUESTION_MESSAGE);
                }while(newCost.length() < 0);

                pstm.setDouble(1, Double.parseDouble(newCost));
                pstm.setInt(2, userID);
                pstm.executeUpdate();
                pstm.close();
            } catch (SQLException ex) {
                Logger.getLogger(AdminGUI.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
    
    public void changeSurname(int userID){
        if(!getChecksum(conn,"staffAccounts",staffCheckSum)){
            JOptionPane.showMessageDialog(null,
                    "The previous action has failed as the table contains old records."
                            + "\nThe data will now be updated!",
                    "Table has been altered!",
            JOptionPane.ERROR_MESSAGE);
        }else{
            String surname;
            try {
                //Statement stmt = conn.createStatement();
                PreparedStatement pstm = conn.prepareStatement("UPDATE `staffaccounts` SET `Surname` = ? WHERE `staffaccounts`.`ID` = ? ");

                do{
                    surname = JOptionPane.showInputDialog(null, "Enter a new surname:", "Please enter new surname", 
                            JOptionPane.QUESTION_MESSAGE);
                }while(surname.length() < 0);

                pstm.setString(1, surname);
                pstm.setInt(2, userID);
                pstm.executeUpdate();
                pstm.close();
            } catch (SQLException ex) {
                Logger.getLogger(AdminGUI.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
    
    public void changeName(int userID){
        if(!getChecksum(conn,"staffAccounts",staffCheckSum)){
            JOptionPane.showMessageDialog(null,
                    "The previous action has failed as the table contains old records."
                            + "\nThe data will now be updated!",
                    "Table has been altered!",
            JOptionPane.ERROR_MESSAGE);
        }else{
            String name;
            try {
                //Statement stmt = conn.createStatement();
                PreparedStatement pstm = conn.prepareStatement("UPDATE `staffaccounts` SET `name` = ? WHERE `staffaccounts`.`ID` = ? ");

                do{
                    name = JOptionPane.showInputDialog(null, "Enter a new name:", "Please enter new name", 
                            JOptionPane.QUESTION_MESSAGE);
                }while(name.length() < 0);

                pstm.setString(1, name);
                pstm.setInt(2, userID);
                pstm.executeUpdate();
                pstm.close();
            } catch (SQLException ex) {
                Logger.getLogger(AdminGUI.class.getName()).log(Level.SEVERE, null, ex);
            } 
        }
    }
    
    
    /**
     * This method changes the selected user accounts password
     * @param username 
     */
    public void changePassword(String username){
        if(!getChecksum(conn,"staffAccounts",staffCheckSum)){
            JOptionPane.showMessageDialog(null,
                    "The previous action has failed as the table contains old records."
                            + "\nThe data will now be updated!",
                    "Table has been altered!",
            JOptionPane.ERROR_MESSAGE);
        }else{
            String newpassword;
            PasswordHashing ph = new PasswordHashing();
            try {
                PreparedStatement pstm = conn.prepareStatement("UPDATE staffAccounts SET password=? WHERE username=?;");

                do{
                    //JPasswordField pwd = new JPasswordField();
                    newpassword = JOptionPane.showInputDialog(null,"Enter a new password for "+username
                            +": (must be at least 3 characters long)","Please enter password", JOptionPane.QUESTION_MESSAGE);
                }while(newpassword.length() < 3);
                
                byte[] hashedpw = ph.hashPass(newpassword);
                pstm.setBytes(1, hashedpw);
                pstm.setString(2, username);
                pstm.executeUpdate();
                pstm.close();
            } catch (SQLException ex) {
                Logger.getLogger(AdminGUI.class.getName()).log(Level.SEVERE, null, ex);
            } catch (NullPointerException e){
                    JOptionPane.showMessageDialog(null,
                        "Password has not been changed!",
                        "Operation cancelled out",
                JOptionPane.WARNING_MESSAGE);
            }
        }
    }
    
    
    public void changeType(String username){
        if(!getChecksum(conn,"staffAccounts",staffCheckSum)){
            JOptionPane.showMessageDialog(null,
                    "The previous action has failed as the table contains old records."
                            + "\nThe data will now be updated!",
                    "Table has been altered!",
            JOptionPane.ERROR_MESSAGE);
        }else{
            String[] types = { "ADMIN","FRANCHISEE","FOREPERSON","MECHANIC","RECEPTIONIST" };
            String newType = null;
            try {
                PreparedStatement pstm = conn.prepareStatement("UPDATE staffAccounts SET type=? WHERE username=?;");

                newType = (String) JOptionPane.showInputDialog(null, "Select a new account type for "+username+":","Changing account type",
                        JOptionPane.QUESTION_MESSAGE, null, types, types[0]);
                if(newType != null){
                    pstm.setString(1, newType);
                    pstm.setString(2, username);
                    pstm.executeUpdate();
                    pstm.close();
                }else{
                JOptionPane.showMessageDialog(null,
                    "Account type has not been changed!",
                    "Operation cancelled out",
                    JOptionPane.WARNING_MESSAGE);
                }
            } catch (SQLException ex) {
                Logger.getLogger(AdminGUI.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
    
    public void deleteAccount(String username){
        if(!getChecksum(conn,"staffAccounts",staffCheckSum)){
            JOptionPane.showMessageDialog(null,
                    "The previous action has failed as the table contains old records."
                            + "\nThe data will now be updated!",
                    "Table has been altered!",
            JOptionPane.ERROR_MESSAGE);
        }else{
            try {
                PreparedStatement pstm = conn.prepareStatement("DELETE FROM staffAccounts WHERE username=?;");
                pstm.setString(1, username);
                pstm.executeUpdate();
                pstm.close();
            } catch (SQLException ex) {
                Logger.getLogger(AdminGUI.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    
    }
    
    
    public void addNewUser(String username, String password, String type, String surname, String name){
        if(!getChecksum(conn,"staffAccounts",staffCheckSum)){
            JOptionPane.showMessageDialog(null,
                    "The previous action has failed as the table contains old records."
                            + "\nThe data will now be updated!",
                    "Table has been altered!",
            JOptionPane.ERROR_MESSAGE);
        }else{
            PasswordHashing ph = new PasswordHashing();
            try {
                PreparedStatement pstm = conn.prepareStatement("INSERT INTO `staffaccounts` "
                        + "(`ID`, `UserName`, `Password`, `Type`, `Surname`, `Name`, `Date_creation`) "
                        + "VALUES (NULL, ?, ?, ?, ?, ?, CURRENT_DATE());");
                byte[] hashedpw = ph.hashPass(password);
                pstm.setString(1, username);
                pstm.setBytes(2, hashedpw);
                pstm.setString(3, type);
                pstm.setString(4, surname);
                pstm.setString(5, name);
                pstm.executeUpdate();
                pstm.close();
            } catch (SQLException ex) {
                Logger.getLogger(AdminGUI.class.getName()).log(Level.SEVERE, null, ex);
            } catch (NullPointerException e){
                    JOptionPane.showMessageDialog(null,
                        "User has not been added!",
                        "Operation cancelled out",
                        JOptionPane.WARNING_MESSAGE);
            }
        }
    }
    
    //called if labour cost is needed eg. forperson/mech
    public void addNewUser(String username, String password, String type, String surname, String name, double labour){
        if(!getChecksum(conn,"staffAccounts",staffCheckSum)){
            JOptionPane.showMessageDialog(null,
                    "The previous action has failed as the table contains old records."
                            + "\nThe data will now be updated!",
                    "Table has been altered!",
            JOptionPane.ERROR_MESSAGE);
        }else{
            PasswordHashing ph = new PasswordHashing();
            try {
                PreparedStatement pstm = conn.prepareStatement("INSERT INTO `staffaccounts` "
                        + "(`ID`, `UserName`, `Password`, `Type`, `Surname`, `Name`, `Date_creation`, `Labour_cost`) "
                        + "VALUES (NULL, ?, ?, ?, ?, ?, CURRENT_DATE(),?);");
                byte[] hashedpw = ph.hashPass(password);
                pstm.setString(1, username);
                pstm.setBytes(2, hashedpw);
                pstm.setString(3, type);
                pstm.setString(4, surname);
                pstm.setString(5, name);
                pstm.setDouble(6, labour);
                pstm.executeUpdate();
                pstm.close();
            } catch (SQLException ex) {
                Logger.getLogger(AdminGUI.class.getName()).log(Level.SEVERE, null, ex);
            } catch (NullPointerException e){
                    JOptionPane.showMessageDialog(null,
                        "User has not been added!",
                        "Operation cancelled out",
                        JOptionPane.WARNING_MESSAGE);
            }
        }
    }
        
        
    private String SQLDirectoryChooser(){
        JFileChooser chooser = new JFileChooser("C:\\xampp");
        String mySQLDir = null;
        chooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
        chooser.setDialogTitle("Select MySQL folder");
        int returnVal = chooser.showOpenDialog(new javax.swing.JFrame());
        if(returnVal == JFileChooser.APPROVE_OPTION) {
             mySQLDir = chooser.getSelectedFile().getAbsolutePath();
        }
        return mySQLDir;
    }
    
    public void backupDatabase(){
        String mySQLDir = SQLDirectoryChooser();
        if(mySQLDir == null){
                JOptionPane.showMessageDialog(null,
                    "Backup cancelled!",
                    "Operation has be cancelled",
                    JOptionPane.INFORMATION_MESSAGE);
        }else{
            try {
                String folderPath =  System.getProperty("user.dir")+"\\backup";
                //create directory if it don't exist.
                new File(folderPath).mkdir();
                //Creates a cmd command to be used later on, uses mysqldump to backup
                String executeCmd = mySQLDir+"\\bin\\mysqldump -u root --password= --databases gartisdb -r " 
                        +folderPath+ "\\garitsDBBackup"
                        +LocalDateTime.now().format(DateTimeFormatter.ofPattern("dd-MM-YYYY%HH-mm-ss"))
                        +".sql";
                //executes the command, and waits for the outcome.
                Process runtimeProcess = Runtime.getRuntime().exec(executeCmd);
                int processComplete = runtimeProcess.waitFor();           
                //Tell the user if the outcome was succesful or not
                if (processComplete == 0) {
                    JOptionPane.showMessageDialog(null,
                        "Backup was succesful!",
                        "Backup completed",
                        JOptionPane.INFORMATION_MESSAGE);
                } else {
                    JOptionPane.showMessageDialog(null,
                        "Back up has failed, please try again",
                        "Backup has failed",
                        JOptionPane.ERROR_MESSAGE);
                }

            } catch (Exception ex) {
                    JOptionPane.showMessageDialog(null,
                        "An error has occured while backing up! "+ex.getMessage(),
                        "Backup has failed",
                        JOptionPane.ERROR_MESSAGE);
            }
        }
    }
    
    public void restoreDatabase(){
        String mySQLDir = SQLDirectoryChooser();
        
        JFileChooser chooser = new JFileChooser(System.getProperty("user.dir")+"\\backup");
        chooser.setFileFilter(new FileNameExtensionFilter("SQL File", "sql"));
        chooser.setAcceptAllFileFilterUsed(false); //not all files only sql
        String backupPath = null;
        chooser.setFileSelectionMode(JFileChooser.FILES_ONLY);
        chooser.setDialogTitle("Select Backup file");
        int returnVal = chooser.showOpenDialog(new javax.swing.JFrame());
        if(returnVal == JFileChooser.APPROVE_OPTION) {
             backupPath = chooser.getSelectedFile().getAbsolutePath();
        }
        
        dbc.dropDB();
        dbc.createDB();
        
       // System.out.println(backupFile);
        try {
            File backupFile = new File(backupPath);
            String[] command = new String[]{mySQLDir+"\\bin\\mysql ", "-uroot", "--password=", "gartisdb"};
            ProcessBuilder processBuilder = new ProcessBuilder(command);
            processBuilder = new ProcessBuilder(Arrays.asList(command));
            processBuilder.redirectError(Redirect.INHERIT);
            processBuilder.redirectInput(Redirect.from(backupFile));

            Process process = processBuilder.start();
            int processComplete = process.waitFor();
            
            //debug code
//            InputStream stdin = runtimeProcess.getInputStream();
//            InputStreamReader isr = new InputStreamReader(stdin);
//            BufferedReader br = new BufferedReader(isr);
//            String line = null;
//            System.out.println("<OUTPUT>");
//            while ( (line = br.readLine()) != null)
//                System.out.println(line);
//            System.out.println("</OUTPUT>");
//            int processComplete = runtimeProcess.waitFor();           
//            System.out.println("Process exitValue: " + processComplete);
//            //int processComplete = runtimeProcess.waitFor();
//
//            //Tell the user if the outcome was succesful or not
            if (processComplete == 0) {
                JOptionPane.showMessageDialog(null,
                    "Restore was succesful!",
                    "Backup completed",
                    JOptionPane.INFORMATION_MESSAGE);
            } else {
                JOptionPane.showMessageDialog(null,
                    "Restore has failed, please try again",
                    "Backup has failed",
                    JOptionPane.ERROR_MESSAGE);
            }

        } catch (Exception ex) {
                JOptionPane.showMessageDialog(null,
                    "An error has occured while backing up! "+ex.getMessage(),
                    "Backup has failed",
                    JOptionPane.ERROR_MESSAGE);
        }
    }
    
    public void addTask(String desc, int hour, int min, int sec){
        if(!getChecksum(conn,"taskList",taskListChecksum)){
            JOptionPane.showMessageDialog(null,
                    "The previous action has failed as the table contains old records."
                            + "\nThe data will now be updated!",
                    "Table has been altered!",
            JOptionPane.ERROR_MESSAGE);
        }else{
            try {
                PreparedStatement pstm = conn.prepareStatement("INSERT INTO `tasklist` "
                        + "(`ListID`, `Description`, `Duration`) "
                        + "VALUES (NULL, ?, ?);");
                Time time = new Time(hour,min,sec);
                pstm.setString(1, desc);
                pstm.setTime(2, time);
                pstm.executeUpdate();
                pstm.close();
            } catch (SQLException ex) {
                Logger.getLogger(AdminGUI.class.getName()).log(Level.SEVERE, null, ex);
            } catch (NullPointerException e){
                    JOptionPane.showMessageDialog(null,
                        "Task has not been added!",
                        "Operation cancelled out",
                        JOptionPane.WARNING_MESSAGE);
            }
        }
    }
    
    public void alterTask(int listID, String desc, int hour, int min, int sec){
        if(!getChecksum(conn,"taskList",taskListChecksum)){
            JOptionPane.showMessageDialog(null,
                    "The previous action has failed as the table contains old records."
                            + "\nThe data will now be updated!",
                    "Table has been altered!",
            JOptionPane.ERROR_MESSAGE);
        }else{
            try {
                PreparedStatement pstm = conn.prepareStatement("UPDATE `tasklist` SET"
                        + " `Description` = ?, `Duration` = ? "
                        + "WHERE `tasklist`.`ListID` = ? ");
                Time time = new Time(hour,min,sec);
                pstm.setString(1, desc);
                pstm.setTime(2, time);
                pstm.setInt(3, listID);
                pstm.executeUpdate();
                pstm.close();
                JOptionPane.showMessageDialog(null,
                        "Task has been altered!",
                        "Task altered",
                        JOptionPane.INFORMATION_MESSAGE);
            } catch (SQLException ex) {
                Logger.getLogger(AdminGUI.class.getName()).log(Level.SEVERE, null, ex);
            } catch (NullPointerException e){
                    JOptionPane.showMessageDialog(null,
                        "Task has not been altered!",
                        "Operation cancelled out",
                        JOptionPane.WARNING_MESSAGE);
            }
        }
    }
    
    public void deleteTask(int listID){
            if(!getChecksum(conn,"taskList",taskListChecksum)){
            JOptionPane.showMessageDialog(null,
                    "The previous action has failed as the table contains old records."
                            + "\nThe data will now be updated!",
                    "Table has been altered!",
            JOptionPane.ERROR_MESSAGE);
        }else{
            try {
                PreparedStatement pstm = conn.prepareStatement("DELETE FROM tasklist WHERE listid=?;");
                pstm.setInt(1, listID);
                pstm.executeUpdate();
                pstm.close();
                JOptionPane.showMessageDialog(null,
                        "Task has been deleted!",
                        "Task deleted",
                        JOptionPane.INFORMATION_MESSAGE);
            } catch (SQLException ex) {
                Logger.getLogger(AdminGUI.class.getName()).log(Level.SEVERE, null, ex);
            } catch (NullPointerException e){
                    JOptionPane.showMessageDialog(null,
                        "Task has not been deleted!",
                        "Operation cancelled out",
                        JOptionPane.WARNING_MESSAGE);
            }
        } 
    }
    
    public void addBusType(String type, double flatrate){
        if(!getChecksum(conn,"businesstypes",busTypeChecksum)){
            JOptionPane.showMessageDialog(null,
                    "The previous action has failed as the table contains old records."
                            + "\nThe data will now be updated!",
                    "Table has been altered!",
            JOptionPane.ERROR_MESSAGE);
        }else{
            try {
                PreparedStatement pstm = conn.prepareStatement("INSERT INTO "
                        + "`businesstypes` (`BusID`, `Type`, `Flatrate`) VALUES (NULL, ? , ?)");
                pstm.setString(1, type);
                pstm.setDouble(2, flatrate);
                pstm.executeUpdate();
                pstm.close();
                JOptionPane.showMessageDialog(null,
                        "Business type added!",
                        "Type Added",
                        JOptionPane.INFORMATION_MESSAGE);
            } catch (SQLException ex) {
                Logger.getLogger(AdminGUI.class.getName()).log(Level.SEVERE, null, ex);
            } catch (NullPointerException e){
                    JOptionPane.showMessageDialog(null,
                        "Task has not been added!",
                        "Operation cancelled out",
                        JOptionPane.WARNING_MESSAGE);
            }
        }
    }
    
    public void alterBusType(int busID, String type){
        if(!getChecksum(conn,"businesstypes",busTypeChecksum)){
            JOptionPane.showMessageDialog(null,
                    "The previous action has failed as the table contains old records."
                            + "\nThe data will now be updated!",
                    "Table has been altered!",
            JOptionPane.ERROR_MESSAGE);
        }else{
            try {
                PreparedStatement pstm = conn.prepareStatement("UPDATE `businesstypes`"
                        + " SET `Type` = ? WHERE `businesstypes`.`BusID` = ? ");
                pstm.setString(1, type);
                pstm.setInt(2, busID);
                pstm.executeUpdate();
                pstm.close();
                JOptionPane.showMessageDialog(null,
                        "Business type altered!",
                        "Type altered",
                        JOptionPane.INFORMATION_MESSAGE);
            } catch (SQLException ex) {
                Logger.getLogger(AdminGUI.class.getName()).log(Level.SEVERE, null, ex);
            } catch (NullPointerException e){
                    JOptionPane.showMessageDialog(null,
                        "Task has not been added!",
                        "Operation cancelled out",
                        JOptionPane.WARNING_MESSAGE);
            }
        }
    }
    
    public void deleteBusType(int busID){
            if(!getChecksum(conn,"businesstypes",busTypeChecksum)){
            JOptionPane.showMessageDialog(null,
                    "The previous action has failed as the table contains old records."
                            + "\nThe data will now be updated!",
                    "Table has been altered!",
            JOptionPane.ERROR_MESSAGE);
        }else{
            try {
                PreparedStatement pstm = conn.prepareStatement("DELETE FROM businesstypes WHERE busid=?;");
                pstm.setInt(1, busID);
                pstm.executeUpdate();
                pstm.close();
                JOptionPane.showMessageDialog(null,
                        "Business type has been deleted!",
                        "Business type deleted",
                        JOptionPane.INFORMATION_MESSAGE);
            } catch (SQLException ex) {
                Logger.getLogger(AdminGUI.class.getName()).log(Level.SEVERE, null, ex);
            } catch (NullPointerException e){
                    JOptionPane.showMessageDialog(null,
                        "Business type has not been deleted!",
                        "Operation cancelled out",
                        JOptionPane.WARNING_MESSAGE);
            }
        } 
    }
    
    public void alterVAT(double VAT){
        try {
            PreparedStatement pstm = conn.prepareStatement("UPDATE config SET value=? WHERE configname=\"VAT\";");
            pstm.setDouble(1, VAT);
            pstm.executeUpdate();
            pstm.close();
        } catch (SQLException ex) {
            Logger.getLogger(AdminGUI.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
