/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controllers;

import Entities.Job;
import Entities.StockItem;
import Entities.Vehicle;
import GUI.Mechanic.JobTask;
import java.sql.*;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.HashMap;
import javax.swing.JOptionPane;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperPrintManager;
import net.sf.jasperreports.engine.JasperReport;


public class MechanicController extends ActorController  {
    private Connection conn;
    private DatabaseConnection dbc;
    private ArrayList<Job> pendingJobData;
    private ArrayList<JobTask> taskData;
    private ArrayList<StockItem> stockData;
    private ArrayList<Vehicle> vehData;
    private HashMap<String,Time> taskList;
    private int staffID;
    private byte[] pendingChecksum;
    private byte[] jobChecksum; //used for altering jobstatus
    private byte[] stockChecksum;

    public MechanicController(DatabaseConnection dbc, int staffID){
        this.dbc = dbc;
        conn = dbc.getConnection();
        this.staffID = staffID;
        pendingJobData = new ArrayList<>();
        taskData = new ArrayList<>();
        stockData = new ArrayList<>();
        vehData = new ArrayList<>();
        taskList = new HashMap<>();
        pendingChecksum = setChecksum(conn, "PendingJobs");
        jobChecksum = setChecksum(conn,"Jobs");
        stockChecksum = setChecksum(conn,"SparePartStock");
    }
    
    /**
     * Populates an arraylist with  all pending jobs that do have not been assigned to anyone
     */
    private void getPendingJobInfo() {
        pendingJobData.clear();
        try {
            Statement st = conn.createStatement();
            ResultSet rs = st.executeQuery("SELECT Jobs.JobID, RegNo, Work_Required, JobStatus, BusinessType, Duration, Date_creation "
                    + "FROM `jobs` INNER JOIN pendingjobs ON jobs.JobID = pendingjobs.JobID AND `StaffID` IS NULL");
            while(rs.next()){
                pendingJobData.add(new Job(rs.getInt("JobID"), rs.getString("RegNo"), 
                        rs.getString("Work_required"), rs.getString("JobStatus"), 
                        rs.getString("BusinessType"),
                        rs.getTime("Duration"), rs.getDate("Date_creation")));
            }
            rs.close();
            st.close();
        } catch (SQLException ex) {
            ex.printStackTrace();
        }    
    }
    
    /**
     * Returns the arraylist of the pending jobs that have not been assigned to anyone
     * and resets the checksum.
     * @return 
     */
    public ArrayList getPendingJobData(){
        pendingChecksum = setChecksum(conn, "PendingJobs");
        getPendingJobInfo();
        return pendingJobData;
    }
    /**
     * Populates the arraylist with the all jobs that are assigned to the staff that is logged in
     */
    private void getSelectedPendingJobInfo() {
        pendingJobData.clear();
        try {
            Statement st = conn.createStatement();
            ResultSet rs = st.executeQuery("SELECT Jobs.JobID, RegNo, Work_Required, JobStatus, BusinessType, Duration, Date_creation "
                    + "FROM `jobs` INNER JOIN pendingjobs ON jobs.JobID = pendingjobs.JobID AND `StaffID` ="+staffID);
            while(rs.next()){
                pendingJobData.add(new Job(rs.getInt("JobID"), rs.getString("RegNo"), 
                        rs.getString("Work_required"), rs.getString("JobStatus"), 
                        rs.getString("BusinessType"),
                        rs.getTime("Duration"), rs.getDate("Date_creation")));
            }
            rs.close();
            st.close();
        } catch (SQLException ex) {
            ex.printStackTrace();
        }    
    }
    
    /**
     * Returns array list of the all jobs assigned to the staff signed in.
     * Checksum of job is set as all the information needed is there 
     * @return 
     */
    public ArrayList getSelectedPendingJobData(){
        jobChecksum = setChecksum(conn,"Jobs");
        getSelectedPendingJobInfo();
        return pendingJobData;
    }
    
    /**
     * Populates an arraylist with all the tasks related to a specific job
     * from the task table in database
     * @param jobID - the selected job id
     */
    public void getSelectedTaskInfo(int jobID){
        taskData.clear();
        try {
            Statement st = conn.createStatement();
            ResultSet rs = st.executeQuery("SELECT * FROM tasks WHERE JobID ="+jobID);
            while(rs.next()){
                taskData.add(new JobTask(rs.getInt("TaskID"),rs.getInt("JobID"),
                rs.getString("Description"),rs.getTime("Duration")));
            }
            rs.close();
            st.close();
        } catch (SQLException ex) {
            ex.printStackTrace();
        }    
    }
    
    /**
     * Returns the array list that contains all the task related to the a selected
     * job
     * @param jobID - selected job id
     * @return
     */
    public ArrayList getSelectedTaskData(int jobID){
        getSelectedTaskInfo(jobID);
        return taskData;
    }
    
    /**
     * Populates an Hashmap of all the pre-defined task list and their durations
     * this is retrieved from task-list table which can be modified by the admin user
     */
    public void getTaskListInfo(){
        taskList.clear();
        try {
            Statement st = conn.createStatement();
            ResultSet rs = st.executeQuery("SELECT * FROM tasklist");
            while(rs.next()){
                taskList.put(rs.getString("Description"), rs.getTime("Duration"));
            }
            rs.close();
            st.close();
        } catch (SQLException ex) {
            ex.printStackTrace();
        }    
    }
    
    /**
      * Returns the hashmap of the predefined tasks that mechanic can choose from
      * @return 
      */  
    public HashMap getTaskList(){
        getTaskListInfo();
        return taskList;
    }
    
    /**
     * Get all vehicle information from a single customer.
     * @param regNo - selected vehicle
     */
    public void getCusVehInfo(String regNo){
        vehData.clear();
        try {
            PreparedStatement pstm = conn.prepareStatement("SELECT * FROM `Vehicles` WHERE RegNo = ?");
            pstm.setString(1, regNo);
            ResultSet rs = pstm.executeQuery();
            while(rs.next()){
                vehData.add(new Vehicle(rs.getString("RegNo"), rs.getInt("EngSerial"), 
                        rs.getInt("ChassisNo"),rs.getString("Colour"), 
                        rs.getString("Make"), rs.getString("Model"), rs.getString("Year"),
                        rs.getDate("LastMoTCheck")));
            }
            rs.close();
            pstm.close();
        } catch (SQLException ex) {
            ex.printStackTrace();
        }    
    }
    
    /**
     * Return the Arraylist that contains all the information of the selected vehicle
     * @param regNo
     * @return 
     */
    public ArrayList getCusVehData(String regNo){
        getCusVehInfo(regNo);
        return vehData;
    }
    
    /**
     * Populates an arraylist with all the stock information retrieved from the 
     * sparepartstock table from the database.
     */
    private void getStockInfo(){
        stockData.clear();
        try {
            Statement st = conn.createStatement();
            ResultSet rs = st.executeQuery("SELECT * FROM sparepartstock");
            while(rs.next()){
                stockData.add(new StockItem(rs.getString("Code"), rs.getString("part_name"), 
                        rs.getString("manufacturer"),rs.getString("Vehicle_Type"), 
                        rs.getString("Year"), rs.getDouble("Price"), rs.getDouble("Cost_Per_Item"),
                        rs.getInt("Stock_Level"),rs.getDouble("Stock_Cost"),rs.getInt("Threshold_Level"),
                        rs.getInt("Inital_Stock_Level"),rs.getDouble("Inital_Stock_Cost")));
            }
            rs.close();
            st.close();
        } catch (SQLException ex) {
            ex.printStackTrace();
        }   
    }
    
    /**
     * Returns the arraylist of the stock data, and sets the checksum for it
     * @return 
     */
    public ArrayList getStockData(){
        stockChecksum = setChecksum(conn, "SparePartStock");
        getStockInfo();
        return stockData;
    }
    

    /**
     * The mechanic has the option to assign a job to himself to start working on.
     * It will first do a check to make sure that the data is the most updated version and 
     * job hasn't been assigned to anyone before hand. Once that check has passed it will then
     * set the staffID in the pendinglist table to the mechanic/foreperson that has assigned himself
     * @param jobID - the selected job
     */
    public void assignJob(int jobID){
        if(!getChecksum(conn,"PendingJobs",pendingChecksum)){
            JOptionPane.showMessageDialog(null,
                    "The previous action has failed as the table contains old records."
                            + "\nThe data will now be updated!",
                    "Table has been altered!",
            JOptionPane.ERROR_MESSAGE);
        }else{
            try {
                PreparedStatement pstm = conn.prepareStatement("UPDATE `pendingjobs` "
                        + "SET `StaffID` = ? WHERE `pendingjobs`.`JobID` = ? ");
                pstm.setInt(1, staffID);
                pstm.setInt(2, jobID);
                pstm.executeUpdate();
                pstm.close();
                JOptionPane.showMessageDialog(null,
                    "Job has been assigned to you!",
                    "Job assigned",
                    JOptionPane.INFORMATION_MESSAGE);
            } catch (SQLException ex) {
                ex.printStackTrace();
            } catch (NullPointerException e){
                    JOptionPane.showMessageDialog(null,
                        "Job has not been assigned!",
                        "Operation cancelled out",
                        JOptionPane.WARNING_MESSAGE);
            }
        }
    }
    
    /**
     * This method is used by the foreperson/mechanic to alter the job status
     * to the 3 states (IDLE/IN PROGRESS/COMPLETE). If the staff member selects
     * COMPLETED state. It will begin to calculate the final accurate task duration which is
     * set in the duration column in the job table.
     * @param jobID
     * @param status 
     */
    public void alterJobStatus(int jobID, String status){
        if(!getChecksum(conn,"Jobs",jobChecksum)){
            JOptionPane.showMessageDialog(null,
                    "The previous action has failed as the table contains old records."
                            + "\nThe data will now be updated!",
                    "Table has been altered!",
            JOptionPane.ERROR_MESSAGE);
        }else{
            try { 
                if(status.equals("COMPLETED")){
                    calculateTaskTime(jobID);
                }
                PreparedStatement pstm = conn.prepareStatement("UPDATE `jobs` SET `JobStatus` = ?"
                        + " WHERE `jobs`.`JobID` = ?");
                pstm.setString(1, status);
                pstm.setInt(2, jobID);
                pstm.executeUpdate();
                pstm.close();
                JOptionPane.showMessageDialog(null,
                    "Status altered!",
                    "Job status changed",
                    JOptionPane.INFORMATION_MESSAGE);
            } catch (SQLException ex) {
                ex.printStackTrace();
            } catch (NullPointerException e){
                    JOptionPane.showMessageDialog(null,
                        "Job has not been assigned!",
                        "Operation cancelled out",
                        JOptionPane.WARNING_MESSAGE);
            }
        }
    }
    
    /**
     * This method is called when the job has been completed and been assigned completed state
     * in the job status. The method will calculate the sum of all the tasks associated to the job
     * and set it accordingly
     * @param jobID - selected job id 
     */
    private void calculateTaskTime(int jobID){
        try {
            Statement st = conn.createStatement();
            ResultSet rs = st.executeQuery("SELECT SEC_TO_TIME( SUM( TIME_TO_SEC( `Duration` ) ) ) AS timeSum FROM tasks WHERE JobID ="+jobID);
            Time time = null;
            while(rs.next()){
                time = rs.getTime("timeSum");
            }
            if(time==null) time = new Time(0); //check used if there's no tasks linked to the job
            rs.close();
            st.close();
            
            PreparedStatement pstm = conn.prepareStatement("UPDATE `jobs` SET `Duration` = ?"
                        + " WHERE `jobs`.`JobID` = ?");
            pstm.setTime(1, time);
            pstm.setInt(2, jobID);
            pstm.executeUpdate();
            pstm.close();
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
    }
    
    /**
     * This method is called when the mechanic/foreperson wants to assign a new task
     * to the job.
     * @param jobID - selected job id
     * @param desc - the task description 
     * @param h - hours needed to finish the task
     * @param m - minutes needed to finish the task
     * @param s - seconds needed to finish the task
     */
    public void addNewTask(int jobID, String desc, int h, int m, int s){
        try {
            PreparedStatement pstm = conn.prepareStatement("INSERT INTO `tasks` "
                    + "(`TaskID`, `JobID`, `Description`, `Duration`) "
                    + "VALUES (NULL, ?, ?, ?)");
            pstm.setInt(1, jobID);
            pstm.setString(2, desc);
            Time time = new Time(h,m,s);
            pstm.setTime(3, time);
            pstm.executeUpdate();
            pstm.close();
            JOptionPane.showMessageDialog(null,
                "The task has been added for the Job ID: "+jobID+"!",
                "Task added",
                JOptionPane.INFORMATION_MESSAGE);
        } catch (SQLException ex) {
            ex.printStackTrace();
        } catch (NullPointerException e){
                JOptionPane.showMessageDialog(null,
                    "Task has not been added!",
                    "Operation cancelled out",
                    JOptionPane.WARNING_MESSAGE);
        }
    }
    /**
     * Allows the mechanic/foreperson to alter an existing task thats allocated
     * to a job.
     * @param taskID - selected task that will be altered
     * @param desc - the new description of the task
     * @param h - the new hour duration of the task
     * @param m - the new minute duration of the task
     * @param s  - the new second duration of the task
     */
    public void alterTask(int taskID, String desc, int h, int m, int s){
    //UPDATE `tasks` SET `Description` = 'Windshield ', `Duration` = '00:10:0' WHERE `tasks`.`TaskID` = 1 
        try {
            PreparedStatement pstm = conn.prepareStatement("UPDATE `tasks` "
                    + "SET `Description` = ?, `Duration` = ? "
                    + "WHERE `tasks`.`TaskID` = ? ");
            pstm.setString(1, desc);
            Time time = new Time(h,m,s);
            pstm.setTime(2, time);
            pstm.setInt(3, taskID);
            pstm.executeUpdate();
            pstm.close();
            JOptionPane.showMessageDialog(null,
                "The task has been modified!",
                "Task altered",
                JOptionPane.INFORMATION_MESSAGE);
        } catch (SQLException ex) {
            ex.printStackTrace();
        } catch (NullPointerException e){
                JOptionPane.showMessageDialog(null,
                    "Task has not been added!",
                    "Operation cancelled out",
                    JOptionPane.WARNING_MESSAGE);
        }
    }
    /**
     * This method is called when the the mechanic/foreperson deletes an existing
     * task from the list of tasks associated to job in progress
     * @param taskID - the selected task id
     */
    public void deleteTask(int taskID){
        try {
            PreparedStatement pstm = conn.prepareStatement("DELETE FROM Tasks WHERE TaskID=?;");
            pstm.setInt(1, taskID);
            pstm.executeUpdate();
            pstm.close();
            JOptionPane.showMessageDialog(null,
                "The task has been deleted!",
                "Task deleted",
                JOptionPane.INFORMATION_MESSAGE);
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
    }
    
    /**
     * This method is invoked when the mechanic/foreperson selects an stock item
     * to be used in a specific job. Once the mechanic has specified the amount needed
     * the sparepartstock table will be updated with the details such as new stock level, and the items 
     * specified will now be associated to the job through sparepartused table
     * @param code - stock code
     * @param amount - the new stock level of the selected item 
     * @param price - the price of the selected item
     * @param jobID - the selected job id
     * @param used - the amount used in the job specified
     */
    public void useStockPart(String code, int amount, double price, int jobID, int used){
        if(!getChecksum(conn,"SparePartStock",stockChecksum)){
            JOptionPane.showMessageDialog(null,
                    "The previous action has failed as the table contains old records."
                            + "\nThe data will now be updated!",
                    "Table has been altered!",
            JOptionPane.ERROR_MESSAGE);
        }else{
            try {
                PreparedStatement pstm = conn.prepareStatement("UPDATE `sparepartstock` "
                        + "SET `Stock_Level` = ?, `Stock_Cost` = ? "
                        + "WHERE `sparepartstock`.`Code` = ?  ");
                pstm.setInt(1, amount);
                pstm.setDouble(2, amount*price);
                pstm.setString(3, code);
                pstm.executeUpdate();
                pstm = conn.prepareStatement("INSERT INTO `sparepartused` "
                        + "(`UsedID`, `Code`, `JobID`, `Used`, `DateUsed`) VALUES (NULL, ?, ?, ?, CURRENT_DATE())");
                pstm.setString(1, code);
                pstm.setInt(2, jobID);
                pstm.setInt(3, used);
                pstm.executeUpdate();
                pstm.close();
                JOptionPane.showMessageDialog(null,
                    "The Stock part has been assigned to the job!",
                    "Stock part used",
                    JOptionPane.INFORMATION_MESSAGE);
            } catch (SQLException ex) {
                ex.printStackTrace();
            } catch (NullPointerException e){
                    JOptionPane.showMessageDialog(null,
                        "Job has not been assigned!",
                        "Operation cancelled out",
                        JOptionPane.WARNING_MESSAGE);
            }
        }
    }   
    
    /**
     * This method generates a PDF file that consists of all the information needed
     * for a jobsheet (an example of this is shown in the brief).
     * The method retrieves the needed information to generate the file, then
     * uses a third party framework called jasperreports to insert this information in
     * a generated template given in the reporttemplates folder.
     * @param jobID - selected job
     * @param regNo - vehicle that is been worked on by that job
     */
    public void generateJobsheet(int jobID, String regNo){
        try{
            PreparedStatement pstm = conn.prepareStatement("SELECT `CustomerID` FROM `vehicles` WHERE `RegNo` = ?");
            pstm.setString(1, regNo);
            ResultSet rs = pstm.executeQuery();
            int cusID = 0;
            while(rs.next()){
                cusID = rs.getInt("CustomerID");
            }
            
            JasperReport jasperMasterReport = JasperCompileManager.compileReport(System.getProperty("user.dir")+"\\reporttemplates\\jobSheet.jrxml");
            String fileDate = LocalDateTime.now().format(DateTimeFormatter.ofPattern("dd-MM-YYYY"));
            HashMap paraMap = new HashMap<>();
            paraMap.put("cusID",cusID);
            paraMap.put("jobID",jobID);
            paraMap.put("SUBREPORT_DIR",System.getProperty("user.dir")+"\\reporttemplates\\");
            JasperPrint jp = JasperFillManager.fillReport(jasperMasterReport, paraMap, conn);
            JasperExportManager.exportReportToPdfFile(jp,
                  System.getProperty("user.dir")+"\\jobsheets\\jobsheet-"+regNo+"-"+fileDate+".pdf");
            JasperPrintManager.printReport(jp, true); //print report
            JOptionPane.showMessageDialog(null,
                    "Job sheet generated file: jobsheet-"+regNo+"-"+fileDate+".pdf",
                    "Job sheet created",
                    JOptionPane.INFORMATION_MESSAGE);
        }catch(Exception e){
            e.printStackTrace();
        }
    }
}
