/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controllers;

import Entities.Job;
import Entities.PendingJob;
import java.sql.*;
import java.util.ArrayList;
import javax.swing.JOptionPane;

/**
 * The foreperson controller. This class contains all the complex logic
 * that the foreperson account can do. This class is used by ForepersonGUI.
 * As the foreperson has same functions as Mech and Receptionist, it inherits the Mech controller
 * As java does not support multi inheritance, Receptionist section is launched through
 * an option in the menubar.
 * @author Abdalrahmane
 */
public class ForepersonController extends MechanicController {
    private Connection conn;
    private ArrayList<Job> jobData;
    private ArrayList<PendingJob> pendingListData;
    private byte[] jobChecksum;
    private byte[] pendingListChecksum;
    
    public ForepersonController(DatabaseConnection dbc, int staffID) {
        super(dbc, staffID);
        conn = dbc.getConnection();
        jobData = new ArrayList<>();
        pendingListData = new ArrayList<>();
    }
    
    /**
     * Populates the arraylist with all jobs currently in the jobs table
     */
    private void getJobInfo() {
        jobData.clear();
        try {
            Statement st = conn.createStatement();
            ResultSet rs = st.executeQuery("SELECT * FROM Jobs");
            while(rs.next()){
                jobData.add(new Job(rs.getInt("JobID"), rs.getString("RegNo"), 
                        rs.getString("Work_required"), rs.getString("JobStatus"), 
                        rs.getString("BusinessType"),
                        rs.getTime("Duration"), rs.getDate("Date_creation")));
            }
            rs.close();
            st.close();
        } catch (SQLException ex) {
            ex.printStackTrace();
        }    
    }
    
    /**
     * Returns the arraylist containing all jobs.
     * Method also updates the checksum to the latest one.
     * @return 
     */
    public ArrayList getJobData(){
        jobChecksum = setChecksum(conn, "Jobs");
        getJobInfo();
        return jobData;
    }
    /**
     * Populates arraylist with current pending job list located in the pendinglist table
     */
    private void getPendingListInfo() {
        pendingListData.clear();
        try {
            Statement st = conn.createStatement();
            ResultSet rs = st.executeQuery("SELECT Jobs.JobID, RegNo, Work_Required, JobStatus, BusinessType, Duration, jobs.Date_creation, Username FROM `jobs` "
                    + "INNER JOIN pendingjobs ON jobs.JobID = pendingjobs.JobID "
                    + "LEFT JOIN staffaccounts ON pendingjobs.StaffID=staffaccounts.ID ");
            while(rs.next()){
                pendingListData.add(new PendingJob(rs.getInt("JobID"), rs.getString("RegNo"), 
                        rs.getString("Work_required"), rs.getString("JobStatus"), 
                        rs.getString("BusinessType"),
                        rs.getTime("Duration"), rs.getDate("Date_creation"), rs.getString("username")));
            }
            rs.close();
            st.close();
        } catch (SQLException ex) {
            ex.printStackTrace();
        }    
    }
    /**
     * Returns the arraylist containing all pending jobs.
     * Method also updates the checksum to the latest one.
     * @return 
     */
    public ArrayList getPendingListData(){
        pendingListChecksum = setChecksum(conn, "PendingJobs");
        getPendingListInfo();
        return pendingListData;
    }
    
    /**
     * Allows the foreperson to add a job created by either foreperson/receptionist
     * to a pending job list, which can be assigned by foreperson himself or the mech
     * @param jobID 
     */
   public void addJobToList(int jobID){        
        if(!getChecksum(conn,"PendingJobs",pendingListChecksum)){
            JOptionPane.showMessageDialog(null,
                    "The previous action has failed as the table contains old records."
                            + "\nThe data will now be updated!",
                    "Table has been altered!",
            JOptionPane.ERROR_MESSAGE);
        }else{
            try {
                PreparedStatement pstm = conn.prepareStatement("INSERT INTO `pendingjobs` "
                        + "(`PendingJobID`, `JobID`, `StaffID`) VALUES (NULL, ?, NULL)");
                pstm.setInt(1, jobID);
                pstm.executeUpdate();
                pstm.close();
                JOptionPane.showMessageDialog(null,
                    "Job has been added to Pending Job List",
                    "Job added",
                    JOptionPane.INFORMATION_MESSAGE);
            } catch (SQLException ex) {
                ex.printStackTrace();
            } catch (NullPointerException e){
                    JOptionPane.showMessageDialog(null,
                        "Job has not been added!",
                        "Operation cancelled out",
                        JOptionPane.WARNING_MESSAGE);
            }
        }
   }
}
