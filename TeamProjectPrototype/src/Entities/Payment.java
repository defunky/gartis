/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entities;

import java.sql.Date;

/**
 * An Entity class related to the Payment Entity in the database
 * @author Abdalrahmane
 */
public class Payment {
    private int paymentID, invoiceID;
    private String card;
    private String expire;
    private Date payment;

    public Payment(int paymentID, int refID, String card, String expire, Date payment) {
        this.paymentID = paymentID;
        this.invoiceID = refID;
        this.card = card;
        this.expire = expire;
        this.payment = payment;
    }

    public int getPaymentID() {
        return paymentID;
    }

    public void setPaymentID(int paymentID) {
        this.paymentID = paymentID;
    }

    public int getRefID() {
        return invoiceID;
    }

    public void setRefID(int refID) {
        this.invoiceID = refID;
    }

    public String getCard() {
        return card;
    }

    public void setCard(String card) {
        this.card = card;
    }

    public String getExpire() {
        return expire;
    }

    public void setExpire(String expire) {
        this.expire = expire;
    }

    public Date getPayment() {
        return payment;
    }

    public void setPayment(Date payment) {
        this.payment = payment;
    }
    
    
}
