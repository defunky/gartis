/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entities;

import java.sql.Date;

/**
 * An Entity class related to the Invoice Entity in the database
 * @author Abdalrahmane
 */
public class Invoice {
    private int invoiceID, jobID, reminder;
    private double stockCost, labourCost, total, VAT, grandtotal;
    private Date initalDate, lastDateReminder;
    private boolean paid;

    public Invoice(int invoiceID, int jobID, int reminder, double stockCost, double labourCost, double total, double VAT, double grandtotal, Date initalDate, Date lastDateReminder, boolean paid) {
        this.invoiceID = invoiceID;
        this.jobID = jobID;
        this.reminder = reminder;
        this.stockCost = stockCost;
        this.labourCost = labourCost;
        this.total = total;
        this.VAT = VAT;
        this.grandtotal = grandtotal;
        this.initalDate = initalDate;
        this.lastDateReminder = lastDateReminder;
        this.paid = paid;
    }

    public int getInvoiceID() {
        return invoiceID;
    }

    public void setInvoiceID(int invoiceID) {
        this.invoiceID = invoiceID;
    }

    public int getJobID() {
        return jobID;
    }

    public void setJobID(int jobID) {
        this.jobID = jobID;
    }

    public int getReminder() {
        return reminder;
    }

    public void setReminder(int reminder) {
        this.reminder = reminder;
    }

    public double getStockCost() {
        return stockCost;
    }

    public void setStockCost(double stockCost) {
        this.stockCost = stockCost;
    }

    public double getLabourCost() {
        return labourCost;
    }

    public void setLabourCost(double labourCost) {
        this.labourCost = labourCost;
    }

    public double getTotal() {
        return total;
    }

    public void setTotal(double total) {
        this.total = total;
    }

    public double getVAT() {
        return VAT;
    }

    public void setVAT(double VAT) {
        this.VAT = VAT;
    }

    public double getGrandtotal() {
        return grandtotal;
    }

    public void setGrandtotal(double grandtotal) {
        this.grandtotal = grandtotal;
    }

    public Date getInitalDate() {
        return initalDate;
    }

    public void setInitalDate(Date initalDate) {
        this.initalDate = initalDate;
    }

    public Date getLastDateReminder() {
        return lastDateReminder;
    }

    public void setLastDateReminder(Date lastDateReminder) {
        this.lastDateReminder = lastDateReminder;
    }

    public boolean isPaid() {
        return paid;
    }

    public void setPaid(boolean paid) {
        this.paid = paid;
    }   
    
}
