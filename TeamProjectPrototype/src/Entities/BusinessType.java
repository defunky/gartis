/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entities;

/**
 * An Entity class related to the Entity Business Types in the database
 * @author Abdalrahmane
 */
public class BusinessType {
    int busID;
    String type;

    public BusinessType(int busID, String type) {
        this.busID = busID;
        this.type = type;
    }

    public int getBusID() {
        return busID;
    }

    public void setBusID(int busID) {
        this.busID = busID;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
    
}
