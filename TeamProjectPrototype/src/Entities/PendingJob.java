/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entities;

import java.sql.Date;
import java.sql.Time;

/**
 * An Entity class related to the Pending Job list in the database.
 * As Pending Job is directly related to Job, it inherits all functions from the
 * Job class
 * @author Abdalrahmane
 */
public class PendingJob extends Job {
    String username;
    
    public PendingJob(int jobID, String regNo, String workReq, String jobStatus, String bussinessType, Time duration, Date created, String username) {
        super(jobID, regNo, workReq, jobStatus, bussinessType, duration, created);
        this.username = username;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }
    
}
