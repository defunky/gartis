/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package GUI.Franchisee;


import Entities.Invoice;
import java.sql.Date;
import java.util.ArrayList;
import javax.swing.table.AbstractTableModel;

/**
 *  Table model to display the invoice data
 * @author Abdalrahmane
 */
public class InvoiceTableModel extends AbstractTableModel {
    private ArrayList<Invoice> data;
    private String colNames[] = { "Invoice ID", "Job ID", "Paid","Stock Cost", "Labour Cost", "Total", "VAT", "Grand Total", "Inital Date", "Last Reminder Date"};
    private Class<?> colClasses[] = { Integer.class, Integer.class, Boolean.class, Double.class, Double.class, Double.class, Double.class, Double.class, Date.class, Date.class};

    /**
     * initalize the data for table
     * @param data 
     */
    public InvoiceTableModel(ArrayList<Invoice> data) {
        this.data = data;
    }

    /**
     * returns the row count
     * @return row count
     */
    @Override
    public int getRowCount() {
        return data.size();
    }
    
    /**
     * Returns column count
     * @return column count
     */
    @Override
    public int getColumnCount() {
        return colNames.length;
    }
    
    /**
     * Returns column name at specified index
     * @param columnIndex
     * @return column name
     */
    @Override
    public String getColumnName(int columnIndex) {
        return colNames[columnIndex];
    }
    
    /**
     * Returns the class of specified column
     * @param columnIndex
     * @return 
     */    
    @Override
    public Class<?> getColumnClass(int columnIndex) {
        return colClasses[columnIndex];
    }
    
    /**
     * Retrieve the data at specified value
     * this method is most important one as it is used to display the data correctly in the
     * jTable
     * @param rowIndex
     * @param columnIndex
     * @return 
     */
    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        if (columnIndex == 0) {
            return data.get(rowIndex).getInvoiceID();
        }
        if (columnIndex == 1) {
            return data.get(rowIndex).getJobID();
        }
        if (columnIndex == 2) {
            return data.get(rowIndex).isPaid();
        }
        if (columnIndex == 3){
            return data.get(rowIndex).getStockCost();
        }
        if(columnIndex == 4){
            return data.get(rowIndex).getLabourCost();
        }
        if(columnIndex == 5){
            return data.get(rowIndex).getTotal();
        }
        if(columnIndex == 6){
            return data.get(rowIndex).getVAT();
        }
        if(columnIndex == 7){
            return data.get(rowIndex).getGrandtotal();
        }
        if(columnIndex == 8){
            return data.get(rowIndex).getInitalDate();
        }
        if(columnIndex == 9){
            return data.get(rowIndex).getLastDateReminder();
        }
        return null;
    }
    
    /**
     * Refresh the data and update the table to display new values
     * @param data 
     */    
    public void refresh(ArrayList data){
        this.data = data;
        fireTableDataChanged();
    }
    
}
