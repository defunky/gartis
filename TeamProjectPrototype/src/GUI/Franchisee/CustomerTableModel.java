/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package GUI.Franchisee;

import Entities.Customer;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import javax.swing.table.AbstractTableModel;

/**
 * A table model for displaying customer data
 * @author Abdalrahmane
 */
public class CustomerTableModel extends AbstractTableModel {
    private ArrayList<Customer> data;
    private String colNames[] = { "Customer ID", "Name", "Telephone", "Mobile", "Email", "Address", "Street", "Locality", "City", "Post code", "Notes", "Type" };
    private Class<?> colClasses[] = { Integer.class, String.class, String.class, String.class, String.class, String.class, String.class, String.class, String.class, String.class, String.class, String.class };

    /**
     * initalize the data for table
     * @param data 
     */
    public CustomerTableModel(ArrayList data) {
        this.data = data;
    }
    
    /**
     * returns the row count
     * @return row count
     */
    @Override
    public int getRowCount() {
        return data.size();
    }
    
    /**
     * Returns column count
     * @return column count
     */
    @Override
    public int getColumnCount() {
        return colNames.length;
    }
    
    /**
     * Returns column name at specified index
     * @param columnIndex
     * @return column name
     */
    @Override
    public String getColumnName(int columnIndex) {
        return colNames[columnIndex];
    }
    
    /**
     * Returns the class of specified column
     * @param columnIndex
     * @return 
     */  
    @Override
    public Class<?> getColumnClass(int columnIndex) {
        return colClasses[columnIndex];
    }
    
    /**
     * Retrieve the data at specified value
     * this method is most important one as it is used to display the data correctly in the
     * jTable
     * @param rowIndex
     * @param columnIndex
     * @return 
     */
    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {

        if (columnIndex == 0) {
            return data.get(rowIndex).getCusID();
        }
        if (columnIndex == 1) {
            return data.get(rowIndex).getFname();
        }
        if (columnIndex == 2){
            return data.get(rowIndex).getTelephone();
        }
       if(columnIndex == 3){
            return data.get(rowIndex).getMobile();
        }
        if(columnIndex == 4){
            return data.get(rowIndex).getEmail();
        }
        if(columnIndex == 5){
            return data.get(rowIndex).getAddress();
        }
        if(columnIndex == 6){
            return data.get(rowIndex).getStreet();
        }
        if(columnIndex == 7){
            return data.get(rowIndex).getLocality();
        }
        if(columnIndex == 8){
            return data.get(rowIndex).getCity();
        }
        if(columnIndex == 9){
            return data.get(rowIndex).getPostcode();
        }
        if(columnIndex == 10){
            return data.get(rowIndex).getNotes();
        }
        if(columnIndex == 11){
            return data.get(rowIndex).getType();
        }
        return null;
    }
    
    /**
     * Refresh the data and update the table to display new values
     * @param data 
     */   
    public void refresh(ArrayList data){
        this.data = data;
        fireTableDataChanged();
    }
}
