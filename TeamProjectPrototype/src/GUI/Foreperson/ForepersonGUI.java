/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package GUI.Foreperson;

import GUI.Mechanic.*;
import Controllers.DatabaseConnection;
import Controllers.ForepersonController;
import Entities.Job;
import Entities.PendingJob;
import Entities.StockItem;
import Entities.Vehicle;
import GUI.Franchisee.VehicleTableModel;
import GUI.Login;
import GUI.Receptionist.JobInformationList;
import GUI.Receptionist.JobTableModel;
import GUI.Receptionist.ReceptionistGUI;
import GUI.Receptionist.StockTableModel;
import java.awt.event.ActionEvent;
import java.sql.Time;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Map.Entry;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.RowFilter;
import javax.swing.table.TableRowSorter;

/**
 * Foreperson GUI
 * @author Abdalrahmane
 */
public class ForepersonGUI extends javax.swing.JFrame {

    private ArrayList<Job> jobData;
    private ArrayList<JobTask> taskData;
    private ArrayList<StockItem> stockData;
    private ArrayList<Vehicle> vehData;
    private Object[] taskArray; //task
    private HashMap<String,Time> taskList;
    private ArrayList<Job> allJobData;
    private ArrayList<PendingJob> pendingListData;
    private int selectedRow, selectedCol;
    private int staffID, jobID, taskID; //used to find which person has taken the job
    private ForepersonController controller;
    private DatabaseConnection dbc;
    
    /**
     * Creates new form MechanicGUI
     */

    public ForepersonGUI(DatabaseConnection dbc, int staffID){
        controller = new ForepersonController(dbc, staffID);
        this.dbc = dbc;
        this.staffID = staffID;
        jobData = new ArrayList<>();
        taskData = new ArrayList<>();
        stockData = new ArrayList<>();
        vehData = new ArrayList<>();
        taskList = new HashMap<>();
        allJobData = new ArrayList<>();
        selectedRow = -1;
        selectedCol = -1;
        initData();
        initComponents();
        setLocationRelativeTo(null);
        setVisible(true);
        
    }
    
     /**
     * initalize the needed data
     */
    public void initData(){
        jobData = controller.getPendingJobData();
        stockData = controller.getStockData();
        taskList = controller.getTaskList();
        taskArray = taskList.keySet().toArray();
        allJobData = controller.getJobData();
        pendingListData = controller.getPendingListData();
    }
    
    /**
     * update the data and refresh the table to show the new information
     */
    public void updateData(){
        initData();
        jtm.refresh(jobData);
        stm.refresh(stockData);
        ajtm.refresh(allJobData);
        pltm.refresh(pendingListData);
        //update tasklist
        preTaskCombo.setModel(new DefaultComboBoxModel(taskArray));
        preTaskCombo1.setModel(new DefaultComboBoxModel(taskArray));
    }
    /**
     * initalize mechanics own selected data
     */
    public void initSelectedData(){
        jobData = controller.getSelectedPendingJobData();
        jtm.refresh(jobData);
    }
    /**
     * initalize the task data related to a job
     * @param jobID - selected job
     */
    public void initTaskData(int jobID){
        taskData = controller.getSelectedTaskData(jobID);
        jttm.refresh(taskData);
    }
   /**
     * initalize the vehicle data for a selected reg no
     * @param regNo selected reg no
     */
    public void initVehData(String regNo){
        vehData = controller.getCusVehData(regNo);
        vtm.refresh(vehData);
    }
    /**
     * Switches the panels dynamically to display different tables/information
     * @param type - the panel type
     */
    public void panelSwitcher(String type){
        mainPanel.removeAll();
        switch(type){
            case "viewJob":
                mainPanel.add(allJobPanel);
                break;
            case "viewSelected":
                mainPanel.add(selectedJobPanel);
                break;
            case "viewTask":
                mainPanel.add(viewTaskPanel);
                break;
            case "addTask":
                mainPanel.add(newTaskPanel);
                break;
            case "alterTask":
                mainPanel.add(alterTaskPanel);
                break;
            case "selectStock":
                mainPanel.add(viewStockPanel);
                break;
            case "viewVeh":
                mainPanel.add(viewVehPanel);
                break;
            case "pendingList":
                mainPanel.add(pendingList);
                break;
        }
        mainPanel.repaint();
        mainPanel.revalidate();
    }
    /**
     * Handles the logic for the job right click table
     * @param event 
     */
    private void jobRightClickMenu(ActionEvent event){
        updateData();
        JMenuItem menu = (JMenuItem) event.getSource(); //gets what the user selects
        int selected;
        try{
            selected = (int)jobTable.getValueAt(selectedRow, 0);
        }catch(Exception e){
            JOptionPane.showMessageDialog(null,
                "Please select a valid row.",
                "Invalid selection",
            JOptionPane.ERROR_MESSAGE);
            return;
        }
        
        if(menu == workReqMenu){
            new JobInformationList(jobData.get(selectedRow).getWorkReq());
        }else if(menu == assignJob){
            controller.assignJob(selected);
        }
        updateData();
    }
     /**
     * Handles the logic for the assigned jobs right click table
     * @param event 
     */
    private void selectedRightClickMenu(ActionEvent event){
        initSelectedData();
        JMenuItem menu = (JMenuItem) event.getSource(); //gets what the user selects
        int selected;
        try{
            selected = (int)jobTable.getValueAt(selectedRow, 0);
        }catch(Exception e){
            JOptionPane.showMessageDialog(null,
                "Please select a valid row.",
                "Invalid selection",
            JOptionPane.ERROR_MESSAGE);
            e.printStackTrace();
            return;
        } 
        if(menu == selectedWorkReqMenu){
            for (Job p : jobData) {
                if (p.getJobID() == (selected)) {
                    new JobInformationList(p.getWorkReq());
                    break;
                }
            }
        }else if(menu == assignJob){
            controller.assignJob(selected);
        }else if(menu == viewTaskMenu){
            initTaskData(selected);
            panelSwitcher("viewTask");
        }else if(menu == addTaskMenu){
            jobID = selected;
            panelSwitcher("addTask");
        }else if(menu == alterJobStatus){
            String[] choices = { "IDLE", "IN PROGRESS", "COMPLETED"};
            String input = (String) JOptionPane.showInputDialog(null, "Select new job status",
                "Select new job status", JOptionPane.QUESTION_MESSAGE, null, choices, choices[0]);
            if(input != null){
                controller.alterJobStatus(selected, input);
            }
        }else if(menu == selectPartMenu){
            jobID = selected;
            panelSwitcher("selectStock");
        }else if(menu == viewVehMenu){
            jobID = selected;
            initVehData((String)jobTable.getValueAt(selectedRow, 1));
            panelSwitcher("viewVeh");
        }else if(menu == generateJobSheet){
            if(!((String)jobTable.getValueAt(selectedRow, 2)).equalsIgnoreCase("COMPLETED")){
                JOptionPane.showMessageDialog(null,
                    "Job must be completed before a Job sheet can be generated.",
                    "Error",
                    JOptionPane.ERROR_MESSAGE);
                return;
            }
            controller.generateJobsheet(selected, (String)jobTable.getValueAt(selectedRow, 1));
            
        }
        initSelectedData();
    }
    /**
     * Handles the logic of the task right click table
     * @param event 
     */
    private void taskRightClickMenu(ActionEvent event){
        JMenuItem menu = (JMenuItem) event.getSource(); //gets what the user selects
        int selected;
        try{
            selected = (int)taskTable.getValueAt(selectedRow, 0);
        }catch(Exception e){
            JOptionPane.showMessageDialog(null,
                "Please select a valid row.",
                "Invalid selection",
            JOptionPane.ERROR_MESSAGE);
            e.printStackTrace();
            return;
        }
        
        if(menu == deleteTaskMenu){
            controller.deleteTask(selected);
            initTaskData((int)taskTable.getValueAt(selectedRow, 1));
            panelSwitcher("viewTask");
        }else if(menu == alterTaskMenu){
            taskID = selected;
            jobID = (int)taskTable.getValueAt(selectedRow, 1);
            taskField1.setText((String)taskTable.getValueAt(selectedRow, 2));
            String dur = (String) taskTable.getValueAt(selectedRow, 3);
            String[] spl = dur.split(":");
            hourSpinner1.setValue(Integer.parseInt(spl[0]));
            minSpinner1.setValue(Integer.parseInt(spl[1]));
            secSpinner1.setValue(Integer.parseInt(spl[2]));
            panelSwitcher("alterTask");
        }
    }
    
    /**
     * Handles the logic of the stock right click table
     * @param event 
     */
    private void stockRightClickMenu(ActionEvent event){
        JMenuItem menu = (JMenuItem) event.getSource(); //gets what the user selects
        String selected;
        try{
            selected = (String)stockTable.getValueAt(selectedRow, 0);
        }catch(Exception e){
            JOptionPane.showMessageDialog(null,
                "Please select a valid row.",
                "Invalid selection",
            JOptionPane.ERROR_MESSAGE);
            e.printStackTrace();
            return;
        }
        
        if(menu == selectQuanMenu){
            boolean errorFlag = false;

            String input = (String)JOptionPane.showInputDialog(null, "Select amount",
                "Select amount", JOptionPane.QUESTION_MESSAGE);
            int amountUsed = Integer.parseInt(input);
            int newLevel = (int)stockTable.getValueAt(selectedRow, 7) - Integer.parseInt(input);
            if(newLevel < 0){
                JOptionPane.showMessageDialog(null,
                    "You cannot use more than what is currently in-stock!",
                    "Invalid quantity",
                    JOptionPane.ERROR_MESSAGE);
                errorFlag = true;
            }
            if(input != null && !errorFlag){
                controller.useStockPart(selected, newLevel,(double)stockTable.getValueAt(selectedRow, 6), jobID, amountUsed);
            }
        }
        updateData();
    }
    /**
     * Filters the table with a filter (users text in quick search field)
     * @param filter 
     */
    private void searchTable(String filter){
        jobTable.setRowSorter(jobSorter);
        selectedJobTable.setRowSorter(jobSorter);
        taskTable.setRowSorter(taskSorter);
        stockTable.setRowSorter(stockSorter);
        //vehTable.setRowSorter(vehSorter);
        //(?i) is used to ignore case in the search term
        jobSorter.setRowFilter(RowFilter.regexFilter("(?i)"+filter));
        taskSorter.setRowFilter(RowFilter.regexFilter("(?i)"+filter));
        //vehSorter.setRowFilter(RowFilter.regexFilter("(?i)"+filter));
        stockSorter.setRowFilter(RowFilter.regexFilter("(?i)"+filter));
        
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jobRightClickMenu = new javax.swing.JPopupMenu();
        workReqMenu = new javax.swing.JMenuItem();
        assignJob = new javax.swing.JMenuItem();
        selectedRightClickMenu = new javax.swing.JPopupMenu();
        selectedWorkReqMenu = new javax.swing.JMenuItem();
        viewTaskMenu = new javax.swing.JMenuItem();
        addTaskMenu = new javax.swing.JMenuItem();
        selectPartMenu = new javax.swing.JMenuItem();
        alterJobStatus = new javax.swing.JMenuItem();
        viewVehMenu = new javax.swing.JMenuItem();
        generateJobSheet = new javax.swing.JMenuItem();
        taskRightClickMenu = new javax.swing.JPopupMenu();
        deleteTaskMenu = new javax.swing.JMenuItem();
        alterTaskMenu = new javax.swing.JMenuItem();
        stockRightClickMenu = new javax.swing.JPopupMenu();
        selectQuanMenu = new javax.swing.JMenuItem();
        pJobRightClickMenu = new javax.swing.JPopupMenu();
        jobToList = new javax.swing.JMenuItem();
        sideMenuPanel = new javax.swing.JPanel();
        viewSelectedBtn = new javax.swing.JButton();
        viewPendingBtn = new javax.swing.JButton();
        userConLabel = new javax.swing.JLabel();
        updateTableBtn = new javax.swing.JButton();
        searchLabel = new javax.swing.JLabel();
        searchField = new javax.swing.JTextField();
        pendingListBtn = new javax.swing.JButton();
        mainPanel = new javax.swing.JPanel();
        pendingList = new javax.swing.JPanel();
        jScrollPane6 = new javax.swing.JScrollPane();
        allJobTable = new javax.swing.JTable();
        jScrollPane7 = new javax.swing.JScrollPane();
        pendingListTable = new javax.swing.JTable();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        allJobPanel = new javax.swing.JPanel();
        jScrollPane3 = new javax.swing.JScrollPane();
        jobTable = new javax.swing.JTable();
        selectedJobPanel = new javax.swing.JPanel();
        jScrollPane4 = new javax.swing.JScrollPane();
        selectedJobTable = new javax.swing.JTable();
        viewTaskPanel = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        taskTable = new javax.swing.JTable();
        newTaskPanel = new javax.swing.JPanel();
        taskTitleLabel = new javax.swing.JLabel();
        descLabel = new javax.swing.JLabel();
        taskField = new javax.swing.JTextField();
        hourSpinner = new javax.swing.JSpinner();
        minSpinner = new javax.swing.JSpinner();
        secSpinner = new javax.swing.JSpinner();
        preTaskCombo = new javax.swing.JComboBox();
        durationLabel = new javax.swing.JLabel();
        hourLabel = new javax.swing.JLabel();
        minLabel = new javax.swing.JLabel();
        secLabel = new javax.swing.JLabel();
        newTaskBtn = new javax.swing.JButton();
        alterTaskPanel = new javax.swing.JPanel();
        taskTitleLabel1 = new javax.swing.JLabel();
        descLabel1 = new javax.swing.JLabel();
        taskField1 = new javax.swing.JTextField();
        hourSpinner1 = new javax.swing.JSpinner();
        minSpinner1 = new javax.swing.JSpinner();
        secSpinner1 = new javax.swing.JSpinner();
        preTaskCombo1 = new javax.swing.JComboBox();
        durationLabel1 = new javax.swing.JLabel();
        hourLabel1 = new javax.swing.JLabel();
        minLabel1 = new javax.swing.JLabel();
        secLabel1 = new javax.swing.JLabel();
        alterTaskBtn = new javax.swing.JButton();
        viewStockPanel = new javax.swing.JPanel();
        jScrollPane5 = new javax.swing.JScrollPane();
        stockTable = new javax.swing.JTable();
        viewVehPanel = new javax.swing.JPanel();
        jScrollPane2 = new javax.swing.JScrollPane();
        vehTable = new javax.swing.JTable();
        jMenuBar1 = new javax.swing.JMenuBar();
        jMenu1 = new javax.swing.JMenu();
        Logout = new javax.swing.JMenuItem();
        jMenu2 = new javax.swing.JMenu();
        launchReceptionistGUI = new javax.swing.JMenuItem();

        workReqMenu.setText("View Work Required");
        workReqMenu.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                workReqMenuActionPerformed(evt);
            }
        });
        jobRightClickMenu.add(workReqMenu);

        assignJob.setText("Assign job to self");
        assignJob.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                assignJobActionPerformed(evt);
            }
        });
        jobRightClickMenu.add(assignJob);

        jobTable.setComponentPopupMenu(jobRightClickMenu);

        selectedWorkReqMenu.setText("View work required");
        selectedWorkReqMenu.setToolTipText("");
        selectedWorkReqMenu.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                selectedWorkReqMenuActionPerformed(evt);
            }
        });
        selectedRightClickMenu.add(selectedWorkReqMenu);

        viewTaskMenu.setText("View associated tasks");
        viewTaskMenu.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                viewTaskMenuActionPerformed(evt);
            }
        });
        selectedRightClickMenu.add(viewTaskMenu);

        addTaskMenu.setText("Add a task to selected job");
        addTaskMenu.setToolTipText("");
        addTaskMenu.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                addTaskMenuActionPerformed(evt);
            }
        });
        selectedRightClickMenu.add(addTaskMenu);

        selectPartMenu.setText("Select a Stock Part to use");
        selectPartMenu.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                selectPartMenuActionPerformed(evt);
            }
        });
        selectedRightClickMenu.add(selectPartMenu);

        alterJobStatus.setText("Modify Job status");
        alterJobStatus.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                alterJobStatusActionPerformed(evt);
            }
        });
        selectedRightClickMenu.add(alterJobStatus);

        viewVehMenu.setText("View Vehicle information");
        viewVehMenu.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                viewVehMenuActionPerformed(evt);
            }
        });
        selectedRightClickMenu.add(viewVehMenu);

        generateJobSheet.setText("Generate Job Sheet");
        generateJobSheet.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                generateJobSheetActionPerformed(evt);
            }
        });
        selectedRightClickMenu.add(generateJobSheet);

        selectedJobTable.setComponentPopupMenu(selectedRightClickMenu);

        deleteTaskMenu.setText("Delete selected task");
        deleteTaskMenu.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                deleteTaskMenuActionPerformed(evt);
            }
        });
        taskRightClickMenu.add(deleteTaskMenu);

        alterTaskMenu.setText("Alter task information");
        alterTaskMenu.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                alterTaskMenuActionPerformed(evt);
            }
        });
        taskRightClickMenu.add(alterTaskMenu);

        taskTable.setComponentPopupMenu(taskRightClickMenu);

        selectQuanMenu.setText("Select amount of parts");
        selectQuanMenu.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                selectQuanMenuActionPerformed(evt);
            }
        });
        stockRightClickMenu.add(selectQuanMenu);

        stockTable.setComponentPopupMenu(stockRightClickMenu);

        jobToList.setText("Add Job to Pending List");
        jobToList.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jobToListActionPerformed(evt);
            }
        });
        pJobRightClickMenu.add(jobToList);

        allJobTable.setComponentPopupMenu(pJobRightClickMenu);

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("GARTIS - Foreperson");

        sideMenuPanel.setBackground(new java.awt.Color(255, 255, 255));
        sideMenuPanel.setBorder(new javax.swing.border.LineBorder(new java.awt.Color(0, 0, 0), 1, true));

        viewSelectedBtn.setText("View Selected Jobs");
        viewSelectedBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                viewSelectedBtnActionPerformed(evt);
            }
        });

        viewPendingBtn.setText("View Pending Jobs");
        viewPendingBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                viewPendingBtnActionPerformed(evt);
            }
        });

        userConLabel.setText("User Control");

        updateTableBtn.setText("Update Table Data");
        updateTableBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                updateTableBtnActionPerformed(evt);
            }
        });

        searchLabel.setText("Quick Search");

        searchField.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                searchFieldKeyReleased(evt);
            }
        });

        pendingListBtn.setText("Add to Pending List");
        pendingListBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                pendingListBtnActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout sideMenuPanelLayout = new javax.swing.GroupLayout(sideMenuPanel);
        sideMenuPanel.setLayout(sideMenuPanelLayout);
        sideMenuPanelLayout.setHorizontalGroup(
            sideMenuPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(sideMenuPanelLayout.createSequentialGroup()
                .addGroup(sideMenuPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(sideMenuPanelLayout.createSequentialGroup()
                        .addGap(52, 52, 52)
                        .addComponent(userConLabel)
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addGroup(sideMenuPanelLayout.createSequentialGroup()
                        .addContainerGap()
                        .addGroup(sideMenuPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(sideMenuPanelLayout.createSequentialGroup()
                                .addGap(37, 37, 37)
                                .addComponent(searchLabel)
                                .addGap(0, 40, Short.MAX_VALUE))
                            .addComponent(searchField, javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(updateTableBtn, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(viewSelectedBtn, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(viewPendingBtn, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(pendingListBtn, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))
                .addContainerGap())
        );
        sideMenuPanelLayout.setVerticalGroup(
            sideMenuPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(sideMenuPanelLayout.createSequentialGroup()
                .addGap(19, 19, 19)
                .addComponent(userConLabel)
                .addGap(18, 18, 18)
                .addComponent(pendingListBtn)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(viewPendingBtn)
                .addGap(10, 10, 10)
                .addComponent(updateTableBtn)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(viewSelectedBtn)
                .addGap(18, 18, 18)
                .addComponent(searchLabel)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(searchField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        mainPanel.setLayout(new java.awt.CardLayout());

        allJobTable.setModel(ajtm = new JobTableModel(allJobData));
        allJobTable.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseReleased(java.awt.event.MouseEvent evt) {
                allJobTableMouseReleased(evt);
            }
        });
        jScrollPane6.setViewportView(allJobTable);

        pendingListTable.setModel(pltm = new PendingListTableModel(pendingListData));
        jScrollPane7.setViewportView(pendingListTable);

        jLabel1.setText("All Jobs");

        jLabel2.setText("Pending Job List");

        javax.swing.GroupLayout pendingListLayout = new javax.swing.GroupLayout(pendingList);
        pendingList.setLayout(pendingListLayout);
        pendingListLayout.setHorizontalGroup(
            pendingListLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pendingListLayout.createSequentialGroup()
                .addGap(188, 188, 188)
                .addComponent(jLabel1)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jLabel2)
                .addGap(199, 199, 199))
            .addGroup(pendingListLayout.createSequentialGroup()
                .addComponent(jScrollPane6, javax.swing.GroupLayout.PREFERRED_SIZE, 476, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane7, javax.swing.GroupLayout.PREFERRED_SIZE, 505, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
        );
        pendingListLayout.setVerticalGroup(
            pendingListLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pendingListLayout.createSequentialGroup()
                .addGap(0, 0, Short.MAX_VALUE)
                .addGroup(pendingListLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1)
                    .addComponent(jLabel2))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(pendingListLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jScrollPane6)
                    .addComponent(jScrollPane7, javax.swing.GroupLayout.DEFAULT_SIZE, 464, Short.MAX_VALUE)))
        );

        mainPanel.add(pendingList, "card9");

        jobTable.setModel(jtm = new JobTableModel(jobData));
        jobSorter = new TableRowSorter<JobTableModel>(jtm);
        jobTable.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseReleased(java.awt.event.MouseEvent evt) {
                jobTableMouseReleased(evt);
            }
        });
        jScrollPane3.setViewportView(jobTable);

        javax.swing.GroupLayout allJobPanelLayout = new javax.swing.GroupLayout(allJobPanel);
        allJobPanel.setLayout(allJobPanelLayout);
        allJobPanelLayout.setHorizontalGroup(
            allJobPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane3, javax.swing.GroupLayout.DEFAULT_SIZE, 987, Short.MAX_VALUE)
        );
        allJobPanelLayout.setVerticalGroup(
            allJobPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane3, javax.swing.GroupLayout.DEFAULT_SIZE, 484, Short.MAX_VALUE)
        );

        mainPanel.add(allJobPanel, "card2");

        selectedJobTable.setModel(jtm = new JobTableModel(jobData));
        selectedJobTable.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseReleased(java.awt.event.MouseEvent evt) {
                selectedJobTableMouseReleased(evt);
            }
        });
        jScrollPane4.setViewportView(selectedJobTable);

        javax.swing.GroupLayout selectedJobPanelLayout = new javax.swing.GroupLayout(selectedJobPanel);
        selectedJobPanel.setLayout(selectedJobPanelLayout);
        selectedJobPanelLayout.setHorizontalGroup(
            selectedJobPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane4, javax.swing.GroupLayout.DEFAULT_SIZE, 987, Short.MAX_VALUE)
        );
        selectedJobPanelLayout.setVerticalGroup(
            selectedJobPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane4, javax.swing.GroupLayout.DEFAULT_SIZE, 484, Short.MAX_VALUE)
        );

        mainPanel.add(selectedJobPanel, "card2");

        taskTable.setModel(jttm = new JobTaskTableModel(taskData));
        taskSorter = new TableRowSorter<JobTaskTableModel>(jttm);
        taskTable.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseReleased(java.awt.event.MouseEvent evt) {
                taskTableMouseReleased(evt);
            }
        });
        jScrollPane1.setViewportView(taskTable);

        javax.swing.GroupLayout viewTaskPanelLayout = new javax.swing.GroupLayout(viewTaskPanel);
        viewTaskPanel.setLayout(viewTaskPanelLayout);
        viewTaskPanelLayout.setHorizontalGroup(
            viewTaskPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 987, Short.MAX_VALUE)
        );
        viewTaskPanelLayout.setVerticalGroup(
            viewTaskPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 484, Short.MAX_VALUE)
        );

        mainPanel.add(viewTaskPanel, "card4");

        newTaskPanel.setPreferredSize(new java.awt.Dimension(875, 475));

        taskTitleLabel.setFont(new java.awt.Font("Tahoma", 0, 36)); // NOI18N
        taskTitleLabel.setText("Adding a new task");

        descLabel.setText("Task Description:");

        preTaskCombo.setModel(new DefaultComboBoxModel(taskArray));
        preTaskCombo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                preTaskComboActionPerformed(evt);
            }
        });

        durationLabel.setText("Task Duration:");

        hourLabel.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        hourLabel.setText("Hours");

        minLabel.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        minLabel.setText("Minutes");

        secLabel.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        secLabel.setText("Seconds");

        newTaskBtn.setText("Add new Task");
        newTaskBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                newTaskBtnActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout newTaskPanelLayout = new javax.swing.GroupLayout(newTaskPanel);
        newTaskPanel.setLayout(newTaskPanelLayout);
        newTaskPanelLayout.setHorizontalGroup(
            newTaskPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(newTaskPanelLayout.createSequentialGroup()
                .addGroup(newTaskPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(newTaskPanelLayout.createSequentialGroup()
                        .addGap(284, 284, 284)
                        .addComponent(taskTitleLabel))
                    .addGroup(newTaskPanelLayout.createSequentialGroup()
                        .addGap(351, 351, 351)
                        .addComponent(preTaskCombo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(newTaskPanelLayout.createSequentialGroup()
                        .addGap(293, 293, 293)
                        .addComponent(descLabel)
                        .addGap(42, 42, 42)
                        .addComponent(taskField, javax.swing.GroupLayout.PREFERRED_SIZE, 160, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(newTaskPanelLayout.createSequentialGroup()
                        .addGap(421, 421, 421)
                        .addComponent(hourLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(minLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(secLabel))
                    .addGroup(newTaskPanelLayout.createSequentialGroup()
                        .addGap(293, 293, 293)
                        .addComponent(durationLabel)
                        .addGap(58, 58, 58)
                        .addComponent(hourSpinner, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(minSpinner, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(secSpinner, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, newTaskPanelLayout.createSequentialGroup()
                .addGap(0, 0, Short.MAX_VALUE)
                .addComponent(newTaskBtn)
                .addGap(226, 226, 226))
        );
        newTaskPanelLayout.setVerticalGroup(
            newTaskPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(newTaskPanelLayout.createSequentialGroup()
                .addGap(23, 23, 23)
                .addComponent(taskTitleLabel)
                .addGap(34, 34, 34)
                .addComponent(preTaskCombo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(62, 62, 62)
                .addGroup(newTaskPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(taskField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(descLabel))
                .addGap(84, 84, 84)
                .addGroup(newTaskPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(hourLabel)
                    .addComponent(minLabel)
                    .addComponent(secLabel))
                .addGap(6, 6, 6)
                .addGroup(newTaskPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(newTaskPanelLayout.createSequentialGroup()
                        .addGap(3, 3, 3)
                        .addComponent(durationLabel))
                    .addComponent(hourSpinner, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(minSpinner, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(secSpinner, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(59, 59, 59)
                .addComponent(newTaskBtn)
                .addContainerGap(75, Short.MAX_VALUE))
        );

        mainPanel.add(newTaskPanel, "card5");

        taskTitleLabel1.setFont(new java.awt.Font("Tahoma", 0, 36)); // NOI18N
        taskTitleLabel1.setText("Altering a task");

        descLabel1.setText("Task Description:");

        preTaskCombo1.setModel(new DefaultComboBoxModel(taskArray));
        preTaskCombo1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                preTaskCombo1ActionPerformed(evt);
            }
        });

        durationLabel1.setText("Task Duration:");

        hourLabel1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        hourLabel1.setText("Hours");

        minLabel1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        minLabel1.setText("Minutes");

        secLabel1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        secLabel1.setText("Seconds");

        alterTaskBtn.setText("Alter exisiting Task");
        alterTaskBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                alterTaskBtnActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout alterTaskPanelLayout = new javax.swing.GroupLayout(alterTaskPanel);
        alterTaskPanel.setLayout(alterTaskPanelLayout);
        alterTaskPanelLayout.setHorizontalGroup(
            alterTaskPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, alterTaskPanelLayout.createSequentialGroup()
                .addGap(0, 0, Short.MAX_VALUE)
                .addComponent(alterTaskBtn)
                .addGap(226, 226, 226))
            .addGroup(alterTaskPanelLayout.createSequentialGroup()
                .addGroup(alterTaskPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(alterTaskPanelLayout.createSequentialGroup()
                        .addGap(351, 351, 351)
                        .addComponent(preTaskCombo1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(alterTaskPanelLayout.createSequentialGroup()
                        .addGap(293, 293, 293)
                        .addComponent(descLabel1)
                        .addGap(42, 42, 42)
                        .addComponent(taskField1, javax.swing.GroupLayout.PREFERRED_SIZE, 160, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(alterTaskPanelLayout.createSequentialGroup()
                        .addGap(421, 421, 421)
                        .addComponent(hourLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(minLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(secLabel1))
                    .addGroup(alterTaskPanelLayout.createSequentialGroup()
                        .addGap(293, 293, 293)
                        .addComponent(durationLabel1)
                        .addGap(58, 58, 58)
                        .addComponent(hourSpinner1, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(minSpinner1, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(secSpinner1, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(alterTaskPanelLayout.createSequentialGroup()
                        .addGap(285, 285, 285)
                        .addComponent(taskTitleLabel1)))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        alterTaskPanelLayout.setVerticalGroup(
            alterTaskPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(alterTaskPanelLayout.createSequentialGroup()
                .addGap(27, 27, 27)
                .addComponent(taskTitleLabel1)
                .addGap(30, 30, 30)
                .addComponent(preTaskCombo1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(62, 62, 62)
                .addGroup(alterTaskPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(taskField1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(descLabel1))
                .addGap(84, 84, 84)
                .addGroup(alterTaskPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(hourLabel1)
                    .addComponent(minLabel1)
                    .addComponent(secLabel1))
                .addGap(6, 6, 6)
                .addGroup(alterTaskPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(alterTaskPanelLayout.createSequentialGroup()
                        .addGap(3, 3, 3)
                        .addComponent(durationLabel1))
                    .addComponent(hourSpinner1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(minSpinner1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(secSpinner1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(59, 59, 59)
                .addComponent(alterTaskBtn)
                .addContainerGap(75, Short.MAX_VALUE))
        );

        mainPanel.add(alterTaskPanel, "card5");

        stockTable.setModel(stm = new StockTableModel(stockData));
        stockSorter = new TableRowSorter<StockTableModel>(stm);
        stockTable.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseReleased(java.awt.event.MouseEvent evt) {
                stockTableMouseReleased(evt);
            }
        });
        jScrollPane5.setViewportView(stockTable);

        javax.swing.GroupLayout viewStockPanelLayout = new javax.swing.GroupLayout(viewStockPanel);
        viewStockPanel.setLayout(viewStockPanelLayout);
        viewStockPanelLayout.setHorizontalGroup(
            viewStockPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane5, javax.swing.GroupLayout.DEFAULT_SIZE, 987, Short.MAX_VALUE)
        );
        viewStockPanelLayout.setVerticalGroup(
            viewStockPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane5, javax.swing.GroupLayout.DEFAULT_SIZE, 484, Short.MAX_VALUE)
        );

        mainPanel.add(viewStockPanel, "card5");

        vehTable.setModel(vtm = new VehicleTableModel(vehData));
        vehTable.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseReleased(java.awt.event.MouseEvent evt) {
                vehTableMouseReleased(evt);
            }
        });
        jScrollPane2.setViewportView(vehTable);

        javax.swing.GroupLayout viewVehPanelLayout = new javax.swing.GroupLayout(viewVehPanel);
        viewVehPanel.setLayout(viewVehPanelLayout);
        viewVehPanelLayout.setHorizontalGroup(
            viewVehPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane2, javax.swing.GroupLayout.DEFAULT_SIZE, 987, Short.MAX_VALUE)
        );
        viewVehPanelLayout.setVerticalGroup(
            viewVehPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane2, javax.swing.GroupLayout.DEFAULT_SIZE, 484, Short.MAX_VALUE)
        );

        mainPanel.add(viewVehPanel, "card3");

        jMenu1.setText("File");

        Logout.setText("Logout");
        Logout.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                LogoutActionPerformed(evt);
            }
        });
        jMenu1.add(Logout);

        jMenuBar1.add(jMenu1);

        jMenu2.setText("View");

        launchReceptionistGUI.setText("Launch Receptionist GUI");
        launchReceptionistGUI.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                launchReceptionistGUIActionPerformed(evt);
            }
        });
        jMenu2.add(launchReceptionistGUI);

        jMenuBar1.add(jMenu2);

        setJMenuBar(jMenuBar1);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(sideMenuPanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(mainPanel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(mainPanel, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(sideMenuPanel, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void viewSelectedBtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_viewSelectedBtnActionPerformed
        initSelectedData();
        panelSwitcher("viewSelected"); //view select data
    }//GEN-LAST:event_viewSelectedBtnActionPerformed

    private void viewPendingBtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_viewPendingBtnActionPerformed
        updateData();
        panelSwitcher("viewJob"); //view pending jobs
    }//GEN-LAST:event_viewPendingBtnActionPerformed

    private void updateTableBtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_updateTableBtnActionPerformed
        updateData(); //update data and refresh tables
    }//GEN-LAST:event_updateTableBtnActionPerformed

    private void searchFieldKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_searchFieldKeyReleased
        searchTable(searchField.getText()); //get textfrom dynamic search
    }//GEN-LAST:event_searchFieldKeyReleased
   /**
     * retrieve row and col for a selected item in job table
     * @param evt 
     */
    private void jobTableMouseReleased(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jobTableMouseReleased
        selectedRow = jobTable.getSelectedRow();
        selectedCol = jobTable.getSelectedColumn();
    }//GEN-LAST:event_jobTableMouseReleased

    private void workReqMenuActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_workReqMenuActionPerformed
        jobRightClickMenu(evt); //view work required right click menu
    }//GEN-LAST:event_workReqMenuActionPerformed

    private void assignJobActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_assignJobActionPerformed
        jobRightClickMenu(evt); //called when user assigs job tohimself through right click
    }//GEN-LAST:event_assignJobActionPerformed
    /**
     * retrieve row and col for a selected item in selected job table
     * @param evt 
     */
    private void selectedJobTableMouseReleased(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_selectedJobTableMouseReleased
        selectedRow = selectedJobTable.getSelectedRow();
        selectedCol = selectedJobTable.getSelectedColumn();
    }//GEN-LAST:event_selectedJobTableMouseReleased

    private void selectedWorkReqMenuActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_selectedWorkReqMenuActionPerformed
        selectedRightClickMenu(evt); //view work required for an assigned job
    }//GEN-LAST:event_selectedWorkReqMenuActionPerformed

    private void addTaskMenuActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_addTaskMenuActionPerformed
        selectedRightClickMenu(evt); //go to task detail panel to fill information in
    }//GEN-LAST:event_addTaskMenuActionPerformed

    private void viewTaskMenuActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_viewTaskMenuActionPerformed
        selectedRightClickMenu(evt); //called when user selects view task for a job
    }//GEN-LAST:event_viewTaskMenuActionPerformed
    /**
     * When a predefined task is selected, the task is searched in hashmap to retrieve duration
     * this is then added automatically to the jspinners
     * @param evt 
     */
    private void preTaskComboActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_preTaskComboActionPerformed
        //Predefined Tasks, Oil Check, Engine Check, Tyre Replacement, Fender Replacement, Windscreen Replacement
        String selectedTask = (String)preTaskCombo.getSelectedItem();
        taskField.setText(selectedTask);
        String duration = taskList.get(selectedTask).toString();
        String[] spl = duration.split(":");
        hourSpinner.setValue(Integer.parseInt(spl[0]));
        minSpinner.setValue(Integer.parseInt(spl[1]));
        secSpinner.setValue(Integer.parseInt(spl[2]));
    }//GEN-LAST:event_preTaskComboActionPerformed
    /**
     * The method is called when new task is confirmed. 
     * The information is then passed to controller, and display the task information
     * for that job
     * @param evt 
     */
    private void newTaskBtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_newTaskBtnActionPerformed
        controller.addNewTask(jobID, taskField.getText(),(int)hourSpinner.getValue(),(int)minSpinner.getValue(),(int)secSpinner.getValue());
        initTaskData(jobID);
        panelSwitcher("viewTask");
    }//GEN-LAST:event_newTaskBtnActionPerformed
    /**
     * When a predefined task is selected, the task is searched in hashmap to retrieve duration
     * this is then added automatically to the jspinners
     * this method is called for the alter task combo box
     * @param evt 
     */
    private void preTaskCombo1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_preTaskCombo1ActionPerformed
        //Predefined Tasks, Oil Check, Engine Check, Tyre Replacement, Fender Replacement, Windscreen Replacement
        String selectedTask = (String)preTaskCombo1.getSelectedItem();
        taskField1.setText(selectedTask);
        String duration = taskList.get(selectedTask).toString();
        String[] spl = duration.split(":");
        hourSpinner1.setValue(Integer.parseInt(spl[0]));
        minSpinner1.setValue(Integer.parseInt(spl[1]));
        secSpinner1.setValue(Integer.parseInt(spl[2]));
    }//GEN-LAST:event_preTaskCombo1ActionPerformed
    /**
     * The method is called when alter task button is pressed.
     * The information is passed to controller and alters task information
     * @param evt 
     */
    private void alterTaskBtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_alterTaskBtnActionPerformed
        controller.alterTask(taskID,taskField1.getText(), (int)hourSpinner1.getValue(), 
                (int)minSpinner1.getValue(), (int)secSpinner1.getValue());
        initTaskData(jobID);
        panelSwitcher("viewTask");
    }//GEN-LAST:event_alterTaskBtnActionPerformed

    private void deleteTaskMenuActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_deleteTaskMenuActionPerformed
        taskRightClickMenu(evt); //called when user deletes task through right click menu
    }//GEN-LAST:event_deleteTaskMenuActionPerformed

    private void alterTaskMenuActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_alterTaskMenuActionPerformed
        taskRightClickMenu(evt); //called when user alters task through right click menu
    }//GEN-LAST:event_alterTaskMenuActionPerformed
    /**
     * gets row and col for task table for a selected task item
     * @param evt 
     */
    private void taskTableMouseReleased(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_taskTableMouseReleased
        selectedRow = taskTable.getSelectedRow();
        selectedCol = taskTable.getSelectedColumn();
    }//GEN-LAST:event_taskTableMouseReleased

    private void alterJobStatusActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_alterJobStatusActionPerformed
        selectedRightClickMenu(evt);
    }//GEN-LAST:event_alterJobStatusActionPerformed
    /**
     * gets row and col for stock table for a selected stock item
     * @param evt 
     */
    private void stockTableMouseReleased(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_stockTableMouseReleased
        selectedRow = stockTable.getSelectedRow();
        selectedCol = stockTable.getSelectedColumn();
    }//GEN-LAST:event_stockTableMouseReleased

    private void selectPartMenuActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_selectPartMenuActionPerformed
        selectedRightClickMenu(evt); //called when user clicks select part to use for assigned job
    }//GEN-LAST:event_selectPartMenuActionPerformed

    private void selectQuanMenuActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_selectQuanMenuActionPerformed
        stockRightClickMenu(evt); //called when user clicks on select quantity amount for a selected stock item
    }//GEN-LAST:event_selectQuanMenuActionPerformed

    /**
     * gets row and col for vehicle table for a selected vehicles
     * @param evt 
     */
    private void vehTableMouseReleased(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_vehTableMouseReleased
        selectedRow = vehTable.getSelectedRow();
        selectedCol = vehTable.getSelectedColumn();
    }//GEN-LAST:event_vehTableMouseReleased

    private void viewVehMenuActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_viewVehMenuActionPerformed
        selectedRightClickMenu(evt); //called when user tries to view vehicles details for assigned jobs
    }//GEN-LAST:event_viewVehMenuActionPerformed

    private void generateJobSheetActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_generateJobSheetActionPerformed
        selectedRightClickMenu(evt); //called when user tries to generate a job sheet
    }//GEN-LAST:event_generateJobSheetActionPerformed

    private void pendingListBtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_pendingListBtnActionPerformed
        panelSwitcher("pendingList"); //swap to pending list panel
    }//GEN-LAST:event_pendingListBtnActionPerformed
    /**
     * gets row and col for vehicle table for a all jobs table
     * @param evt 
     */
    private void allJobTableMouseReleased(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_allJobTableMouseReleased
        selectedRow = allJobTable.getSelectedRow();
        selectedCol = allJobTable.getSelectedColumn();
    }//GEN-LAST:event_allJobTableMouseReleased
    /**
     * This method is called when assigning a job to the pending list through right click menu
     * @param evt 
     */
    
    private void jobToListActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jobToListActionPerformed
        controller.addJobToList((int)allJobTable.getValueAt(selectedRow, 0));
        updateData();
    }//GEN-LAST:event_jobToListActionPerformed
    /**
     * launch receptionist GUI through menubar
     * @param evt 
     */
    private void launchReceptionistGUIActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_launchReceptionistGUIActionPerformed
        new ReceptionistGUI(dbc);
    }//GEN-LAST:event_launchReceptionistGUIActionPerformed
    /**
     * the logout method
     * @param evt 
     */
    private void LogoutActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_LogoutActionPerformed
        new Login();
        dispose();
    }//GEN-LAST:event_LogoutActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JMenuItem Logout;
    private javax.swing.JMenuItem addTaskMenu;
    private javax.swing.JPanel allJobPanel;
    private javax.swing.JTable allJobTable;
    private JobTableModel ajtm;
    private javax.swing.JMenuItem alterJobStatus;
    private javax.swing.JButton alterTaskBtn;
    private javax.swing.JMenuItem alterTaskMenu;
    private javax.swing.JPanel alterTaskPanel;
    private javax.swing.JMenuItem assignJob;
    private javax.swing.JMenuItem deleteTaskMenu;
    private javax.swing.JLabel descLabel;
    private javax.swing.JLabel descLabel1;
    private javax.swing.JLabel durationLabel;
    private javax.swing.JLabel durationLabel1;
    private javax.swing.JMenuItem generateJobSheet;
    private javax.swing.JLabel hourLabel;
    private javax.swing.JLabel hourLabel1;
    private javax.swing.JSpinner hourSpinner;
    private javax.swing.JSpinner hourSpinner1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JMenu jMenu1;
    private javax.swing.JMenu jMenu2;
    private javax.swing.JMenuBar jMenuBar1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JScrollPane jScrollPane4;
    private javax.swing.JScrollPane jScrollPane5;
    private javax.swing.JScrollPane jScrollPane6;
    private javax.swing.JScrollPane jScrollPane7;
    private javax.swing.JPopupMenu jobRightClickMenu;
    private javax.swing.JTable jobTable;
    private JobTableModel jtm;
    private TableRowSorter<JobTableModel> jobSorter;
    private javax.swing.JMenuItem jobToList;
    private javax.swing.JMenuItem launchReceptionistGUI;
    private javax.swing.JPanel mainPanel;
    private javax.swing.JLabel minLabel;
    private javax.swing.JLabel minLabel1;
    private javax.swing.JSpinner minSpinner;
    private javax.swing.JSpinner minSpinner1;
    private javax.swing.JButton newTaskBtn;
    private javax.swing.JPanel newTaskPanel;
    private javax.swing.JPopupMenu pJobRightClickMenu;
    private javax.swing.JPanel pendingList;
    private javax.swing.JButton pendingListBtn;
    private javax.swing.JTable pendingListTable;
    private PendingListTableModel pltm;
    private javax.swing.JComboBox preTaskCombo;
    private javax.swing.JComboBox preTaskCombo1;
    private javax.swing.JTextField searchField;
    private javax.swing.JLabel searchLabel;
    private javax.swing.JLabel secLabel;
    private javax.swing.JLabel secLabel1;
    private javax.swing.JSpinner secSpinner;
    private javax.swing.JSpinner secSpinner1;
    private javax.swing.JMenuItem selectPartMenu;
    private javax.swing.JMenuItem selectQuanMenu;
    private javax.swing.JPanel selectedJobPanel;
    private javax.swing.JTable selectedJobTable;
    private javax.swing.JPopupMenu selectedRightClickMenu;
    private javax.swing.JMenuItem selectedWorkReqMenu;
    private javax.swing.JPanel sideMenuPanel;
    private javax.swing.JPopupMenu stockRightClickMenu;
    private javax.swing.JTable stockTable;
    private StockTableModel stm;
    private TableRowSorter stockSorter;
    private javax.swing.JTextField taskField;
    private javax.swing.JTextField taskField1;
    private javax.swing.JPopupMenu taskRightClickMenu;
    private javax.swing.JTable taskTable;
    private JobTaskTableModel jttm;
    private TableRowSorter taskSorter;
    private javax.swing.JLabel taskTitleLabel;
    private javax.swing.JLabel taskTitleLabel1;
    private javax.swing.JButton updateTableBtn;
    private javax.swing.JLabel userConLabel;
    private javax.swing.JTable vehTable;
    private VehicleTableModel vtm;
    private TableRowSorter vehSorter;
    private javax.swing.JButton viewPendingBtn;
    private javax.swing.JButton viewSelectedBtn;
    private javax.swing.JPanel viewStockPanel;
    private javax.swing.JMenuItem viewTaskMenu;
    private javax.swing.JPanel viewTaskPanel;
    private javax.swing.JMenuItem viewVehMenu;
    private javax.swing.JPanel viewVehPanel;
    private javax.swing.JMenuItem workReqMenu;
    // End of variables declaration//GEN-END:variables
}
