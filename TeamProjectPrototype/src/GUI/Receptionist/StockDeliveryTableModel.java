/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package GUI.Receptionist;


import Entities.StockDelivery;
import java.sql.Date;
import java.util.ArrayList;
import javax.swing.table.AbstractTableModel;

/**
 * A table model to display Stock Delivery data
 * @author Abdalrahmane
 */
public class StockDeliveryTableModel extends AbstractTableModel {
    private ArrayList<StockDelivery> data;
    private String colNames[] = { "Delivery ID","Code", "Quantity", "Recieved", "Delivery Cost", "Date Ordered"};
    private Class<?> colClasses[] = { Integer.class, String.class, Integer.class, Boolean.class, Double.class, Date.class};

    /**
     * initalize the data for table
     * @param data 
     */
    public StockDeliveryTableModel(ArrayList<StockDelivery> data) {
        this.data = data;
    }
    
    /**
     * returns the row count
     * @return row count
     */
    @Override
    public int getRowCount() {
        return data.size();
    }

    /**
     * Returns column count
     * @return column count
     */
    @Override
    public int getColumnCount() {
        return colNames.length;
    }
    
    /**
     * Returns column name at specified index
     * @param columnIndex
     * @return column name
     */
    @Override
    public String getColumnName(int columnIndex) {
        return colNames[columnIndex];
    }
    
    /**
     * Returns the class of specified column
     * @param columnIndex
     * @return 
     */  
    @Override
    public Class<?> getColumnClass(int columnIndex) {
        return colClasses[columnIndex];
    }
    
    /**
     * Retrieve the data at specified value
     * this method is most important one as it is used to display the data correctly in the
     * jTable
     * @param rowIndex
     * @param columnIndex
     * @return 
     */
    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        if(columnIndex == 0){
            return data.get(rowIndex).getDeliveryID();
        }
        if (columnIndex == 1) {
            return data.get(rowIndex).getCode();
        }
        if (columnIndex == 2) {
            return data.get(rowIndex).getQuantity();
        }
        if (columnIndex == 3){
            return data.get(rowIndex).isRecieved();
        }
        if(columnIndex == 4){
            return data.get(rowIndex).getStockCost();
        }
        if(columnIndex == 5){
            return data.get(rowIndex).getOrdered();
        }
        return null;
    }
    
    /**
     * Refresh the data and update the table to display new values
     * @param data 
     */    
    public void refresh(ArrayList data){
        this.data = data;
        fireTableDataChanged();
    }
    
}
