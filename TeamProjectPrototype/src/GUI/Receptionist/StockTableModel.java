/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package GUI.Receptionist;

import Entities.StockItem;
import java.util.ArrayList;
import javax.swing.table.AbstractTableModel;

/**
 * A table model used to display Stock data
 * @author Abdalrahmane
 */
public class StockTableModel extends AbstractTableModel {
    private ArrayList<StockItem> data;

    private String colNames[] = { "Code", "Part Name", "Manufacturer", "Vehicle Type", "Year", "Price", "Cost per Item", "Stock Level", "Stock Cost", "Threshold", "Inital Stock", "Inital Cost"};
    private Class<?> colClasses[] = { String.class, String.class, String.class, String.class, String.class, Double.class, Double.class, Integer.class, Double.class, Integer.class, Integer.class, Double.class };
    
    /**
     * initalize the data for table
     * @param data 
     */
    public StockTableModel(ArrayList<StockItem> data) {
        this.data = data;
    }
    
    /**
     * returns the row count
     * @return row count
     */
    @Override
    public int getRowCount() {
        return data.size();
    }
    
    /**
     * Returns column count
     * @return column count
     */
    @Override
    public int getColumnCount() {
        return colNames.length;
    }

    /**
     * Returns column name at specified index
     * @param columnIndex
     * @return column name
     */
    @Override
    public String getColumnName(int columnIndex) {
        return colNames[columnIndex];
    }

    /**
     * Returns the class of specified column
     * @param columnIndex
     * @return 
     */  
    @Override
    public Class<?> getColumnClass(int columnIndex) {
        return colClasses[columnIndex];
    }
    
    /**
     * Retrieve the data at specified value
     * this method is most important one as it is used to display the data correctly in the
     * jTable
     * @param rowIndex
     * @param columnIndex
     * @return 
     */
    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        // "Code", "Part Name", "Manufacturer", "Vehicle Type", "Year", "Price", "Cost per Item", "Stock Level", "Stock Cost", "Threshold", "Inital Stock", "Inital Cost"};


        if (columnIndex == 0) {
            return data.get(rowIndex).getCode();
        }
        if (columnIndex == 1) {
            return data.get(rowIndex).getName();
        }
        if (columnIndex == 2){
            return data.get(rowIndex).getManufacturer();
        }
        if(columnIndex == 3){
            return data.get(rowIndex).getType();
        }
        if(columnIndex == 4){
            return data.get(rowIndex).getYear();
        }
        if(columnIndex == 5){
            return data.get(rowIndex).getPrice();
        }
        if(columnIndex == 6){
            return data.get(rowIndex).getCostItem();
        }
        if(columnIndex == 7){
            return data.get(rowIndex).getStockLvl();
        }
        if(columnIndex == 8){
            return data.get(rowIndex).getStockCost();
        }
        if(columnIndex == 9){
            return data.get(rowIndex).getThreshold();
        }
        if(columnIndex == 10){
            return data.get(rowIndex).getInitalStock();
        }
        if(columnIndex == 11){
            return data.get(rowIndex).getInitalCost();
        }
        return null;
    }
    
    /**
     * Refresh the data and update the table to display new values
     * @param data 
     */    
    public void refresh(ArrayList data){
        this.data = data;
        fireTableDataChanged();
    }
}
