/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package GUI;

import Entities.BusinessType;
import java.util.ArrayList;
import javax.swing.table.AbstractTableModel;

/**
 * A table model used to display business type information
 * @author Abdalrahmane
 */
public class BusinessTypeTableModel extends AbstractTableModel {
    private ArrayList<BusinessType> data;
    private String colNames[] = { "Type ID", "Type"};
    private Class<?> colClasses[] = { Integer.class, String.class};

    /**
     * initalize the data for table
     * @param data 
     */
    public BusinessTypeTableModel(ArrayList data) {
        this.data = data;
    }

    /**
     * returns the row count
     * @return row count
     */
    @Override
    public int getRowCount() {
        return data.size();
    }

    /**
     * Returns column count
     * @return column count
     */
    @Override
    public int getColumnCount() {
        return colNames.length;
    }
    
    /**
     * Returns column name at specified index
     * @param columnIndex
     * @return column name
     */
    @Override
    public String getColumnName(int columnIndex) {
        return colNames[columnIndex];
    }
    
    /**
     * Returns the class of specified column
     * @param columnIndex
     * @return 
     */
    @Override
    public Class<?> getColumnClass(int columnIndex) {
        return colClasses[columnIndex];
    }

    /**
     * Retrieve the data at specified value
     * this method is most important one as it is used to display the data correctly in the
     * jTable
     * @param rowIndex
     * @param columnIndex
     * @return 
     */
    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        if (columnIndex == 0) {
            return data.get(rowIndex).getBusID();
        }
        if (columnIndex == 1) {
            return data.get(rowIndex).getType();
        }
        return null;
    }
    
    /**
     * Refresh the data and update the table to display new values
     * @param data 
     */
    public void refresh(ArrayList data){
        this.data = data;
        fireTableDataChanged();
    }
}
