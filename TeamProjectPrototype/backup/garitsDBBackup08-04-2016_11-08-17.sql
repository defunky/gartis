-- MySQL dump 10.13  Distrib 5.6.20, for Win32 (x86)
--
-- Host: localhost    Database: gartisdb
-- ------------------------------------------------------
-- Server version	5.6.20

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Current Database: `gartisdb`
--

CREATE DATABASE /*!32312 IF NOT EXISTS*/ `gartisdb` /*!40100 DEFAULT CHARACTER SET latin1 */;

USE `gartisdb`;

--
-- Table structure for table `businesstypes`
--

DROP TABLE IF EXISTS `businesstypes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `businesstypes` (
  `BusID` int(11) NOT NULL AUTO_INCREMENT,
  `Type` varchar(255) NOT NULL,
  `Flatrate` double(11,2) NOT NULL,
  PRIMARY KEY (`BusID`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `businesstypes`
--

LOCK TABLES `businesstypes` WRITE;
/*!40000 ALTER TABLE `businesstypes` DISABLE KEYS */;
INSERT INTO `businesstypes` VALUES (1,'Repair',55.00),(2,'MoT',55.00),(3,'Annual Service',55.00);
/*!40000 ALTER TABLE `businesstypes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `config`
--

DROP TABLE IF EXISTS `config`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `config` (
  `ConfigID` int(11) NOT NULL AUTO_INCREMENT,
  `ConfigName` varchar(255) NOT NULL,
  `Value` double NOT NULL,
  PRIMARY KEY (`ConfigID`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `config`
--

LOCK TABLES `config` WRITE;
/*!40000 ALTER TABLE `config` DISABLE KEYS */;
INSERT INTO `config` VALUES (1,'VAT',0.2),(2,'Markup',0.3);
/*!40000 ALTER TABLE `config` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `customers`
--

DROP TABLE IF EXISTS `customers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `customers` (
  `CustomerID` int(11) NOT NULL AUTO_INCREMENT,
  `Name` varchar(255) NOT NULL,
  `Telephone` varchar(255) NOT NULL,
  `Mobile` varchar(255) DEFAULT NULL,
  `Email` varchar(255) DEFAULT NULL,
  `Address` varchar(255) NOT NULL,
  `Street` varchar(255) NOT NULL,
  `Locality` varchar(255) NOT NULL,
  `City` varchar(255) NOT NULL,
  `PostCode` varchar(255) NOT NULL,
  `Additional_Notes` varchar(255) DEFAULT NULL,
  `CustomerType` varchar(255) NOT NULL,
  `Discount_ID` int(11) DEFAULT NULL,
  PRIMARY KEY (`CustomerID`),
  KEY `Discount_ID` (`Discount_ID`),
  CONSTRAINT `customers_ibfk_1` FOREIGN KEY (`Discount_ID`) REFERENCES `discount` (`Discount_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `customers`
--

LOCK TABLES `customers` WRITE;
/*!40000 ALTER TABLE `customers` DISABLE KEYS */;
INSERT INTO `customers` VALUES (1,'Transco Gas Supplies','0494 683 725','','','Methan Buildings','Sulphur Lane','Bucks','Stenchville','HP19 2MT','Contact Jack Varta','Account Holder',NULL),(2,'John Doherty','0101 010 0101','07070 070 707','','Miscellaneous House','Unknown Street','Nowhereshire','Whichville','MT1 2UP','','Account Holder',NULL),(3,'William Gates','0207 477 3333','0666 666 666','','World Domination House','Enormous Street','','Richville','NW10 4AT','','Account Holder',NULL),(4,'Remi','0123456789','','','Northhampton Square','Any Street','','London','E1V 9HB','','Casual',NULL);
/*!40000 ALTER TABLE `customers` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `discount`
--

DROP TABLE IF EXISTS `discount`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `discount` (
  `Discount_ID` int(11) NOT NULL,
  `Type_ID` varchar(255) NOT NULL,
  PRIMARY KEY (`Discount_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `discount`
--

LOCK TABLES `discount` WRITE;
/*!40000 ALTER TABLE `discount` DISABLE KEYS */;
/*!40000 ALTER TABLE `discount` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `fixed`
--

DROP TABLE IF EXISTS `fixed`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `fixed` (
  `Fixed_ID` int(11) NOT NULL,
  `Discount_ID` int(11) NOT NULL,
  `Fixed_Rate` decimal(4,2) DEFAULT NULL,
  PRIMARY KEY (`Fixed_ID`),
  UNIQUE KEY `Discount_ID` (`Discount_ID`),
  CONSTRAINT `fixed_ibfk_1` FOREIGN KEY (`Discount_ID`) REFERENCES `discount` (`Discount_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `fixed`
--

LOCK TABLES `fixed` WRITE;
/*!40000 ALTER TABLE `fixed` DISABLE KEYS */;
/*!40000 ALTER TABLE `fixed` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `invoices`
--

DROP TABLE IF EXISTS `invoices`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `invoices` (
  `InvoiceID` int(11) NOT NULL AUTO_INCREMENT,
  `JobID` int(11) NOT NULL,
  `StockAmount` double DEFAULT NULL,
  `LabourCost` double DEFAULT NULL,
  `Total` double DEFAULT NULL,
  `VAT` double(11,2) DEFAULT NULL,
  `GrandTotal` double(11,2) NOT NULL,
  `Reminder` int(11) NOT NULL,
  `initalDate` date NOT NULL,
  `lastDateReminder` date NOT NULL,
  `Paid` tinyint(1) NOT NULL,
  PRIMARY KEY (`InvoiceID`),
  KEY `JobID` (`JobID`),
  CONSTRAINT `invoices_ibfk_1` FOREIGN KEY (`JobID`) REFERENCES `jobs` (`JobID`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `invoices`
--

LOCK TABLES `invoices` WRITE;
/*!40000 ALTER TABLE `invoices` DISABLE KEYS */;
INSERT INTO `invoices` VALUES (1,1,0,0,0,0.00,55.00,2,'2016-03-08','2016-04-08',0),(2,2,25,0,25,5.00,85.00,0,'2016-04-08','2016-04-08',0),(3,4,25,139.999999965,164.999999965,33.00,253.00,0,'2016-04-08','2016-04-08',0);
/*!40000 ALTER TABLE `invoices` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `jobpayment`
--

DROP TABLE IF EXISTS `jobpayment`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `jobpayment` (
  `PaymentID` int(11) NOT NULL AUTO_INCREMENT,
  `InvoiceID` int(11) NOT NULL,
  `Card_no` varchar(255) NOT NULL,
  `expires` varchar(255) NOT NULL,
  `payment_made` date NOT NULL,
  PRIMARY KEY (`PaymentID`),
  KEY `InvoiceID` (`InvoiceID`),
  CONSTRAINT `jobpayment_ibfk_1` FOREIGN KEY (`InvoiceID`) REFERENCES `invoices` (`InvoiceID`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `jobpayment`
--

LOCK TABLES `jobpayment` WRITE;
/*!40000 ALTER TABLE `jobpayment` DISABLE KEYS */;
INSERT INTO `jobpayment` VALUES (1,1,'012345678','03/09','2016-04-08');
/*!40000 ALTER TABLE `jobpayment` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `jobs`
--

DROP TABLE IF EXISTS `jobs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `jobs` (
  `JobID` int(11) NOT NULL AUTO_INCREMENT,
  `RegNo` varchar(255) NOT NULL,
  `Work_Required` varchar(255) NOT NULL,
  `JobStatus` varchar(1024) NOT NULL,
  `BusinessType` varchar(255) NOT NULL,
  `Duration` time NOT NULL,
  `Date_creation` date NOT NULL,
  PRIMARY KEY (`JobID`),
  KEY `RegNo` (`RegNo`),
  CONSTRAINT `jobs_ibfk_1` FOREIGN KEY (`RegNo`) REFERENCES `vehicles` (`RegNo`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `jobs`
--

LOCK TABLES `jobs` WRITE;
/*!40000 ALTER TABLE `jobs` DISABLE KEYS */;
INSERT INTO `jobs` VALUES (1,'ISI20 02S','Fix Windscreen\nFix Something\n','COMPLETED','Repair','01:00:00','2016-04-08'),(2,'HCU13 UDO','oanf\nisdh\nhdsf','COMPLETED','Repair','01:00:00','2016-04-08'),(3,'V424 BHN','hdfda\n','COMPLETED','Repair','02:15:00','2016-04-08'),(4,'HCU13 UDO','jnscvokds\nhsac[sa','COMPLETED','Repair','01:20:00','2016-04-08'),(5,'A21 OLE','pojdas','IDLE','Repair','01:00:00','2016-04-08'),(6,'V424 BHN','MoT Annual Checkup','IDLE','MoT','00:45:00','2016-04-08'),(7,'ISI20 02S','MoT Annual Checkup','IDLE','MoT','00:45:00','2016-04-08');
/*!40000 ALTER TABLE `jobs` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pendingjobs`
--

DROP TABLE IF EXISTS `pendingjobs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pendingjobs` (
  `PendingJobID` int(11) NOT NULL AUTO_INCREMENT,
  `JobID` int(11) NOT NULL,
  `StaffID` int(11) DEFAULT NULL,
  PRIMARY KEY (`PendingJobID`),
  KEY `JobID` (`JobID`),
  KEY `StaffID` (`StaffID`),
  CONSTRAINT `pendingjobs_ibfk_1` FOREIGN KEY (`JobID`) REFERENCES `jobs` (`JobID`),
  CONSTRAINT `pendingjobs_ibfk_2` FOREIGN KEY (`StaffID`) REFERENCES `staffaccounts` (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pendingjobs`
--

LOCK TABLES `pendingjobs` WRITE;
/*!40000 ALTER TABLE `pendingjobs` DISABLE KEYS */;
INSERT INTO `pendingjobs` VALUES (1,1,6),(2,2,4),(3,3,4),(4,4,6),(5,5,6);
/*!40000 ALTER TABLE `pendingjobs` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `purchasedpartinvoices`
--

DROP TABLE IF EXISTS `purchasedpartinvoices`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `purchasedpartinvoices` (
  `pInvoiceID` int(11) NOT NULL AUTO_INCREMENT,
  `SoldID` int(11) NOT NULL,
  `StockAmount` double(11,2) DEFAULT NULL,
  `VAT` double(11,2) DEFAULT NULL,
  `GrandTotal` double(11,2) NOT NULL,
  `Reminder` int(11) NOT NULL,
  `initalDate` date NOT NULL,
  `lastDateReminder` date NOT NULL,
  `Paid` tinyint(1) NOT NULL,
  PRIMARY KEY (`pInvoiceID`),
  KEY `SoldID` (`SoldID`),
  CONSTRAINT `purchasedpartinvoices_ibfk_1` FOREIGN KEY (`SoldID`) REFERENCES `sparepartsold` (`SoldID`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `purchasedpartinvoices`
--

LOCK TABLES `purchasedpartinvoices` WRITE;
/*!40000 ALTER TABLE `purchasedpartinvoices` DISABLE KEYS */;
INSERT INTO `purchasedpartinvoices` VALUES (1,1,45.00,9.00,54.00,0,'2016-04-08','2016-04-08',0);
/*!40000 ALTER TABLE `purchasedpartinvoices` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sparepartdelivery`
--

DROP TABLE IF EXISTS `sparepartdelivery`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sparepartdelivery` (
  `DeliveryID` int(11) NOT NULL AUTO_INCREMENT,
  `Code` varchar(255) NOT NULL,
  `Quantity` int(11) NOT NULL,
  `Recieved` tinyint(1) NOT NULL,
  `Stock_Cost` double NOT NULL,
  `Date_Ordered` date NOT NULL,
  PRIMARY KEY (`DeliveryID`),
  KEY `Code` (`Code`),
  CONSTRAINT `sparepartdelivery_ibfk_1` FOREIGN KEY (`Code`) REFERENCES `sparepartstock` (`Code`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sparepartdelivery`
--

LOCK TABLES `sparepartdelivery` WRITE;
/*!40000 ALTER TABLE `sparepartdelivery` DISABLE KEYS */;
INSERT INTO `sparepartdelivery` VALUES (1,'JSI YSI',10,1,150,'2016-04-08');
/*!40000 ALTER TABLE `sparepartdelivery` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sparepartsold`
--

DROP TABLE IF EXISTS `sparepartsold`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sparepartsold` (
  `SoldID` int(11) NOT NULL AUTO_INCREMENT,
  `Code` varchar(255) NOT NULL,
  `CustomerID` int(11) NOT NULL,
  `Amount` int(11) NOT NULL,
  `DatePurchased` date NOT NULL,
  PRIMARY KEY (`SoldID`),
  KEY `Code` (`Code`),
  KEY `CustomerID` (`CustomerID`),
  CONSTRAINT `sparepartsold_ibfk_1` FOREIGN KEY (`Code`) REFERENCES `sparepartstock` (`Code`),
  CONSTRAINT `sparepartsold_ibfk_2` FOREIGN KEY (`CustomerID`) REFERENCES `customers` (`CustomerID`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sparepartsold`
--

LOCK TABLES `sparepartsold` WRITE;
/*!40000 ALTER TABLE `sparepartsold` DISABLE KEYS */;
INSERT INTO `sparepartsold` VALUES (1,'JSI YSI',4,3,'2016-04-08');
/*!40000 ALTER TABLE `sparepartsold` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sparepartstock`
--

DROP TABLE IF EXISTS `sparepartstock`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sparepartstock` (
  `Code` varchar(255) NOT NULL,
  `Part_Name` varchar(255) NOT NULL,
  `Manufacturer` varchar(255) NOT NULL,
  `Vehicle_Type` varchar(255) NOT NULL,
  `Year` varchar(255) NOT NULL,
  `Price` double NOT NULL,
  `Cost_Per_Item` double NOT NULL,
  `Stock_Level` int(11) NOT NULL,
  `Stock_Cost` double NOT NULL,
  `Threshold_Level` int(11) NOT NULL,
  `Inital_Stock_Level` int(11) NOT NULL,
  `Inital_Stock_Cost` double NOT NULL,
  PRIMARY KEY (`Code`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sparepartstock`
--

LOCK TABLES `sparepartstock` WRITE;
/*!40000 ALTER TABLE `sparepartstock` DISABLE KEYS */;
INSERT INTO `sparepartstock` VALUES ('0SI JA13','Motor Oil','All Makes','All Makes','2003',25,20,27,540,25,30,600),('0SK SH9','Distributor Cap','Fjord','Fjord Vehicles','2009',35,28,10,280,5,10,280),('GJD 8US','Engine Mount','All Makes','All Makes','2008',15,12,6,72,4,6,72),('GX39 230','Heavy Tread Tyre','Fjord','Transit Van','2006',45,25,8,200,6,8,200),('J9S 90S','Oil Filter','All Makes','All Makes','2004',10,6,16,96,15,16,96),('JSI YSI','AlteredTest','test1','test2','2003',25,15,11,165,10,15,225),('K0DS K0S','Interior Bulb','Rolls Royce','Royce Vehicles','2010',118,98,2,196,1,2,196),('OQ2 9ES','Spark Plugs','All Makes','All Makes','2003',1.5,0.7,23,16.1,20,23,16.1),('OS3 0SD','Paint','Slap-it-on','All','2000',60,30,3,90,2,3,90),('SD03 9SD','Air Filter','All makes','All makes','2008',15,11,15,165,10,15,165),('Y7S 9S','Spark Leads','All Makes','All Makes','2003',12.5,10.5,16,168,10,16,168),('YU2 83S','Exhaust, Complete Box','Fjord','Estate','2003',200,150,3,450,2,3,450);
/*!40000 ALTER TABLE `sparepartstock` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sparepartused`
--

DROP TABLE IF EXISTS `sparepartused`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sparepartused` (
  `UsedID` int(11) NOT NULL AUTO_INCREMENT,
  `Code` varchar(255) NOT NULL,
  `JobID` int(11) NOT NULL,
  `Used` int(11) NOT NULL,
  `DateUsed` date NOT NULL,
  PRIMARY KEY (`UsedID`),
  KEY `Code` (`Code`),
  KEY `JobID` (`JobID`),
  CONSTRAINT `sparepartused_ibfk_1` FOREIGN KEY (`Code`) REFERENCES `sparepartstock` (`Code`),
  CONSTRAINT `sparepartused_ibfk_2` FOREIGN KEY (`JobID`) REFERENCES `jobs` (`JobID`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sparepartused`
--

LOCK TABLES `sparepartused` WRITE;
/*!40000 ALTER TABLE `sparepartused` DISABLE KEYS */;
INSERT INTO `sparepartused` VALUES (1,'0SI JA13',1,1,'2016-04-08'),(2,'0SI JA13',2,1,'2016-04-08'),(3,'0SI JA13',4,1,'2016-04-08');
/*!40000 ALTER TABLE `sparepartused` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `staffaccounts`
--

DROP TABLE IF EXISTS `staffaccounts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `staffaccounts` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `UserName` varchar(255) NOT NULL,
  `Password` varbinary(512) NOT NULL,
  `Type` varchar(255) NOT NULL,
  `Surname` varchar(255) NOT NULL,
  `Name` varchar(255) NOT NULL,
  `Date_creation` date NOT NULL,
  `Labour_cost` double DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `staffaccounts`
--

LOCK TABLES `staffaccounts` WRITE;
/*!40000 ALTER TABLE `staffaccounts` DISABLE KEYS */;
INSERT INTO `staffaccounts` VALUES (1,'admin','^�H��(qQ��o��)\'s`=\rj���*�rB�','ADMIN','Default','Account','2016-04-08',NULL),(2,'Penelope','V��\"�H|��t�U:�`�l`ʲ�A���JѮnaE�','RECEPTIONIST','Carr','Penelope','2016-04-08',NULL),(3,'Sunny','�<�k>�ܸ��Q\n�M>w��|���+0�N����','FOREPERSON','Evans','Sunny','2016-04-08',125),(4,'Glynne','�4Tu���Qo_�~\']�\r�ؑ���$kc','FRANCHISEE','Lancaster','Glynne','2016-04-08',NULL),(5,'SYSDBA','\0�k��ִ�}g��K\"�\Zϰ�ۢ��Yp�V','ADMIN','Role','Administrator','2016-04-08',NULL),(6,'Gavin','�lIv��%���}p�Ug�?@�&4�gnt�R�T\'�','MECHANIC','Ross','Gavin','2016-04-08',105),(7,'Anthony','R- ���\nr4Y�36sx��4�\'�\n�r�x�(S)','MECHANIC','Wild','Anthony','2016-04-08',105),(8,'Newtest','�e�Y B/�A~Hg��O��J?��~������z�','MECHANIC','Test','Test','2016-04-08',105);
/*!40000 ALTER TABLE `staffaccounts` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tasklist`
--

DROP TABLE IF EXISTS `tasklist`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tasklist` (
  `ListID` int(11) NOT NULL AUTO_INCREMENT,
  `Description` varchar(255) NOT NULL,
  `Duration` time NOT NULL,
  PRIMARY KEY (`ListID`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tasklist`
--

LOCK TABLES `tasklist` WRITE;
/*!40000 ALTER TABLE `tasklist` DISABLE KEYS */;
INSERT INTO `tasklist` VALUES (1,'Fender Replacement','01:20:00'),(2,'Windscreen Replacement','01:30:00'),(3,'Oil Check','00:45:00'),(4,'Engine Check','01:00:00'),(5,'Tyre Replacement','00:30:00');
/*!40000 ALTER TABLE `tasklist` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tasks`
--

DROP TABLE IF EXISTS `tasks`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tasks` (
  `TaskID` int(11) NOT NULL AUTO_INCREMENT,
  `JobID` int(11) NOT NULL,
  `Description` varchar(255) NOT NULL,
  `Duration` time NOT NULL,
  PRIMARY KEY (`TaskID`),
  KEY `JobID` (`JobID`),
  CONSTRAINT `tasks_ibfk_1` FOREIGN KEY (`JobID`) REFERENCES `jobs` (`JobID`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tasks`
--

LOCK TABLES `tasks` WRITE;
/*!40000 ALTER TABLE `tasks` DISABLE KEYS */;
INSERT INTO `tasks` VALUES (1,3,'Windscreen Replacement','01:30:00'),(2,3,'Oil Check','00:45:00'),(3,4,'Fender Replacement','01:20:00');
/*!40000 ALTER TABLE `tasks` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `vehicles`
--

DROP TABLE IF EXISTS `vehicles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `vehicles` (
  `RegNo` varchar(255) NOT NULL,
  `CustomerID` int(11) NOT NULL,
  `EngSerial` int(11) NOT NULL,
  `Colour` varchar(255) DEFAULT NULL,
  `Make` varchar(255) DEFAULT NULL,
  `Model` varchar(255) DEFAULT NULL,
  `ChassisNo` int(11) NOT NULL,
  `Year` varchar(255) NOT NULL,
  `LastMoTCheck` date DEFAULT NULL,
  PRIMARY KEY (`RegNo`),
  KEY `CustomerID` (`CustomerID`),
  CONSTRAINT `vehicles_ibfk_1` FOREIGN KEY (`CustomerID`) REFERENCES `customers` (`CustomerID`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `vehicles`
--

LOCK TABLES `vehicles` WRITE;
/*!40000 ALTER TABLE `vehicles` DISABLE KEYS */;
INSERT INTO `vehicles` VALUES ('A21 OLE',3,30213,'Red','Rolls Royce','Silver Shadow',3182,'2006(08/08)','2016-04-08'),('A878 CPG',1,73822,'white','Fjord','Transit Van',38212,'2006(Dec)','2016-04-08'),('C399 DWR',1,92312,'white','Fjord','Transit Van',293123,'2007(Dec)','2016-04-08'),('F33 GHT',1,78192,'white','Fjord','Transit Van',18232,'2008(Feb)','2016-04-08'),('G135 PUB',2,32123,'green','Fjord','Estate MK8',1234,'2001','2016-04-08'),('HCU13 UDO',4,36271,'white','Ferrari','430',3781,'2008','2016-04-08'),('ISI20 02S',4,3821,'white','Toyota','Some Make',29123,'2010(dec)','2015-04-08'),('V424 BHN',1,79321,'white','Fjord','Transit Van',29312,'2008(Feb)','2015-04-08');
/*!40000 ALTER TABLE `vehicles` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2016-04-08 11:08:20
